// ----------------------------------------------------------------------------
enum EEUI_MapType {
    EEUIMT_VALUE = 0,
    EEUIMT_REF = 1,
}
// ----------------------------------------------------------------------------
struct SEUI_ReferenceId {
    var id: String;
    var valueIdPrefix: String;
}
// ----------------------------------------------------------------------------
struct SEUI_ValueId {
    var cat: String;
    var id: String;
}
// ----------------------------------------------------------------------------
struct SEUI_VarMapping {
    var id: String;
    var type: EEUI_MapType;
    var value: SEUI_ValueId;
    var reference: SEUI_ReferenceId;
}
// ----------------------------------------------------------------------------
struct SEUI_ClassMapping {
    var id: String;
    var values: array<SEUI_VarMapping>;
}
// ----------------------------------------------------------------------------
// maps yaml definition structure supported by encoder to ids from envui
class CModEnvUiDefinitionMapper {
    // ------------------------------------------------------------------------
    private var mappings: array<SEUI_ClassMapping>;
    // ------------------------------------------------------------------------
    public function init() {
        add_root();
    }
    // ------------------------------------------------------------------------
    private function add_root() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("environment");
        group.values.PushBack(SEUI_VarMapping("finalColorBalance", EEUIMT_REF,, SEUI_ReferenceId("FinalColorBalanceParameters")));
        group.values.PushBack(SEUI_VarMapping("sharpen", EEUIMT_REF,, SEUI_ReferenceId("SharpenParameters")));
        group.values.PushBack(SEUI_VarMapping("paintEffect", EEUIMT_REF,, SEUI_ReferenceId("PaintEffectParameters")));
        group.values.PushBack(SEUI_VarMapping("ssaoNV", EEUIMT_REF,, SEUI_ReferenceId("NVSSAOParameters")));
        group.values.PushBack(SEUI_VarMapping("ssaoMS", EEUIMT_REF,, SEUI_ReferenceId("MSSSAOParameters")));
        group.values.PushBack(SEUI_VarMapping("globalLight", EEUIMT_REF,, SEUI_ReferenceId("GlobalLightParameters")));
        group.values.PushBack(SEUI_VarMapping("interiorFallback", EEUIMT_REF,, SEUI_ReferenceId("InteriorFallbackParameters")));
        group.values.PushBack(SEUI_VarMapping("speedTree", EEUIMT_REF,, SEUI_ReferenceId("SpeedTreeParameters")));
        group.values.PushBack(SEUI_VarMapping("toneMapping", EEUIMT_REF,, SEUI_ReferenceId("ToneMappingParameters")));
        group.values.PushBack(SEUI_VarMapping("bloomNew", EEUIMT_REF,, SEUI_ReferenceId("BloomNewParameters")));
        group.values.PushBack(SEUI_VarMapping("globalFog", EEUIMT_REF,, SEUI_ReferenceId("GlobalFogParameters")));
        group.values.PushBack(SEUI_VarMapping("sky", EEUIMT_REF,, SEUI_ReferenceId("GlobalSkyParameters")));
        group.values.PushBack(SEUI_VarMapping("depthOfField", EEUIMT_REF,, SEUI_ReferenceId("DepthOfFieldParameters")));
        group.values.PushBack(SEUI_VarMapping("colorModTransparency", EEUIMT_REF,, SEUI_ReferenceId("ColorModTransparencyParameters")));
        group.values.PushBack(SEUI_VarMapping("shadows", EEUIMT_REF,, SEUI_ReferenceId("ShadowsParameters")));
        group.values.PushBack(SEUI_VarMapping("water", EEUIMT_REF,, SEUI_ReferenceId("WaterParameters")));
        group.values.PushBack(SEUI_VarMapping("colorGroups", EEUIMT_REF,, SEUI_ReferenceId("ColorGroupsParameters")));
        group.values.PushBack(SEUI_VarMapping("flareColorGroups", EEUIMT_REF,, SEUI_ReferenceId("FlareColorGroupsParameters")));
        group.values.PushBack(SEUI_VarMapping("sunAndMoonParams", EEUIMT_REF,, SEUI_ReferenceId("SunAndMoonParameters")));
        group.values.PushBack(SEUI_VarMapping("windParams", EEUIMT_REF,, SEUI_ReferenceId("WindParameters")));
        group.values.PushBack(SEUI_VarMapping("gameplayEffects", EEUIMT_REF,, SEUI_ReferenceId("GameplayEffectsParameters")));
        group.values.PushBack(SEUI_VarMapping("motionBlur", EEUIMT_REF,, SEUI_ReferenceId("MotionBlurParameters")));
        group.values.PushBack(SEUI_VarMapping("cameraLightsSetup", EEUIMT_REF,, SEUI_ReferenceId("CameraLightsSetupParameters")));
        group.values.PushBack(SEUI_VarMapping("dialogLightParams", EEUIMT_REF,, SEUI_ReferenceId("DialogLightParameters")));
        mappings.PushBack(group);

        add_finalColorBalance();
        add_sharpen();
        add_paintEffect();
        add_ssaoNV();
        add_ssaoMS();
        add_globalLight();
        add_interiorFallback();
        add_speedTree();
        add_toneMapping();
        add_bloomNew();
        add_globalFog();
        add_sky();
        add_depthOfField();
        add_colorModTransparency();
        add_shadows();
        add_water();
        add_colorGroups();
        add_flareColorGroups();
        add_sunAndMoonParams();
        add_windParams();
        add_gameplayEffects();
        add_motionBlur();
        add_cameraLightsSetup();
        add_dialogLightParams();
    }
    // ------------------------------------------------------------------------
    private function add_finalColorBalance() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("FinalColorBalanceParameters");
        group.values.Clear();
        cat1 = "finalColorBalance";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("activatedBalanceMap", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedBalanceMap")));
        group.values.PushBack(SEUI_VarMapping("activatedParametricBalance", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedParametricBalance")));
        group.values.PushBack(SEUI_VarMapping("vignetteWeights", EEUIMT_VALUE, SEUI_ValueId(cat1, "vignetteWeights")));
        group.values.PushBack(SEUI_VarMapping("vignetteColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "vignetteColor")));
        group.values.PushBack(SEUI_VarMapping("vignetteOpacity", EEUIMT_VALUE, SEUI_ValueId(cat1, "vignetteOpacity")));
        group.values.PushBack(SEUI_VarMapping("chromaticAberrationSize", EEUIMT_VALUE, SEUI_ValueId(cat1, "chromaticAberrationSize")));
        group.values.PushBack(SEUI_VarMapping("balanceMapLerp", EEUIMT_VALUE, SEUI_ValueId(cat1, "balanceMapLerp")));
        group.values.PushBack(SEUI_VarMapping("balanceMapAmount", EEUIMT_VALUE, SEUI_ValueId(cat1, "balanceMapAmount")));
        //group.values.PushBack(SEUI_VarMapping("balanceMap0", EEUIMT_VALUE,,SEUI_ValueId(, "CBitmapTexture")));
        //group.values.PushBack(SEUI_VarMapping("balanceMap1", EEUIMT_VALUE,,SEUI_ValueId(, "CBitmapTexture")));
        group.values.PushBack(SEUI_VarMapping("balancePostBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "balancePostBrightness")));
        group.values.PushBack(SEUI_VarMapping("levelsShadows", EEUIMT_VALUE, SEUI_ValueId(cat1, "levelsShadows")));
        group.values.PushBack(SEUI_VarMapping("levelsMidtones", EEUIMT_VALUE, SEUI_ValueId(cat1, "levelsMidtones")));
        group.values.PushBack(SEUI_VarMapping("levelsHighlights", EEUIMT_VALUE, SEUI_ValueId(cat1, "levelsHighlights")));
        group.values.PushBack(SEUI_VarMapping("midtoneRangeMin", EEUIMT_VALUE, SEUI_ValueId(cat1, "midtoneRangeMin")));
        group.values.PushBack(SEUI_VarMapping("midtoneRangeMax", EEUIMT_VALUE, SEUI_ValueId(cat1, "midtoneRangeMax")));
        group.values.PushBack(SEUI_VarMapping("midtoneMarginMin", EEUIMT_VALUE, SEUI_ValueId(cat1, "midtoneMarginMin")));
        group.values.PushBack(SEUI_VarMapping("midtoneMarginMax", EEUIMT_VALUE, SEUI_ValueId(cat1, "midtoneMarginMax")));

        group.values.PushBack(SEUI_VarMapping("parametricBalanceLow", EEUIMT_REF,, SEUI_ReferenceId("ParametricBalance", "parametricBalanceLow.")));
        group.values.PushBack(SEUI_VarMapping("parametricBalanceMid", EEUIMT_REF,, SEUI_ReferenceId("ParametricBalance", "parametricBalanceMid.")));
        group.values.PushBack(SEUI_VarMapping("parametricBalanceHigh", EEUIMT_REF,, SEUI_ReferenceId("ParametricBalance", "parametricBalanceHigh.")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("ParametricBalance");
        group.values.Clear();
        cat1 = "finalColorBalance";
        group.values.PushBack(SEUI_VarMapping("saturation", EEUIMT_VALUE, SEUI_ValueId(cat1, "saturation")));
        group.values.PushBack(SEUI_VarMapping("color", EEUIMT_VALUE, SEUI_ValueId(cat1, "color")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_sharpen() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("SharpenParameters");
        group.values.Clear();
        cat1 = "sharpen";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("sharpenNear", EEUIMT_VALUE, SEUI_ValueId(cat1, "sharpenNear")));
        group.values.PushBack(SEUI_VarMapping("sharpenFar", EEUIMT_VALUE, SEUI_ValueId(cat1, "sharpenFar")));
        group.values.PushBack(SEUI_VarMapping("distanceNear", EEUIMT_VALUE, SEUI_ValueId(cat1, "distanceNear")));
        group.values.PushBack(SEUI_VarMapping("distanceFar", EEUIMT_VALUE, SEUI_ValueId(cat1, "distanceFar")));
        group.values.PushBack(SEUI_VarMapping("lumFilterOffset", EEUIMT_VALUE, SEUI_ValueId(cat1, "lumFilterOffset")));
        group.values.PushBack(SEUI_VarMapping("lumFilterRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "lumFilterRange")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_paintEffect() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("PaintEffectParameters");
        group.values.Clear();
        cat1 = "paintEffect";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("amount", EEUIMT_VALUE, SEUI_ValueId(cat1, "amount")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_ssaoNV() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("NVSSAOParameters");
        group.values.Clear();
        cat1 = "ssaoNV";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("radius", EEUIMT_VALUE, SEUI_ValueId(cat1, "radius")));
        group.values.PushBack(SEUI_VarMapping("bias", EEUIMT_VALUE, SEUI_ValueId(cat1, "bias")));
        group.values.PushBack(SEUI_VarMapping("detailStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "detailStrength")));
        group.values.PushBack(SEUI_VarMapping("coarseStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "coarseStrength")));
        group.values.PushBack(SEUI_VarMapping("powerExponent", EEUIMT_VALUE, SEUI_ValueId(cat1, "powerExponent")));
        group.values.PushBack(SEUI_VarMapping("blurSharpness", EEUIMT_VALUE, SEUI_ValueId(cat1, "blurSharpness")));
        group.values.PushBack(SEUI_VarMapping("valueClamp", EEUIMT_VALUE, SEUI_ValueId(cat1, "valueClamp")));
        group.values.PushBack(SEUI_VarMapping("ssaoColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "ssaoColor")));
        group.values.PushBack(SEUI_VarMapping("nonAmbientInfluence", EEUIMT_VALUE, SEUI_ValueId(cat1, "nonAmbientInfluence")));
        group.values.PushBack(SEUI_VarMapping("translucencyInfluence", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyInfluence")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_ssaoMS() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("MSSSAOParameters");
        group.values.Clear();
        cat1 = "ssaoMS";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("noiseFilterTolerance", EEUIMT_VALUE, SEUI_ValueId(cat1, "noiseFilterTolerance")));
        group.values.PushBack(SEUI_VarMapping("blurTolerance", EEUIMT_VALUE, SEUI_ValueId(cat1, "blurTolerance")));
        group.values.PushBack(SEUI_VarMapping("upsampleTolerance", EEUIMT_VALUE, SEUI_ValueId(cat1, "upsampleTolerance")));
        group.values.PushBack(SEUI_VarMapping("rejectionFalloff", EEUIMT_VALUE, SEUI_ValueId(cat1, "rejectionFalloff")));
        group.values.PushBack(SEUI_VarMapping("combineResolutionsBeforeBlur", EEUIMT_VALUE, SEUI_ValueId(cat1, "combineResolutionsBeforeBlur")));
        group.values.PushBack(SEUI_VarMapping("combineResolutionsWithMul", EEUIMT_VALUE, SEUI_ValueId(cat1, "combineResolutionsWithMul")));
        group.values.PushBack(SEUI_VarMapping("hierarchyDepth", EEUIMT_VALUE, SEUI_ValueId(cat1, "hierarchyDepth")));
        group.values.PushBack(SEUI_VarMapping("normalAOMultiply", EEUIMT_VALUE, SEUI_ValueId(cat1, "normalAOMultiply")));
        group.values.PushBack(SEUI_VarMapping("normalToDepthBrightnessEqualiser", EEUIMT_VALUE, SEUI_ValueId(cat1, "normalToDepthBrightnessEqualiser")));
        group.values.PushBack(SEUI_VarMapping("normalBackProjectionTolerance", EEUIMT_VALUE, SEUI_ValueId(cat1, "normalBackProjectionTolerance")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_globalLight() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("GlobalLightParameters");
        group.values.Clear();
        cat1 = "globalLight";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("activatedGlobalLightActivated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedGlobalLightActivated")));
        group.values.PushBack(SEUI_VarMapping("globalLightActivated", EEUIMT_VALUE, SEUI_ValueId(cat1, "globalLightActivated")));
        group.values.PushBack(SEUI_VarMapping("activatedActivatedFactorLightDir", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedActivatedFactorLightDir")));
        group.values.PushBack(SEUI_VarMapping("activatedFactorLightDir", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedFactorLightDir")));
        group.values.PushBack(SEUI_VarMapping("sunColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColor")));
        group.values.PushBack(SEUI_VarMapping("sunColorLightSide", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorLightSide")));
        group.values.PushBack(SEUI_VarMapping("sunColorLightOppositeSide", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorLightOppositeSide")));
        group.values.PushBack(SEUI_VarMapping("sunColorCenterArea", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorCenterArea")));
        group.values.PushBack(SEUI_VarMapping("sunColorSidesMargin", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorSidesMargin")));
        group.values.PushBack(SEUI_VarMapping("sunColorBottomHeight", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorBottomHeight")));
        group.values.PushBack(SEUI_VarMapping("sunColorTopHeight", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorTopHeight")));
        group.values.PushBack(SEUI_VarMapping("forcedLightDirAnglesYaw", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedLightDirAnglesYaw")));
        group.values.PushBack(SEUI_VarMapping("forcedLightDirAnglesPitch", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedLightDirAnglesPitch")));
        group.values.PushBack(SEUI_VarMapping("forcedLightDirAnglesRoll", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedLightDirAnglesRoll")));
        group.values.PushBack(SEUI_VarMapping("forcedSunDirAnglesYaw", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedSunDirAnglesYaw")));
        group.values.PushBack(SEUI_VarMapping("forcedSunDirAnglesPitch", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedSunDirAnglesPitch")));
        group.values.PushBack(SEUI_VarMapping("forcedSunDirAnglesRoll", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedSunDirAnglesRoll")));
        group.values.PushBack(SEUI_VarMapping("forcedMoonDirAnglesYaw", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedMoonDirAnglesYaw")));
        group.values.PushBack(SEUI_VarMapping("forcedMoonDirAnglesPitch", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedMoonDirAnglesPitch")));
        group.values.PushBack(SEUI_VarMapping("forcedMoonDirAnglesRoll", EEUIMT_VALUE, SEUI_ValueId(cat1, "forcedMoonDirAnglesRoll")));
        group.values.PushBack(SEUI_VarMapping("translucencyViewDependency", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyViewDependency")));
        group.values.PushBack(SEUI_VarMapping("translucencyBaseFlatness", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyBaseFlatness")));
        group.values.PushBack(SEUI_VarMapping("translucencyFlatBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyFlatBrightness")));
        group.values.PushBack(SEUI_VarMapping("translucencyGainBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyGainBrightness")));
        group.values.PushBack(SEUI_VarMapping("translucencyFresnelScaleLight", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyFresnelScaleLight")));
        group.values.PushBack(SEUI_VarMapping("translucencyFresnelScaleReflection", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyFresnelScaleReflection")));
        group.values.PushBack(SEUI_VarMapping("envProbeBaseLightingAmbient", EEUIMT_REF,, SEUI_ReferenceId("AmbientProbesGenParameters", "envProbeBaseLightingAmbient.")));
        group.values.PushBack(SEUI_VarMapping("envProbeBaseLightingReflection", EEUIMT_REF,, SEUI_ReferenceId("ReflectionProbesGenParameters", "envProbeBaseLightingReflection.")));
        group.values.PushBack(SEUI_VarMapping("charactersLightingBoostAmbientLight", EEUIMT_VALUE, SEUI_ValueId(cat1, "charactersLightingBoostAmbientLight")));
        group.values.PushBack(SEUI_VarMapping("charactersLightingBoostAmbientShadow", EEUIMT_VALUE, SEUI_ValueId(cat1, "charactersLightingBoostAmbientShadow")));
        group.values.PushBack(SEUI_VarMapping("charactersLightingBoostReflectionLight", EEUIMT_VALUE, SEUI_ValueId(cat1, "charactersLightingBoostReflectionLight")));
        group.values.PushBack(SEUI_VarMapping("charactersLightingBoostReflectionShadow", EEUIMT_VALUE, SEUI_ValueId(cat1, "charactersLightingBoostReflectionShadow")));
        group.values.PushBack(SEUI_VarMapping("charactersEyeBlicksColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "charactersEyeBlicksColor")));
        group.values.PushBack(SEUI_VarMapping("charactersEyeBlicksShadowedScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "charactersEyeBlicksShadowedScale")));
        group.values.PushBack(SEUI_VarMapping("envProbeAmbientScaleLight", EEUIMT_VALUE, SEUI_ValueId(cat1, "envProbeAmbientScaleLight")));
        group.values.PushBack(SEUI_VarMapping("envProbeAmbientScaleShadow", EEUIMT_VALUE, SEUI_ValueId(cat1, "envProbeAmbientScaleShadow")));
        group.values.PushBack(SEUI_VarMapping("envProbeReflectionScaleLight", EEUIMT_VALUE, SEUI_ValueId(cat1, "envProbeReflectionScaleLight")));
        group.values.PushBack(SEUI_VarMapping("envProbeReflectionScaleShadow", EEUIMT_VALUE, SEUI_ValueId(cat1, "envProbeReflectionScaleShadow")));
        group.values.PushBack(SEUI_VarMapping("envProbeDistantScaleFactor", EEUIMT_VALUE, SEUI_ValueId(cat1, "envProbeDistantScaleFactor")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("AmbientProbesGenParameters");
        group.values.Clear();
        cat1 = "globalLight";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("colorAmbient", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorAmbient")));
        group.values.PushBack(SEUI_VarMapping("colorSceneAdd", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSceneAdd")));
        group.values.PushBack(SEUI_VarMapping("colorSkyTop", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSkyTop")));
        group.values.PushBack(SEUI_VarMapping("colorSkyHorizon", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSkyHorizon")));
        group.values.PushBack(SEUI_VarMapping("skyShape", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyShape")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("ReflectionProbesGenParameters");
        group.values.Clear();
        cat1 = "globalLight";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("colorAmbient", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorAmbient")));
        group.values.PushBack(SEUI_VarMapping("colorSceneMul", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSceneMul")));
        group.values.PushBack(SEUI_VarMapping("colorSceneAdd", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSceneAdd")));
        group.values.PushBack(SEUI_VarMapping("colorSkyMul", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSkyMul")));
        group.values.PushBack(SEUI_VarMapping("colorSkyAdd", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorSkyAdd")));
        group.values.PushBack(SEUI_VarMapping("remapOffset", EEUIMT_VALUE, SEUI_ValueId(cat1, "remapOffset")));
        group.values.PushBack(SEUI_VarMapping("remapStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "remapStrength")));
        group.values.PushBack(SEUI_VarMapping("remapClamp", EEUIMT_VALUE, SEUI_ValueId(cat1, "remapClamp")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_interiorFallback() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("InteriorFallbackParameters");
        group.values.Clear();
        cat1 = "interiorFallback";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("colorAmbientMul", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorAmbientMul")));
        group.values.PushBack(SEUI_VarMapping("colorReflectionLow", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorReflectionLow")));
        group.values.PushBack(SEUI_VarMapping("colorReflectionMiddle", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorReflectionMiddle")));
        group.values.PushBack(SEUI_VarMapping("colorReflectionHigh", EEUIMT_VALUE, SEUI_ValueId(cat1, "colorReflectionHigh")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_speedTree() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("SpeedTreeParameters");
        group.values.Clear();
        cat1 = "speedTree";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("diffuse", EEUIMT_VALUE, SEUI_ValueId(cat1, "diffuse")));
        group.values.PushBack(SEUI_VarMapping("specularScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "specularScale")));
        group.values.PushBack(SEUI_VarMapping("translucencyScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "translucencyScale")));
        group.values.PushBack(SEUI_VarMapping("ambientOcclusionScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "ambientOcclusionScale")));
        group.values.PushBack(SEUI_VarMapping("billboardsColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "billboardsColor")));
        group.values.PushBack(SEUI_VarMapping("billboardsTranslucency", EEUIMT_VALUE, SEUI_ValueId(cat1, "billboardsTranslucency")));
        group.values.PushBack(SEUI_VarMapping("randomColorsTrees", EEUIMT_REF,, SEUI_ReferenceId("SpeedTreeRandomCol", "randomColorsTrees.")));
        group.values.PushBack(SEUI_VarMapping("randomColorsBranches", EEUIMT_REF,, SEUI_ReferenceId("SpeedTreeRandomCol", "randomColorsBranches.")));
        group.values.PushBack(SEUI_VarMapping("randomColorsGrass", EEUIMT_REF,, SEUI_ReferenceId("SpeedTreeRandomCol", "randomColorsGrass.")));
        group.values.PushBack(SEUI_VarMapping("randomColorsFallback", EEUIMT_VALUE, SEUI_ValueId(cat1, "randomColorsFallback")));
        group.values.PushBack(SEUI_VarMapping("pigmentBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "pigmentBrightness")));
        group.values.PushBack(SEUI_VarMapping("pigmentFloodStartDist", EEUIMT_VALUE, SEUI_ValueId(cat1, "pigmentFloodStartDist")));
        group.values.PushBack(SEUI_VarMapping("pigmentFloodRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "pigmentFloodRange")));
        group.values.PushBack(SEUI_VarMapping("billboardsLightBleed", EEUIMT_VALUE, SEUI_ValueId(cat1, "billboardsLightBleed")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("SpeedTreeRandomCol");
        group.values.Clear();
        cat1 = "speedTree";
        group.values.PushBack(SEUI_VarMapping("luminanceWeights", EEUIMT_VALUE, SEUI_ValueId(cat1, "luminanceWeights")));
        group.values.PushBack(SEUI_VarMapping("randomColor0", EEUIMT_VALUE, SEUI_ValueId(cat1, "randomColor0")));
        group.values.PushBack(SEUI_VarMapping("saturation0", EEUIMT_VALUE, SEUI_ValueId(cat1, "saturation0")));
        group.values.PushBack(SEUI_VarMapping("randomColor1", EEUIMT_VALUE, SEUI_ValueId(cat1, "randomColor1")));
        group.values.PushBack(SEUI_VarMapping("saturation1", EEUIMT_VALUE, SEUI_ValueId(cat1, "saturation1")));
        group.values.PushBack(SEUI_VarMapping("randomColor2", EEUIMT_VALUE, SEUI_ValueId(cat1, "randomColor2")));
        group.values.PushBack(SEUI_VarMapping("saturation2", EEUIMT_VALUE, SEUI_ValueId(cat1, "saturation2")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_toneMapping() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("ToneMappingParameters");
        group.values.Clear();
        cat1 = "toneMapping";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("skyLuminanceCustomValue", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyLuminanceCustomValue")));
        group.values.PushBack(SEUI_VarMapping("skyLuminanceCustomAmount", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyLuminanceCustomAmount")));
        group.values.PushBack(SEUI_VarMapping("luminanceLimitShape", EEUIMT_VALUE, SEUI_ValueId(cat1, "luminanceLimitShape")));
        group.values.PushBack(SEUI_VarMapping("luminanceLimitMin", EEUIMT_VALUE, SEUI_ValueId(cat1, "luminanceLimitMin")));
        group.values.PushBack(SEUI_VarMapping("luminanceLimitMax", EEUIMT_VALUE, SEUI_ValueId(cat1, "luminanceLimitMax")));
        group.values.PushBack(SEUI_VarMapping("rejectThreshold", EEUIMT_VALUE, SEUI_ValueId(cat1, "rejectThreshold")));
        group.values.PushBack(SEUI_VarMapping("rejectSmoothExtent", EEUIMT_VALUE, SEUI_ValueId(cat1, "rejectSmoothExtent")));
        group.values.PushBack(SEUI_VarMapping("newToneMapCurveParameters", EEUIMT_REF,, SEUI_ReferenceId("ToneMappingCurve", "newToneMapCurveParameters.")));
        group.values.PushBack(SEUI_VarMapping("newToneMapWhitepoint", EEUIMT_VALUE, SEUI_ValueId(cat1, "newToneMapWhitepoint")));
        group.values.PushBack(SEUI_VarMapping("newToneMapPostScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "newToneMapPostScale")));
        group.values.PushBack(SEUI_VarMapping("exposureScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "exposureScale")));
        group.values.PushBack(SEUI_VarMapping("postScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "postScale")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("ToneMappingCurve");
        group.values.Clear();
        cat1 = "toneMapping";
        group.values.PushBack(SEUI_VarMapping("shoulderStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "shoulderStrength")));
        group.values.PushBack(SEUI_VarMapping("linearStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "linearStrength")));
        group.values.PushBack(SEUI_VarMapping("linearAngle", EEUIMT_VALUE, SEUI_ValueId(cat1, "linearAngle")));
        group.values.PushBack(SEUI_VarMapping("toeStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "toeStrength")));
        group.values.PushBack(SEUI_VarMapping("toeNumerator", EEUIMT_VALUE, SEUI_ValueId(cat1, "toeNumerator")));
        group.values.PushBack(SEUI_VarMapping("toeDenominator", EEUIMT_VALUE, SEUI_ValueId(cat1, "toeDenominator")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_bloomNew() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("BloomNewParameters");
        group.values.Clear();
        cat1 = "bloomNew";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("brightPassWeights", EEUIMT_VALUE, SEUI_ValueId(cat1, "brightPassWeights")));
        group.values.PushBack(SEUI_VarMapping("color", EEUIMT_VALUE, SEUI_ValueId(cat1, "color")));
        group.values.PushBack(SEUI_VarMapping("dirtColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "dirtColor")));
        group.values.PushBack(SEUI_VarMapping("threshold", EEUIMT_VALUE, SEUI_ValueId(cat1, "threshold")));
        group.values.PushBack(SEUI_VarMapping("thresholdRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "thresholdRange")));
        group.values.PushBack(SEUI_VarMapping("brightnessMax", EEUIMT_VALUE, SEUI_ValueId(cat1, "brightnessMax")));
        group.values.PushBack(SEUI_VarMapping("shaftsColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "shaftsColor")));
        group.values.PushBack(SEUI_VarMapping("shaftsRadius", EEUIMT_VALUE, SEUI_ValueId(cat1, "shaftsRadius")));
        group.values.PushBack(SEUI_VarMapping("shaftsShapeExp", EEUIMT_VALUE, SEUI_ValueId(cat1, "shaftsShapeExp")));
        group.values.PushBack(SEUI_VarMapping("shaftsShapeInvSquare", EEUIMT_VALUE, SEUI_ValueId(cat1, "shaftsShapeInvSquare")));
        group.values.PushBack(SEUI_VarMapping("shaftsThreshold", EEUIMT_VALUE, SEUI_ValueId(cat1, "shaftsThreshold")));
        group.values.PushBack(SEUI_VarMapping("shaftsThresholdRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "shaftsThresholdRange")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_globalFog() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("GlobalFogParameters");
        group.values.Clear();
        cat1 = "globalFog";
        group.values.PushBack(SEUI_VarMapping("fogActivated", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogActivated")));
        group.values.PushBack(SEUI_VarMapping("fogAppearDistance", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogAppearDistance")));
        group.values.PushBack(SEUI_VarMapping("fogAppearRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogAppearRange")));
        group.values.PushBack(SEUI_VarMapping("fogColorFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogColorFront")));
        group.values.PushBack(SEUI_VarMapping("fogColorMiddle", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogColorMiddle")));
        group.values.PushBack(SEUI_VarMapping("fogColorBack", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogColorBack")));
        group.values.PushBack(SEUI_VarMapping("fogDensity", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogDensity")));
        group.values.PushBack(SEUI_VarMapping("fogFinalExp", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogFinalExp")));
        group.values.PushBack(SEUI_VarMapping("fogDistClamp", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogDistClamp")));
        group.values.PushBack(SEUI_VarMapping("fogVertOffset", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogVertOffset")));
        group.values.PushBack(SEUI_VarMapping("fogVertDensity", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogVertDensity")));
        group.values.PushBack(SEUI_VarMapping("fogVertDensityLightFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogVertDensityLightFront")));
        group.values.PushBack(SEUI_VarMapping("fogVertDensityLightBack", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogVertDensityLightBack")));
        group.values.PushBack(SEUI_VarMapping("fogSkyDensityScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogSkyDensityScale")));
        group.values.PushBack(SEUI_VarMapping("fogCloudsDensityScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCloudsDensityScale")));
        group.values.PushBack(SEUI_VarMapping("fogSkyVertDensityLightFrontScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogSkyVertDensityLightFrontScale")));
        group.values.PushBack(SEUI_VarMapping("fogSkyVertDensityLightBackScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogSkyVertDensityLightBackScale")));
        group.values.PushBack(SEUI_VarMapping("fogVertDensityRimRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogVertDensityRimRange")));
        group.values.PushBack(SEUI_VarMapping("fogCustomColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCustomColor")));
        group.values.PushBack(SEUI_VarMapping("fogCustomColorStart", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCustomColorStart")));
        group.values.PushBack(SEUI_VarMapping("fogCustomColorRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCustomColorRange")));
        group.values.PushBack(SEUI_VarMapping("fogCustomAmountScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCustomAmountScale")));
        group.values.PushBack(SEUI_VarMapping("fogCustomAmountScaleStart", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCustomAmountScaleStart")));
        group.values.PushBack(SEUI_VarMapping("fogCustomAmountScaleRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "fogCustomAmountScaleRange")));
        group.values.PushBack(SEUI_VarMapping("aerialColorFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "aerialColorFront")));
        group.values.PushBack(SEUI_VarMapping("aerialColorMiddle", EEUIMT_VALUE, SEUI_ValueId(cat1, "aerialColorMiddle")));
        group.values.PushBack(SEUI_VarMapping("aerialColorBack", EEUIMT_VALUE, SEUI_ValueId(cat1, "aerialColorBack")));
        group.values.PushBack(SEUI_VarMapping("aerialFinalExp", EEUIMT_VALUE, SEUI_ValueId(cat1, "aerialFinalExp")));
        group.values.PushBack(SEUI_VarMapping("ssaoImpactClamp", EEUIMT_VALUE, SEUI_ValueId(cat1, "ssaoImpactClamp")));
        group.values.PushBack(SEUI_VarMapping("ssaoImpactNearValue", EEUIMT_VALUE, SEUI_ValueId(cat1, "ssaoImpactNearValue")));
        group.values.PushBack(SEUI_VarMapping("ssaoImpactFarValue", EEUIMT_VALUE, SEUI_ValueId(cat1, "ssaoImpactFarValue")));
        group.values.PushBack(SEUI_VarMapping("ssaoImpactNearDistance", EEUIMT_VALUE, SEUI_ValueId(cat1, "ssaoImpactNearDistance")));
        group.values.PushBack(SEUI_VarMapping("ssaoImpactFarDistance", EEUIMT_VALUE, SEUI_ValueId(cat1, "ssaoImpactFarDistance")));
        group.values.PushBack(SEUI_VarMapping("distantLightsIntensityScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "distantLightsIntensityScale")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_sky() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("GlobalSkyParameters");
        group.values.Clear();
        cat1 = "sky";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("activatedActivateFactor", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedActivateFactor")));
        group.values.PushBack(SEUI_VarMapping("activateFactor", EEUIMT_VALUE, SEUI_ValueId(cat1, "activateFactor")));
        group.values.PushBack(SEUI_VarMapping("skyColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyColor")));
        group.values.PushBack(SEUI_VarMapping("skyColorHorizon", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyColorHorizon")));
        group.values.PushBack(SEUI_VarMapping("horizonVerticalAttenuation", EEUIMT_VALUE, SEUI_ValueId(cat1, "horizonVerticalAttenuation")));
        group.values.PushBack(SEUI_VarMapping("sunColorSky", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorSky")));
        group.values.PushBack(SEUI_VarMapping("sunColorSkyBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorSkyBrightness")));
        group.values.PushBack(SEUI_VarMapping("sunAreaSkySize", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunAreaSkySize")));
        group.values.PushBack(SEUI_VarMapping("sunColorHorizon", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorHorizon")));
        group.values.PushBack(SEUI_VarMapping("sunColorHorizonHorizontalScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColorHorizonHorizontalScale")));
        group.values.PushBack(SEUI_VarMapping("sunBackHorizonColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunBackHorizonColor")));
        group.values.PushBack(SEUI_VarMapping("sunInfluence", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunInfluence")));
        group.values.PushBack(SEUI_VarMapping("moonColorSky", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonColorSky")));
        group.values.PushBack(SEUI_VarMapping("moonColorSkyBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonColorSkyBrightness")));
        group.values.PushBack(SEUI_VarMapping("moonAreaSkySize", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonAreaSkySize")));
        group.values.PushBack(SEUI_VarMapping("moonColorHorizon", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonColorHorizon")));
        group.values.PushBack(SEUI_VarMapping("moonColorHorizonHorizontalScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonColorHorizonHorizontalScale")));
        group.values.PushBack(SEUI_VarMapping("moonBackHorizonColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonBackHorizonColor")));
        group.values.PushBack(SEUI_VarMapping("moonInfluence", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonInfluence")));
        group.values.PushBack(SEUI_VarMapping("globalSkyBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "globalSkyBrightness")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_depthOfField() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("DepthOfFieldParameters");
        group.values.Clear();
        cat1 = "depthOfField";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("nearBlurDist", EEUIMT_VALUE, SEUI_ValueId(cat1, "nearBlurDist")));
        group.values.PushBack(SEUI_VarMapping("nearFocusDist", EEUIMT_VALUE, SEUI_ValueId(cat1, "nearFocusDist")));
        group.values.PushBack(SEUI_VarMapping("farFocusDist", EEUIMT_VALUE, SEUI_ValueId(cat1, "farFocusDist")));
        group.values.PushBack(SEUI_VarMapping("farBlurDist", EEUIMT_VALUE, SEUI_ValueId(cat1, "farBlurDist")));
        group.values.PushBack(SEUI_VarMapping("intensity", EEUIMT_VALUE, SEUI_ValueId(cat1, "intensity")));
        group.values.PushBack(SEUI_VarMapping("activatedSkyThreshold", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedSkyThreshold")));
        group.values.PushBack(SEUI_VarMapping("skyThreshold", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyThreshold")));
        group.values.PushBack(SEUI_VarMapping("activatedSkyRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedSkyRange")));
        group.values.PushBack(SEUI_VarMapping("skyRange", EEUIMT_VALUE, SEUI_ValueId(cat1, "skyRange")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_colorModTransparency() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("ColorModTransparencyParameters");
        group.values.Clear();
        cat1 = "colorModTransparency";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("commonFarDist", EEUIMT_VALUE, SEUI_ValueId(cat1, "commonFarDist")));
        group.values.PushBack(SEUI_VarMapping("filterNearColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "filterNearColor")));
        group.values.PushBack(SEUI_VarMapping("filterFarColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "filterFarColor")));
        group.values.PushBack(SEUI_VarMapping("contrastNearStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "contrastNearStrength")));
        group.values.PushBack(SEUI_VarMapping("contrastFarStrength", EEUIMT_VALUE, SEUI_ValueId(cat1, "contrastFarStrength")));
        group.values.PushBack(SEUI_VarMapping("autoHideCustom0", EEUIMT_REF,, SEUI_ReferenceId("DistanceRangeParams", "autoHideCustom0.")));
        group.values.PushBack(SEUI_VarMapping("autoHideCustom1", EEUIMT_REF,, SEUI_ReferenceId("DistanceRangeParams", "autoHideCustom1.")));
        group.values.PushBack(SEUI_VarMapping("autoHideCustom2", EEUIMT_REF,, SEUI_ReferenceId("DistanceRangeParams", "autoHideCustom2.")));
        group.values.PushBack(SEUI_VarMapping("autoHideCustom3", EEUIMT_REF,, SEUI_ReferenceId("DistanceRangeParams", "autoHideCustom3.")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("DistanceRangeParams");
        group.values.Clear();
        cat1 = "colorModTransparency";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("distance", EEUIMT_VALUE, SEUI_ValueId(cat1, "distance")));
        group.values.PushBack(SEUI_VarMapping("range", EEUIMT_VALUE, SEUI_ValueId(cat1, "range")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_shadows() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("ShadowsParameters");
        group.values.Clear();
        cat1 = "shadows";
        group.values.PushBack(SEUI_VarMapping("activatedAutoHide", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedAutoHide")));
        group.values.PushBack(SEUI_VarMapping("autoHideBoxSizeMin", EEUIMT_VALUE, SEUI_ValueId(cat1, "autoHideBoxSizeMin")));
        group.values.PushBack(SEUI_VarMapping("autoHideBoxSizeMax", EEUIMT_VALUE, SEUI_ValueId(cat1, "autoHideBoxSizeMax")));
        group.values.PushBack(SEUI_VarMapping("autoHideBoxCompMaxX", EEUIMT_VALUE, SEUI_ValueId(cat1, "autoHideBoxCompMaxX")));
        group.values.PushBack(SEUI_VarMapping("autoHideBoxCompMaxY", EEUIMT_VALUE, SEUI_ValueId(cat1, "autoHideBoxCompMaxY")));
        group.values.PushBack(SEUI_VarMapping("autoHideBoxCompMaxZ", EEUIMT_VALUE, SEUI_ValueId(cat1, "autoHideBoxCompMaxZ")));
        group.values.PushBack(SEUI_VarMapping("autoHideDistScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "autoHideDistScale")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_water() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("WaterParameters");
        group.values.Clear();
        cat1 = "water";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("waterFlowIntensity", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterFlowIntensity")));
        group.values.PushBack(SEUI_VarMapping("underwaterBrightness", EEUIMT_VALUE, SEUI_ValueId(cat1, "underwaterBrightness")));
        group.values.PushBack(SEUI_VarMapping("underWaterFogIntensity", EEUIMT_VALUE, SEUI_ValueId(cat1, "underWaterFogIntensity")));
        group.values.PushBack(SEUI_VarMapping("waterColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterColor")));
        group.values.PushBack(SEUI_VarMapping("underWaterColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "underWaterColor")));
        group.values.PushBack(SEUI_VarMapping("waterFresnel", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterFresnel")));
        group.values.PushBack(SEUI_VarMapping("waterCaustics", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterCaustics")));
        group.values.PushBack(SEUI_VarMapping("waterFoamIntensity", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterFoamIntensity")));
        group.values.PushBack(SEUI_VarMapping("waterAmbientScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterAmbientScale")));
        group.values.PushBack(SEUI_VarMapping("waterDiffuseScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "waterDiffuseScale")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_colorGroups() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("ColorGroupsParameters");
        group.values.Clear();
        cat1 = "colorGroups";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("defaultGroup", EEUIMT_VALUE, SEUI_ValueId(cat1, "defaultGroup")));
        group.values.PushBack(SEUI_VarMapping("lightsDefault", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightsDefault")));
        group.values.PushBack(SEUI_VarMapping("lightsDawn", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightsDawn")));
        group.values.PushBack(SEUI_VarMapping("lightsNoon", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightsNoon")));
        group.values.PushBack(SEUI_VarMapping("lightsEvening", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightsEvening")));
        group.values.PushBack(SEUI_VarMapping("lightsNight", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightsNight")));
        group.values.PushBack(SEUI_VarMapping("fxDefault", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxDefault")));
        group.values.PushBack(SEUI_VarMapping("fxFire", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxFire")));
        group.values.PushBack(SEUI_VarMapping("fxFireFlares", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxFireFlares")));
        group.values.PushBack(SEUI_VarMapping("fxFireLight", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxFireLight")));
        group.values.PushBack(SEUI_VarMapping("fxSmoke", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSmoke")));
        group.values.PushBack(SEUI_VarMapping("fxSmokeExplosion", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSmokeExplosion")));
        group.values.PushBack(SEUI_VarMapping("fxSky", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSky")));
        group.values.PushBack(SEUI_VarMapping("fxSkyAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyAlpha")));
        group.values.PushBack(SEUI_VarMapping("fxSkyNight", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyNight")));
        group.values.PushBack(SEUI_VarMapping("fxSkyNightAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyNightAlpha")));
        group.values.PushBack(SEUI_VarMapping("fxSkyDawn", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyDawn")));
        group.values.PushBack(SEUI_VarMapping("fxSkyDawnAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyDawnAlpha")));
        group.values.PushBack(SEUI_VarMapping("fxSkyNoon", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyNoon")));
        group.values.PushBack(SEUI_VarMapping("fxSkyNoonAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyNoonAlpha")));
        group.values.PushBack(SEUI_VarMapping("fxSkySunset", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkySunset")));
        group.values.PushBack(SEUI_VarMapping("fxSkySunsetAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkySunsetAlpha")));
        group.values.PushBack(SEUI_VarMapping("fxSkyRain", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyRain")));
        group.values.PushBack(SEUI_VarMapping("fxSkyRainAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxSkyRainAlpha")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsMiddle", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsMiddle")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsMiddleAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsMiddleAlpha")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsFront")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsFrontAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsFrontAlpha")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsBack", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsBack")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsBackAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsBackAlpha")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsRim", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsRim")));
        group.values.PushBack(SEUI_VarMapping("mainCloudsRimAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "mainCloudsRimAlpha")));
        group.values.PushBack(SEUI_VarMapping("backgroundCloudsFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundCloudsFront")));
        group.values.PushBack(SEUI_VarMapping("backgroundCloudsFrontAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundCloudsFrontAlpha")));
        group.values.PushBack(SEUI_VarMapping("backgroundCloudsBack", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundCloudsBack")));
        group.values.PushBack(SEUI_VarMapping("backgroundCloudsBackAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundCloudsBackAlpha")));
        group.values.PushBack(SEUI_VarMapping("backgroundHazeFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundHazeFront")));
        group.values.PushBack(SEUI_VarMapping("backgroundHazeFrontAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundHazeFrontAlpha")));
        group.values.PushBack(SEUI_VarMapping("backgroundHazeBack", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundHazeBack")));
        group.values.PushBack(SEUI_VarMapping("backgroundHazeBackAlpha", EEUIMT_VALUE, SEUI_ValueId(cat1, "backgroundHazeBackAlpha")));
        group.values.PushBack(SEUI_VarMapping("fxBlood", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxBlood")));
        group.values.PushBack(SEUI_VarMapping("fxWater", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxWater")));
        group.values.PushBack(SEUI_VarMapping("fxFog", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxFog")));
        group.values.PushBack(SEUI_VarMapping("fxTrails", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxTrails")));
        group.values.PushBack(SEUI_VarMapping("fxScreenParticles", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxScreenParticles")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaft", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaft")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftSun", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftSun")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftInteriorDawn", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftInteriorDawn")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftSpotlightDawn", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftSpotlightDawn")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftReflectionLightDawn", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftReflectionLightDawn")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftInteriorNoon", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftInteriorNoon")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftSpotlightNoon", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftSpotlightNoon")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftReflectionLightNoon", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftReflectionLightNoon")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftInteriorEvening", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftInteriorEvening")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftSpotlightEvening", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftSpotlightEvening")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftReflectionLightEvening", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftReflectionLightEvening")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftInteriorNight", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftInteriorNight")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftSpotlightNight", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftSpotlightNight")));
        group.values.PushBack(SEUI_VarMapping("fxLightShaftReflectionLightNight", EEUIMT_VALUE, SEUI_ValueId(cat1, "fxLightShaftReflectionLightNight")));
        group.values.PushBack(SEUI_VarMapping("activatedCustom0", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedCustom0")));
        group.values.PushBack(SEUI_VarMapping("customGroup0", EEUIMT_VALUE, SEUI_ValueId(cat1, "customGroup0")));
        group.values.PushBack(SEUI_VarMapping("activatedCustom1", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedCustom1")));
        group.values.PushBack(SEUI_VarMapping("customGroup1", EEUIMT_VALUE, SEUI_ValueId(cat1, "customGroup1")));
        group.values.PushBack(SEUI_VarMapping("activatedCustom2", EEUIMT_VALUE, SEUI_ValueId(cat1, "activatedCustom2")));
        group.values.PushBack(SEUI_VarMapping("customGroup2", EEUIMT_VALUE, SEUI_ValueId(cat1, "customGroup2")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_flareColorGroups() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("FlareColorGroupsParameters");
        group.values.Clear();
        cat1 = "flareColorGroups";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("default", EEUIMT_REF,, SEUI_ReferenceId("FlareColors", "custom0.")));
        group.values.PushBack(SEUI_VarMapping("custom0", EEUIMT_REF,, SEUI_ReferenceId("FlareColors", "custom1.")));
        group.values.PushBack(SEUI_VarMapping("custom1", EEUIMT_REF,, SEUI_ReferenceId("FlareColors", "custom2.")));
        group.values.PushBack(SEUI_VarMapping("custom2", EEUIMT_REF,, SEUI_ReferenceId("FlareColors", "custom3.")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("FlareColors");
        group.values.Clear();
        cat1 = "flareColorGroups";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("color0", EEUIMT_VALUE, SEUI_ValueId(cat1, "color0")));
        group.values.PushBack(SEUI_VarMapping("opacity0", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity0")));
        group.values.PushBack(SEUI_VarMapping("color1", EEUIMT_VALUE, SEUI_ValueId(cat1, "color1")));
        group.values.PushBack(SEUI_VarMapping("opacity1", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity1")));
        group.values.PushBack(SEUI_VarMapping("color2", EEUIMT_VALUE, SEUI_ValueId(cat1, "color2")));
        group.values.PushBack(SEUI_VarMapping("opacity2", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity2")));
        group.values.PushBack(SEUI_VarMapping("color3", EEUIMT_VALUE, SEUI_ValueId(cat1, "color3")));
        group.values.PushBack(SEUI_VarMapping("opacity3", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity3")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_sunAndMoonParams() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("SunAndMoonParameters");
        group.values.Clear();
        cat1 = "sunAndMoonParams";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("sunSize", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunSize")));
        group.values.PushBack(SEUI_VarMapping("sunColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunColor")));
        group.values.PushBack(SEUI_VarMapping("sunFlareSize", EEUIMT_VALUE, SEUI_ValueId(cat1, "sunFlareSize")));
        group.values.PushBack(SEUI_VarMapping("sunFlareColor", EEUIMT_REF,, SEUI_ReferenceId("SMFlareColors", "sunFlareColor.")));
        group.values.PushBack(SEUI_VarMapping("moonSize", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonSize")));
        group.values.PushBack(SEUI_VarMapping("moonColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonColor")));
        group.values.PushBack(SEUI_VarMapping("moonFlareSize", EEUIMT_VALUE, SEUI_ValueId(cat1, "moonFlareSize")));
        group.values.PushBack(SEUI_VarMapping("moonFlareColor", EEUIMT_REF,, SEUI_ReferenceId("SMFlareColors", "moonFlareColor.")));
        mappings.PushBack(group);

        // requires id prefix (duplicated cause of different cat1)
        group = SEUI_ClassMapping("SMFlareColors");
        group.values.Clear();
        cat1 = "sunAndMoonParams";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("color0", EEUIMT_VALUE, SEUI_ValueId(cat1, "color0")));
        group.values.PushBack(SEUI_VarMapping("opacity0", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity0")));
        group.values.PushBack(SEUI_VarMapping("color1", EEUIMT_VALUE, SEUI_ValueId(cat1, "color1")));
        group.values.PushBack(SEUI_VarMapping("opacity1", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity1")));
        group.values.PushBack(SEUI_VarMapping("color2", EEUIMT_VALUE, SEUI_ValueId(cat1, "color2")));
        group.values.PushBack(SEUI_VarMapping("opacity2", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity2")));
        group.values.PushBack(SEUI_VarMapping("color3", EEUIMT_VALUE, SEUI_ValueId(cat1, "color3")));
        group.values.PushBack(SEUI_VarMapping("opacity3", EEUIMT_VALUE, SEUI_ValueId(cat1, "opacity3")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_windParams() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("WindParameters");
        group.values.Clear();
        cat1 = "windParams";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("windStrengthOverride", EEUIMT_VALUE, SEUI_ValueId(cat1, "windStrengthOverride")));
        group.values.PushBack(SEUI_VarMapping("cloudsVelocityOverride", EEUIMT_VALUE, SEUI_ValueId(cat1, "cloudsVelocityOverride")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_gameplayEffects() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("GameplayEffectsParameters");
        group.values.Clear();
        cat1 = "gameplayEffects";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("catEffectBrightnessMultiply", EEUIMT_VALUE, SEUI_ValueId(cat1, "catEffectBrightnessMultiply")));
        group.values.PushBack(SEUI_VarMapping("behaviorAnimationMultiplier", EEUIMT_VALUE, SEUI_ValueId(cat1, "behaviorAnimationMultiplier")));
        group.values.PushBack(SEUI_VarMapping("specularityMultiplier", EEUIMT_VALUE, SEUI_ValueId(cat1, "specularityMultiplier")));
        group.values.PushBack(SEUI_VarMapping("glossinessMultiplier", EEUIMT_VALUE, SEUI_ValueId(cat1, "glossinessMultiplier")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_motionBlur() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("MotionBlurParameters");
        group.values.Clear();
        cat1 = "motionBlur";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("strength", EEUIMT_VALUE, SEUI_ValueId(cat1, "strength")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_cameraLightsSetup() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("CameraLightsSetupParameters");
        group.values.Clear();
        cat1 = "cameraLightsSetup";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("gameplayLight0", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "gameplayLight0.")));
        group.values.PushBack(SEUI_VarMapping("gameplayLight1", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "gameplayLight1.")));
        group.values.PushBack(SEUI_VarMapping("sceneLight0", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "sceneLight0.")));
        group.values.PushBack(SEUI_VarMapping("sceneLight1", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "sceneLight1.")));
        group.values.PushBack(SEUI_VarMapping("dialogLight0", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "dialogLight0.")));
        group.values.PushBack(SEUI_VarMapping("dialogLight1", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "dialogLight1.")));
        group.values.PushBack(SEUI_VarMapping("interiorLight0", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "interiorLight0.")));
        group.values.PushBack(SEUI_VarMapping("interiorLight1", EEUIMT_REF,, SEUI_ReferenceId("CameraLights", "interiorLight1.")));
        group.values.PushBack(SEUI_VarMapping("playerInInteriorLightsScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "playerInInteriorLightsScale")));
        group.values.PushBack(SEUI_VarMapping("sceneLightColorInterior0", EEUIMT_VALUE, SEUI_ValueId(cat1, "sceneLightColorInterior0")));
        group.values.PushBack(SEUI_VarMapping("sceneLightColorInterior1", EEUIMT_VALUE, SEUI_ValueId(cat1, "sceneLightColorInterior1")));
        group.values.PushBack(SEUI_VarMapping("cameraLightsNonCharacterScale", EEUIMT_VALUE, SEUI_ValueId(cat1, "cameraLightsNonCharacterScale")));
        mappings.PushBack(group);

        // requires id prefix
        group = SEUI_ClassMapping("CameraLights");
        group.values.Clear();
        cat1 = "cameraLightsSetup";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("color", EEUIMT_VALUE, SEUI_ValueId(cat1, "color")));
        group.values.PushBack(SEUI_VarMapping("attenuation", EEUIMT_VALUE, SEUI_ValueId(cat1, "attenuation")));
        group.values.PushBack(SEUI_VarMapping("radius", EEUIMT_VALUE, SEUI_ValueId(cat1, "radius")));
        group.values.PushBack(SEUI_VarMapping("offsetFront", EEUIMT_VALUE, SEUI_ValueId(cat1, "offsetFront")));
        group.values.PushBack(SEUI_VarMapping("offsetRight", EEUIMT_VALUE, SEUI_ValueId(cat1, "offsetRight")));
        group.values.PushBack(SEUI_VarMapping("offsetUp", EEUIMT_VALUE, SEUI_ValueId(cat1, "offsetUp")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    private function add_dialogLightParams() {
        var group: SEUI_ClassMapping;
        var cat1: String;

        group = SEUI_ClassMapping("DialogLightParameters");
        group.values.Clear();
        cat1 = "dialogLightParams";
        group.values.PushBack(SEUI_VarMapping("activated", EEUIMT_VALUE, SEUI_ValueId(cat1, "activated")));
        group.values.PushBack(SEUI_VarMapping("lightColor", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightColor")));
        group.values.PushBack(SEUI_VarMapping("lightColor2", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightColor2")));
        group.values.PushBack(SEUI_VarMapping("lightColor3", EEUIMT_VALUE, SEUI_ValueId(cat1, "lightColor3")));
        mappings.PushBack(group);
    }
    // ------------------------------------------------------------------------
    public function get(groupRef: SEUI_ReferenceId) : SEUI_ClassMapping {
        var i, s: int;

        s = mappings.Size();
        for (i = 0; i < s; i += 1) {
            if (mappings[i].id == groupRef.id) {
                return mappings[i];
            }
        }

        return SEUI_ClassMapping();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModEnvUiDefinitionWriter extends CModUiFilteredList {
    // ------------------------------------------------------------------------
    private var mapper: CModEnvUiDefinitionMapper;
    // ------------------------------------------------------------------------
    public function init() {
        mapper = new CModEnvUiDefinitionMapper in this;
        mapper.init();
    }
    // ------------------------------------------------------------------------
    private function encodeGroup(groupId: SEUI_ReferenceId, env: CModEnvUiParams) : SEncValue {
        var group, value: SEncValue;
        var i, s: int;
        var mapping: SEUI_ClassMapping;
        var attributeMapping: SEUI_VarMapping;

        group = encValueNewMap();

        mapping = mapper.get(groupId);
        s = mapping.values.Size();
        for (i = 0; i < s; i += 1) {
            attributeMapping = mapping.values[i];

            if (attributeMapping.type == EEUIMT_REF) {
                value = encodeGroup(attributeMapping.reference, env);
            } else {
                attributeMapping.value.id = groupId.valueIdPrefix + attributeMapping.value.id;
                value = encodeValue(attributeMapping.value, env);
            }
            encMapPush(attributeMapping.id, value, group);
        }
        if (group.m.Size() > 0) {
            return group;
        } else {
            return SEncValue(EEVT_Null);
        }
    }
    // ------------------------------------------------------------------------
    private function encodeValue(valueId: SEUI_ValueId, env: CModEnvUiParams) : SEncValue {
        var value: SEUI_Value;

        value = env.getValue(valueId.cat, valueId.id);

        switch (value.type) {
            case EEUI_CURVE:
                if (value.curve.values.Size() > 0) { return encodeCurve(value.curve); } else { return SEncValue(EEVT_Null); }
            case EEUI_FLOAT: return FloatToEncValue(value.f);
            case EEUI_BOOL: return BoolToEncValue(value.b);
            case EEUI_INT: return IntToEncValue(value.i);
        }

        LogChannel('W3ENV', "found INVALID value type [" + value.type + "] for " + valueId.cat + ":" + valueId.id);
        return SEncValue();
    }
    // ------------------------------------------------------------------------
    private function timeToString(time: float) : String {
        var hours: int = FloorF(time * 24.0);
        var sec, mins: int;
        var h, m : String;

        //sec = FloorF(ModF(time * 86400.0, 60));
        mins = FloorF(ModF(time * 1440.0, 60));
        if (hours < 10) { h = "0" + IntToString(hours); } else { h = IntToString(hours); }
        if (mins < 10)  { m = "0" + IntToString(mins); } else { m = IntToString(mins); }

        return h + ":" + m;
    }
    // ------------------------------------------------------------------------
    private function encodeCurve(curve: SEUI_Curve) : SEncValue {
        var emptyPoint: Vector = Vector(-0.1, 0, 0.1, 0);
        var hasPoint: bool;
        var c, points, d, p: SEncValue;
        var i, s: int;
        var cp: SCurveDataEntry;
        var zero: Uint16;

        c = encValueNewMap();
        if (curve.type != SCT_Float) {
            encMapPush("curveType", StrToEncValue(curve.type), c);
        }
        if (curve.scale != 1) {
            encMapPush("ScalarEditScale", FloatToEncValue(curve.scale), c);
        }
        if (curve.origin != 0) {
            encMapPush("ScalarEditOrigin", FloatToEncValue(curve.origin), c);
        }
        if (curve.baseType != CT_Linear) {
            encMapPush("baseType", StrToEncValue(curve.baseType), c);
        }

        points = encValueNewMap();
        s = curve.values.Size();
        for (i = 0; i < s; i += 1) {
            cp = curve.values[i];

            // this is a heuristic: log either controlpoint or value
            hasPoint = cp.ntrolPoint != emptyPoint;

            //LogChannel('test', cp.rveTypeL);

            if (cp.rveTypeL != zero || cp.rveTypeR != zero) {
                // log everything
                d = encValueNewMap();

                if (hasPoint) {
                    p = encValueNewList();
                    encListPush(FloatToEncValue(cp.ntrolPoint.X), p);
                    encListPush(FloatToEncValue(cp.ntrolPoint.Y), p);
                    encListPush(FloatToEncValue(cp.ntrolPoint.Z), p);
                    encListPush(FloatToEncValue(cp.ntrolPoint.W), p);

                    encMapPush("point", p, d);
                }
                //FIXME
                //d.m.PushBack("curveTypeL", IntToEncValue(cp.rveTypeL));
                //d.m.PushBack("curveTypeR", IntToEncValue(cp.rveTypeR));

                encMapPush("value", FloatToEncValue(cp.lue), d);
            } else {
                if (hasPoint) {
                    d = encValueNewList();
                    encListPush(FloatToEncValue(cp.ntrolPoint.X), d);
                    encListPush(FloatToEncValue(cp.ntrolPoint.Y), d);
                    encListPush(FloatToEncValue(cp.ntrolPoint.Z), d);
                    encListPush(FloatToEncValue(cp.ntrolPoint.W), d);
                } else {
                    d = FloatToEncValue(cp.lue);
                }
            }
            encMapPush(timeToString(cp.me), d, points);
        }
        encMapPush("curve", points, c);
        return c;
    }
    // ------------------------------------------------------------------------
    public function logDefinition(env: CModEnvUiParams) {
        var definitionWriter: CRadishDefinitionWriter;
        var defs, root: SEncValue;
        var i: int;

        root = encValueNewMap();
        encMapPush("environment", encodeGroup(SEUI_ReferenceId("environment"), env), root);

        definitionWriter = new CRadishDefinitionWriter in this;
        definitionWriter.create('W3ENV', "Env Ui", root);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
