// ----------------------------------------------------------------------------
// reason for so much copy n paste:
// the game crashes if var s: SSimpleCurve is used in a called function. to
// circumvent this bug code like:
//
//    case "activated":   return toBool(env.envParams.m_finalColorBalance.activated);
//    //...
//    case "vignetteWeights": return toCurve(env.envParams.m_finalColorBalance.vignetteWeights);
//    case "vignetteColor":   return toCurve(env.envParams.m_finalColorBalance.vignetteColor);
//    //...
//
// has to be "inlined" and looks like this:
//
//    case "activated":   return toBool(env.envParams.m_finalColorBalance.activated);
//    //...
//    case "vignetteWeights":
//        s=env.envParams.m_finalColorBalance.vignetteWeights.dataCurveValues.Size();
//        for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.vignetteWeights.dataCurveValues[i]);}
//        return toCurve(c,
//            env.envParams.m_finalColorBalance.vignetteWeights.ScalarEditScale,
//            env.envParams.m_finalColorBalance.vignetteWeights.ScalarEditOrigin,
//            env.envParams.m_finalColorBalance.vignetteWeights.dataBaseType,
//            env.envParams.m_finalColorBalance.vignetteWeights.CurveType);
//    case "vignetteColor":
//        s=env.envParams.m_finalColorBalance.vignetteColor.dataCurveValues.Size();
//        for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.vignetteColor.dataCurveValues[i]);}
//        return toCurve(c,
//            env.envParams.m_finalColorBalance.vignetteColor.ScalarEditScale,
//            env.envParams.m_finalColorBalance.vignetteColor.ScalarEditOrigin);
//            env.envParams.m_finalColorBalance.vignetteColor.dataBaseType);
//            env.envParams.m_finalColorBalance.vignetteColor.CurveType);
//    //...
//
// ----------------------------------------------------------------------------
enum EEUI_AdjustType {
    EEUI_SCALAR = 0,
    EEUI_VEC1 = 1,
    EEUI_VEC2 = 2,
    EEUI_VEC3 = 3,
    EEUI_VEC4 = 4,
}
struct SEUI_Curve {
    var values: array<SCurveDataEntry>;
    // selected point
    var p: int;
    var scale: Float;
    var origin: Float;
    var baseType: ECurveBaseType;
    var type: ESimpleCurveType;
}
// pseudo union
struct SEUI_Value {
    var type: EEUI_ValueType;
    var curve: SEUI_Curve;
    var f: Float;
    var b: Bool;
    var i: Int;
}
enum EEUI_ValueType {
    EEUI_CURVE = 1,
    EEUI_FLOAT = 2,
    EEUI_BOOL = 3,
    EEUI_INT = 4
}
// ----------------------------------------------------------------------------
class CModEnvUiParams {
    // ------------------------------------------------------------------------
    private var env: CEnvironmentDefinition;
    // ------------------------------------------------------------------------
    public function setEnv(newEnv: CEnvironmentDefinition) {
        var world: CGameWorld;
        world = (CGameWorld)theGame.GetWorld();

        env = newEnv;
        world.environmentParameters.environmentDefinition = newEnv;
    }
    // ------------------------------------------------------------------------
    private function timeToString(time: float) : String {
        var hours: int = FloorF(time * 24.0);
        var sec, mins: int;
        var h, m, s: String;

        sec = FloorF(ModF(time * 86400.0, 60));
        mins = FloorF(ModF(time * 1440.0, 60));

        if (sec < 10) { s = "0" + IntToString(sec); } else { s = IntToString(sec); }
        if (mins < 10) { m = "0" + IntToString(mins); } else { m = IntToString(mins); }
        if (hours < 10) { h = "0" + IntToString(hours); } else { h = IntToString(hours); }

        return h + ":" + m + ":" + s;
    }
    // ------------------------------------------------------------------------
    private function timeToGameTime(time: float) : GameTime {
        return GameTimeCreateFromGameSeconds(FloorF(time * 86400.0));
    }
    // ------------------------------------------------------------------------
    private function valueToHex(v: int) : String {
        switch (v % 16) {
            case 10: return 'a';
            case 11: return 'b';
            case 12: return 'c';
            case 13: return 'd';
            case 14: return 'e';
            case 15: return 'f';
            default: return IntToString(v % 16);
        }
    }
    // ------------------------------------------------------------------------
    private function floatToHex(v: float) : String {
        var i: int;
        // approx...
        i = RoundF(ClampF(v, 0.0, 255.0));
        return valueToHex(i / 16) + valueToHex(i);
    }
    // ------------------------------------------------------------------------
    private function pointToHtmlCol(v: Vector) : String {
        return "<font color=\"#"
            + floatToHex(v.X)
            + floatToHex(v.Y)
            + floatToHex(v.Z)
            + "\">&#9751;</font>";
    }
    // ------------------------------------------------------------------------
    private function formatFloatPadded(v: float, precision: int) : String {
        var str: String;

        str = FloatToStringPrec(v, precision);
        if (StrFindFirst(str, ".") < 0) {
            str += ".0";
        }
        if (v < 10) {
            return "  " + str;
        } else if (v < 100) {
            return " " + str;
        } else {
            return str;
        }
    }
    // ------------------------------------------------------------------------
    private function formatCurveValue(ctype: ESimpleCurveType, cp: SCurveDataEntry) : String {
        switch (ctype) {
            case SCT_Float: return NoTrailZeros(cp.lue);

            case SCT_Color:
                return pointToHtmlCol(cp.ntrolPoint) + "  "
                    + formatFloatPadded(cp.ntrolPoint.X, 1) + " / "
                    + formatFloatPadded(cp.ntrolPoint.Y, 1) + " / "
                    + formatFloatPadded(cp.ntrolPoint.Z, 1) + " / "
                    + formatFloatPadded(cp.ntrolPoint.W, 2);

            case SCT_Vector:
                return formatFloatPadded(cp.ntrolPoint.X, 3) + " / "
                    + formatFloatPadded(cp.ntrolPoint.Y, 3) + " / "
                    + formatFloatPadded(cp.ntrolPoint.Z, 3) + " / "
                    + formatFloatPadded(cp.ntrolPoint.W, 3);

            //case SCT_ColorScaled:
            default:
                return formatFloatPadded(cp.lue, 3) + "   "
                    + pointToHtmlCol(cp.ntrolPoint) + "  "
                    + formatFloatPadded(cp.ntrolPoint.X, 1) + " / "
                    + formatFloatPadded(cp.ntrolPoint.Y, 1) + " / "
                    + formatFloatPadded(cp.ntrolPoint.Z, 1) + " / "
                    + formatFloatPadded(cp.ntrolPoint.W, 2);
        }
    }
    // ------------------------------------------------------------------------
    private function formatCurveTypeLabel(ctype: ESimpleCurveType) : String {
        switch (ctype) {
            case SCT_Float: return GetLocStringByKeyExt("EUI_lScalarValue");
            case SCT_Color: return GetLocStringByKeyExt("EUI_lColorValue");
            case SCT_Vector: return GetLocStringByKeyExt("EUI_lVectorValue");
            case SCT_ColorScaled: return GetLocStringByKeyExt("EUI_lScaledColorValue");
        }
    }
    // ------------------------------------------------------------------------
    private function formatValue(v: SEUI_Value) : String {
        var s: string;
        var cp: SCurveDataEntry;

        s = GetLocStringByKeyExt("EUI_lGenericValue") + " ";

        switch (v.type) {
            case EEUI_CURVE:
                s = formatCurveTypeLabel(v.curve.type) + " ";
                if (v.curve.values.Size() > 0) {
                    cp = v.curve.values[v.curve.p];
                    s += IntToString(v.curve.p + 1) + "/"
                        + IntToString(v.curve.values.Size()) + " | "
                        + timeToString(cp.me) + "   "
                        + formatCurveValue(v.curve.type, cp);
                } else {
                    s += GetLocStringByKeyExt("EUI_lNoCurveValues");
                }
                break;

            case EEUI_FLOAT: s += NoTrailZeros(v.f); break;
            case EEUI_BOOL: s += v.b; break;
            case EEUI_INT: s += IntToString(v.i); break;
            default:
                s = "";
        }
        return s;
    }
    // ------------------------------------------------------------------------
    private function toBool(value: bool) : SEUI_Value {
        return SEUI_Value(EEUI_BOOL, , , value);
    }
    // ------------------------------------------------------------------------
    private function toFloat(value: float) : SEUI_Value {
        return SEUI_Value(EEUI_FLOAT, , value);
    }
    // ------------------------------------------------------------------------
    private function toInt(value: int) : SEUI_Value {
        return SEUI_Value(EEUI_INT, , , , value);
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    private function toCurve(values: array<SCurveDataEntry>,
        optional scale: Float,
        optional origin: Float,
        optional baseType: ECurveBaseType,
        optional curveType: ESimpleCurveType) : SEUI_Value
    {
        var gt: GameTime;
        var bestDelta: float = 1;
        var now, delta: float;
        var i, p: int;
        var s: int = values.Size();

        gt = theGame.GetGameTime();
        now = (GameTimeHours(gt) * 3600
            + GameTimeMinutes(gt) * 60
            + GameTimeSeconds(gt));
        now /= 86400;

        // select available point closest to current game time
        for (i = 0; i < s; i += 1) {
            delta = AbsF(values[i].me - now);
            if (delta < bestDelta) {
                p = i;
                bestDelta = delta;
            }
        }

        return SEUI_Value(EEUI_CURVE, SEUI_Curve(
            values, p, scale, origin, baseType, curveType
        ));
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    private function get_m_finalColorBalance(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":               return toBool(env.envParams.m_finalColorBalance.activated);
            case "activatedBalanceMap":     return toBool(env.envParams.m_finalColorBalance.activatedBalanceMap);
            case "activatedParametricBalance":  return toBool(env.envParams.m_finalColorBalance.activatedParametricBalance);

            case "vignetteWeights":
                s=env.envParams.m_finalColorBalance.vignetteWeights.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.vignetteWeights.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.vignetteWeights.ScalarEditScale,
                    env.envParams.m_finalColorBalance.vignetteWeights.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.vignetteWeights.dataBaseType,
                    env.envParams.m_finalColorBalance.vignetteWeights.CurveType);

            case "vignetteColor":
                s=env.envParams.m_finalColorBalance.vignetteColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.vignetteColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.vignetteColor.ScalarEditScale,
                    env.envParams.m_finalColorBalance.vignetteColor.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.vignetteColor.dataBaseType,
                    env.envParams.m_finalColorBalance.vignetteColor.CurveType);

            case "vignetteOpacity":
                s=env.envParams.m_finalColorBalance.vignetteOpacity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.vignetteOpacity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.vignetteOpacity.ScalarEditScale,
                    env.envParams.m_finalColorBalance.vignetteOpacity.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.vignetteOpacity.dataBaseType,
                    env.envParams.m_finalColorBalance.vignetteOpacity.CurveType);

            case "chromaticAberrationSize":
                s=env.envParams.m_finalColorBalance.chromaticAberrationSize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.chromaticAberrationSize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.chromaticAberrationSize.ScalarEditScale,
                    env.envParams.m_finalColorBalance.chromaticAberrationSize.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.chromaticAberrationSize.dataBaseType,
                    env.envParams.m_finalColorBalance.chromaticAberrationSize.CurveType);

            case "balanceMapLerp":
                s=env.envParams.m_finalColorBalance.balanceMapLerp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.balanceMapLerp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.balanceMapLerp.ScalarEditScale,
                    env.envParams.m_finalColorBalance.balanceMapLerp.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.balanceMapLerp.dataBaseType,
                    env.envParams.m_finalColorBalance.balanceMapLerp.CurveType);

            case "balanceMapAmount":
                s=env.envParams.m_finalColorBalance.balanceMapAmount.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.balanceMapAmount.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.balanceMapAmount.ScalarEditScale,
                    env.envParams.m_finalColorBalance.balanceMapAmount.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.balanceMapAmount.dataBaseType,
                    env.envParams.m_finalColorBalance.balanceMapAmount.CurveType);

            case "balancePostBrightness":
                s=env.envParams.m_finalColorBalance.balancePostBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.balancePostBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.balancePostBrightness.ScalarEditScale,
                    env.envParams.m_finalColorBalance.balancePostBrightness.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.balancePostBrightness.dataBaseType,
                    env.envParams.m_finalColorBalance.balancePostBrightness.CurveType);

            case "levelsShadows":
                s=env.envParams.m_finalColorBalance.levelsShadows.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.levelsShadows.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.levelsShadows.ScalarEditScale,
                    env.envParams.m_finalColorBalance.levelsShadows.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.levelsShadows.dataBaseType,
                    env.envParams.m_finalColorBalance.levelsShadows.CurveType);

            case "levelsMidtones":
                s=env.envParams.m_finalColorBalance.levelsMidtones.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.levelsMidtones.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.levelsMidtones.ScalarEditScale,
                    env.envParams.m_finalColorBalance.levelsMidtones.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.levelsMidtones.dataBaseType,
                    env.envParams.m_finalColorBalance.levelsMidtones.CurveType);

            case "levelsHighlights":
                s=env.envParams.m_finalColorBalance.levelsHighlights.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.levelsHighlights.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.levelsHighlights.ScalarEditScale,
                    env.envParams.m_finalColorBalance.levelsHighlights.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.levelsHighlights.dataBaseType,
                    env.envParams.m_finalColorBalance.levelsHighlights.CurveType);

            case "midtoneRangeMin":
                s=env.envParams.m_finalColorBalance.midtoneRangeMin.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.midtoneRangeMin.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.midtoneRangeMin.ScalarEditScale,
                    env.envParams.m_finalColorBalance.midtoneRangeMin.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.midtoneRangeMin.dataBaseType,
                    env.envParams.m_finalColorBalance.midtoneRangeMin.CurveType);

            case "midtoneRangeMax":
                s=env.envParams.m_finalColorBalance.midtoneRangeMax.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.midtoneRangeMax.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.midtoneRangeMax.ScalarEditScale,
                    env.envParams.m_finalColorBalance.midtoneRangeMax.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.midtoneRangeMax.dataBaseType,
                    env.envParams.m_finalColorBalance.midtoneRangeMax.CurveType);

            case "midtoneMarginMin":
                s=env.envParams.m_finalColorBalance.midtoneMarginMin.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.midtoneMarginMin.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.midtoneMarginMin.ScalarEditScale,
                    env.envParams.m_finalColorBalance.midtoneMarginMin.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.midtoneMarginMin.dataBaseType,
                    env.envParams.m_finalColorBalance.midtoneMarginMin.CurveType);

            case "midtoneMarginMax":
                s=env.envParams.m_finalColorBalance.midtoneMarginMax.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.midtoneMarginMax.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.midtoneMarginMax.ScalarEditScale,
                    env.envParams.m_finalColorBalance.midtoneMarginMax.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.midtoneMarginMax.dataBaseType,
                    env.envParams.m_finalColorBalance.midtoneMarginMax.CurveType);

            case "parametricBalanceLow.saturation":
                s=env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.ScalarEditScale,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.dataBaseType,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.CurveType);

            case "parametricBalanceLow.color":
                s=env.envParams.m_finalColorBalance.parametricBalanceLow.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.parametricBalanceLow.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.color.ScalarEditScale,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.color.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.color.dataBaseType,
                    env.envParams.m_finalColorBalance.parametricBalanceLow.color.CurveType);

            case "parametricBalanceMid.saturation":
                s=env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.ScalarEditScale,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.dataBaseType,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.CurveType);

            case "parametricBalanceMid.color":
                s=env.envParams.m_finalColorBalance.parametricBalanceMid.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.parametricBalanceMid.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.color.ScalarEditScale,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.color.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.color.dataBaseType,
                    env.envParams.m_finalColorBalance.parametricBalanceMid.color.CurveType);

            case "parametricBalanceHigh.saturation":
                s=env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.ScalarEditScale,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.dataBaseType,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.CurveType);

            case "parametricBalanceHigh.color":
                s=env.envParams.m_finalColorBalance.parametricBalanceHigh.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_finalColorBalance.parametricBalanceHigh.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.color.ScalarEditScale,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.color.ScalarEditOrigin,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.color.dataBaseType,
                    env.envParams.m_finalColorBalance.parametricBalanceHigh.color.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_sharpen(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_sharpen.activated);
            case "sharpenNear":
                s=env.envParams.m_sharpen.sharpenNear.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sharpen.sharpenNear.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sharpen.sharpenNear.ScalarEditScale,
                    env.envParams.m_sharpen.sharpenNear.ScalarEditOrigin,
                    env.envParams.m_sharpen.sharpenNear.dataBaseType,
                    env.envParams.m_sharpen.sharpenNear.CurveType);
            case "sharpenFar":
                s=env.envParams.m_sharpen.sharpenFar.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sharpen.sharpenFar.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sharpen.sharpenFar.ScalarEditScale,
                    env.envParams.m_sharpen.sharpenFar.ScalarEditOrigin,
                    env.envParams.m_sharpen.sharpenFar.dataBaseType,
                    env.envParams.m_sharpen.sharpenFar.CurveType);
            case "distanceNear":
                s=env.envParams.m_sharpen.distanceNear.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sharpen.distanceNear.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sharpen.distanceNear.ScalarEditScale,
                    env.envParams.m_sharpen.distanceNear.ScalarEditOrigin,
                    env.envParams.m_sharpen.distanceNear.dataBaseType,
                    env.envParams.m_sharpen.distanceNear.CurveType);
            case "distanceFar":
                s=env.envParams.m_sharpen.distanceFar.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sharpen.distanceFar.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sharpen.distanceFar.ScalarEditScale,
                    env.envParams.m_sharpen.distanceFar.ScalarEditOrigin,
                    env.envParams.m_sharpen.distanceFar.dataBaseType,
                    env.envParams.m_sharpen.distanceFar.CurveType);
            case "lumFilterOffset":
                s=env.envParams.m_sharpen.lumFilterOffset.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sharpen.lumFilterOffset.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sharpen.lumFilterOffset.ScalarEditScale,
                    env.envParams.m_sharpen.lumFilterOffset.ScalarEditOrigin,
                    env.envParams.m_sharpen.lumFilterOffset.dataBaseType,
                    env.envParams.m_sharpen.lumFilterOffset.CurveType);
            case "lumFilterRange":
                s=env.envParams.m_sharpen.lumFilterRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sharpen.lumFilterRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sharpen.lumFilterRange.ScalarEditScale,
                    env.envParams.m_sharpen.lumFilterRange.ScalarEditOrigin,
                    env.envParams.m_sharpen.lumFilterRange.dataBaseType,
                    env.envParams.m_sharpen.lumFilterRange.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_paintEffect(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_paintEffect.activated);
            case "amount":
                s=env.envParams.m_paintEffect.amount.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_paintEffect.amount.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_paintEffect.amount.ScalarEditScale,
                    env.envParams.m_paintEffect.amount.ScalarEditOrigin,
                    env.envParams.m_paintEffect.amount.dataBaseType,
                    env.envParams.m_paintEffect.amount.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_ssaoNV(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_ssaoNV.activated);
            case "radius":
                s=env.envParams.m_ssaoNV.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.radius.ScalarEditScale,
                    env.envParams.m_ssaoNV.radius.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.radius.dataBaseType,
                    env.envParams.m_ssaoNV.radius.CurveType);
            case "bias":
                s=env.envParams.m_ssaoNV.bias.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.bias.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.bias.ScalarEditScale,
                    env.envParams.m_ssaoNV.bias.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.bias.dataBaseType,
                    env.envParams.m_ssaoNV.bias.CurveType);
            case "detailStrength":
                s=env.envParams.m_ssaoNV.detailStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.detailStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.detailStrength.ScalarEditScale,
                    env.envParams.m_ssaoNV.detailStrength.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.detailStrength.dataBaseType,
                    env.envParams.m_ssaoNV.detailStrength.CurveType);
            case "coarseStrength":
                s=env.envParams.m_ssaoNV.coarseStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.coarseStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.coarseStrength.ScalarEditScale,
                    env.envParams.m_ssaoNV.coarseStrength.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.coarseStrength.dataBaseType,
                    env.envParams.m_ssaoNV.coarseStrength.CurveType);
            case "powerExponent":
                s=env.envParams.m_ssaoNV.powerExponent.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.powerExponent.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.powerExponent.ScalarEditScale,
                    env.envParams.m_ssaoNV.powerExponent.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.powerExponent.dataBaseType,
                    env.envParams.m_ssaoNV.powerExponent.CurveType);
            case "blurSharpness":
                s=env.envParams.m_ssaoNV.blurSharpness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.blurSharpness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.blurSharpness.ScalarEditScale,
                    env.envParams.m_ssaoNV.blurSharpness.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.blurSharpness.dataBaseType,
                    env.envParams.m_ssaoNV.blurSharpness.CurveType);
            case "valueClamp":
                s=env.envParams.m_ssaoNV.valueClamp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.valueClamp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.valueClamp.ScalarEditScale,
                    env.envParams.m_ssaoNV.valueClamp.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.valueClamp.dataBaseType,
                    env.envParams.m_ssaoNV.valueClamp.CurveType);
            case "ssaoColor":
                s=env.envParams.m_ssaoNV.ssaoColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.ssaoColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.ssaoColor.ScalarEditScale,
                    env.envParams.m_ssaoNV.ssaoColor.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.ssaoColor.dataBaseType,
                    env.envParams.m_ssaoNV.ssaoColor.CurveType);
            case "nonAmbientInfluence":
                s=env.envParams.m_ssaoNV.nonAmbientInfluence.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.nonAmbientInfluence.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.nonAmbientInfluence.ScalarEditScale,
                    env.envParams.m_ssaoNV.nonAmbientInfluence.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.nonAmbientInfluence.dataBaseType,
                    env.envParams.m_ssaoNV.nonAmbientInfluence.CurveType);
            case "translucencyInfluence":
                s=env.envParams.m_ssaoNV.translucencyInfluence.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoNV.translucencyInfluence.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoNV.translucencyInfluence.ScalarEditScale,
                    env.envParams.m_ssaoNV.translucencyInfluence.ScalarEditOrigin,
                    env.envParams.m_ssaoNV.translucencyInfluence.dataBaseType,
                    env.envParams.m_ssaoNV.translucencyInfluence.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_ssaoMS(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_ssaoMS.activated);
            case "noiseFilterTolerance":
                s=env.envParams.m_ssaoMS.noiseFilterTolerance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.noiseFilterTolerance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.noiseFilterTolerance.ScalarEditScale,
                    env.envParams.m_ssaoMS.noiseFilterTolerance.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.noiseFilterTolerance.dataBaseType,
                    env.envParams.m_ssaoMS.noiseFilterTolerance.CurveType);
            case "blurTolerance":
                s=env.envParams.m_ssaoMS.blurTolerance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.blurTolerance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.blurTolerance.ScalarEditScale,
                    env.envParams.m_ssaoMS.blurTolerance.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.blurTolerance.dataBaseType,
                    env.envParams.m_ssaoMS.blurTolerance.CurveType);
            case "upsampleTolerance":
                s=env.envParams.m_ssaoMS.upsampleTolerance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.upsampleTolerance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.upsampleTolerance.ScalarEditScale,
                    env.envParams.m_ssaoMS.upsampleTolerance.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.upsampleTolerance.dataBaseType,
                    env.envParams.m_ssaoMS.upsampleTolerance.CurveType);
            case "rejectionFalloff":
                s=env.envParams.m_ssaoMS.rejectionFalloff.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.rejectionFalloff.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.rejectionFalloff.ScalarEditScale,
                    env.envParams.m_ssaoMS.rejectionFalloff.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.rejectionFalloff.dataBaseType,
                    env.envParams.m_ssaoMS.rejectionFalloff.CurveType);
            case "combineResolutionsBeforeBlur":return toBool(env.envParams.m_ssaoMS.combineResolutionsBeforeBlur);
            case "combineResolutionsWithMul":   return toBool(env.envParams.m_ssaoMS.combineResolutionsWithMul);
            case "hierarchyDepth":
                s=env.envParams.m_ssaoMS.hierarchyDepth.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.hierarchyDepth.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.hierarchyDepth.ScalarEditScale,
                    env.envParams.m_ssaoMS.hierarchyDepth.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.hierarchyDepth.dataBaseType,
                    env.envParams.m_ssaoMS.hierarchyDepth.CurveType);
            case "normalAOMultiply":
                s=env.envParams.m_ssaoMS.normalAOMultiply.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.normalAOMultiply.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.normalAOMultiply.ScalarEditScale,
                    env.envParams.m_ssaoMS.normalAOMultiply.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.normalAOMultiply.dataBaseType,
                    env.envParams.m_ssaoMS.normalAOMultiply.CurveType);
            case "normalToDepthBrightnessEqualiser":
                s=env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.ScalarEditScale,
                    env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.dataBaseType,
                    env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.CurveType);
            case "normalBackProjectionTolerance":
                s=env.envParams.m_ssaoMS.normalBackProjectionTolerance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_ssaoMS.normalBackProjectionTolerance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_ssaoMS.normalBackProjectionTolerance.ScalarEditScale,
                    env.envParams.m_ssaoMS.normalBackProjectionTolerance.ScalarEditOrigin,
                    env.envParams.m_ssaoMS.normalBackProjectionTolerance.dataBaseType,
                    env.envParams.m_ssaoMS.normalBackProjectionTolerance.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_globalLight(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_globalLight.activated);
            case "activatedGlobalLightActivated":   return toBool(env.envParams.m_globalLight.activatedGlobalLightActivated);
            case "globalLightActivated":   return toFloat(env.envParams.m_globalLight.globalLightActivated);
            case "activatedActivatedFactorLightDir":   return toBool(env.envParams.m_globalLight.activatedActivatedFactorLightDir);
            case "activatedFactorLightDir":   return toFloat(env.envParams.m_globalLight.activatedFactorLightDir);
            case "sunColor":
                s=env.envParams.m_globalLight.sunColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColor.ScalarEditScale,
                    env.envParams.m_globalLight.sunColor.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColor.dataBaseType,
                    env.envParams.m_globalLight.sunColor.CurveType);
            case "sunColorLightSide":
                s=env.envParams.m_globalLight.sunColorLightSide.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColorLightSide.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColorLightSide.ScalarEditScale,
                    env.envParams.m_globalLight.sunColorLightSide.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColorLightSide.dataBaseType,
                    env.envParams.m_globalLight.sunColorLightSide.CurveType);
            case "sunColorLightOppositeSide":
                s=env.envParams.m_globalLight.sunColorLightOppositeSide.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColorLightOppositeSide.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColorLightOppositeSide.ScalarEditScale,
                    env.envParams.m_globalLight.sunColorLightOppositeSide.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColorLightOppositeSide.dataBaseType,
                    env.envParams.m_globalLight.sunColorLightOppositeSide.CurveType);
            case "sunColorCenterArea":
                s=env.envParams.m_globalLight.sunColorCenterArea.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColorCenterArea.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColorCenterArea.ScalarEditScale,
                    env.envParams.m_globalLight.sunColorCenterArea.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColorCenterArea.dataBaseType,
                    env.envParams.m_globalLight.sunColorCenterArea.CurveType);
            case "sunColorSidesMargin":
                s=env.envParams.m_globalLight.sunColorSidesMargin.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColorSidesMargin.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColorSidesMargin.ScalarEditScale,
                    env.envParams.m_globalLight.sunColorSidesMargin.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColorSidesMargin.dataBaseType,
                    env.envParams.m_globalLight.sunColorSidesMargin.CurveType);
            case "sunColorBottomHeight":
                s=env.envParams.m_globalLight.sunColorBottomHeight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColorBottomHeight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColorBottomHeight.ScalarEditScale,
                    env.envParams.m_globalLight.sunColorBottomHeight.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColorBottomHeight.dataBaseType,
                    env.envParams.m_globalLight.sunColorBottomHeight.CurveType);
            case "sunColorTopHeight":
                s=env.envParams.m_globalLight.sunColorTopHeight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.sunColorTopHeight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.sunColorTopHeight.ScalarEditScale,
                    env.envParams.m_globalLight.sunColorTopHeight.ScalarEditOrigin,
                    env.envParams.m_globalLight.sunColorTopHeight.dataBaseType,
                    env.envParams.m_globalLight.sunColorTopHeight.CurveType);
            case "forcedLightDirAnglesYaw":
                s=env.envParams.m_globalLight.forcedLightDirAnglesYaw.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedLightDirAnglesYaw.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedLightDirAnglesYaw.ScalarEditScale,
                    env.envParams.m_globalLight.forcedLightDirAnglesYaw.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedLightDirAnglesYaw.dataBaseType,
                    env.envParams.m_globalLight.forcedLightDirAnglesYaw.CurveType);
            case "forcedLightDirAnglesPitch":
                s=env.envParams.m_globalLight.forcedLightDirAnglesPitch.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedLightDirAnglesPitch.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedLightDirAnglesPitch.ScalarEditScale,
                    env.envParams.m_globalLight.forcedLightDirAnglesPitch.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedLightDirAnglesPitch.dataBaseType,
                    env.envParams.m_globalLight.forcedLightDirAnglesPitch.CurveType);
            case "forcedLightDirAnglesRoll":
                s=env.envParams.m_globalLight.forcedLightDirAnglesRoll.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedLightDirAnglesRoll.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedLightDirAnglesRoll.ScalarEditScale,
                    env.envParams.m_globalLight.forcedLightDirAnglesRoll.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedLightDirAnglesRoll.dataBaseType,
                    env.envParams.m_globalLight.forcedLightDirAnglesRoll.CurveType);
            case "forcedSunDirAnglesYaw":
                s=env.envParams.m_globalLight.forcedSunDirAnglesYaw.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedSunDirAnglesYaw.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedSunDirAnglesYaw.ScalarEditScale,
                    env.envParams.m_globalLight.forcedSunDirAnglesYaw.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedSunDirAnglesYaw.dataBaseType,
                    env.envParams.m_globalLight.forcedSunDirAnglesYaw.CurveType);
            case "forcedSunDirAnglesPitch":
                s=env.envParams.m_globalLight.forcedSunDirAnglesPitch.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedSunDirAnglesPitch.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedSunDirAnglesPitch.ScalarEditScale,
                    env.envParams.m_globalLight.forcedSunDirAnglesPitch.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedSunDirAnglesPitch.dataBaseType,
                    env.envParams.m_globalLight.forcedSunDirAnglesPitch.CurveType);
            case "forcedSunDirAnglesRoll":
                s=env.envParams.m_globalLight.forcedSunDirAnglesRoll.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedSunDirAnglesRoll.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedSunDirAnglesRoll.ScalarEditScale,
                    env.envParams.m_globalLight.forcedSunDirAnglesRoll.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedSunDirAnglesRoll.dataBaseType,
                    env.envParams.m_globalLight.forcedSunDirAnglesRoll.CurveType);
            case "forcedMoonDirAnglesYaw":
                s=env.envParams.m_globalLight.forcedMoonDirAnglesYaw.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedMoonDirAnglesYaw.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedMoonDirAnglesYaw.ScalarEditScale,
                    env.envParams.m_globalLight.forcedMoonDirAnglesYaw.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedMoonDirAnglesYaw.dataBaseType,
                    env.envParams.m_globalLight.forcedMoonDirAnglesYaw.CurveType);
            case "forcedMoonDirAnglesPitch":
                s=env.envParams.m_globalLight.forcedMoonDirAnglesPitch.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedMoonDirAnglesPitch.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedMoonDirAnglesPitch.ScalarEditScale,
                    env.envParams.m_globalLight.forcedMoonDirAnglesPitch.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedMoonDirAnglesPitch.dataBaseType,
                    env.envParams.m_globalLight.forcedMoonDirAnglesPitch.CurveType);
            case "forcedMoonDirAnglesRoll":
                s=env.envParams.m_globalLight.forcedMoonDirAnglesRoll.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.forcedMoonDirAnglesRoll.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.forcedMoonDirAnglesRoll.ScalarEditScale,
                    env.envParams.m_globalLight.forcedMoonDirAnglesRoll.ScalarEditOrigin,
                    env.envParams.m_globalLight.forcedMoonDirAnglesRoll.dataBaseType,
                    env.envParams.m_globalLight.forcedMoonDirAnglesRoll.CurveType);
            case "translucencyViewDependency":
                s=env.envParams.m_globalLight.translucencyViewDependency.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.translucencyViewDependency.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.translucencyViewDependency.ScalarEditScale,
                    env.envParams.m_globalLight.translucencyViewDependency.ScalarEditOrigin,
                    env.envParams.m_globalLight.translucencyViewDependency.dataBaseType,
                    env.envParams.m_globalLight.translucencyViewDependency.CurveType);
            case "translucencyBaseFlatness":
                s=env.envParams.m_globalLight.translucencyBaseFlatness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.translucencyBaseFlatness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.translucencyBaseFlatness.ScalarEditScale,
                    env.envParams.m_globalLight.translucencyBaseFlatness.ScalarEditOrigin,
                    env.envParams.m_globalLight.translucencyBaseFlatness.dataBaseType,
                    env.envParams.m_globalLight.translucencyBaseFlatness.CurveType);
            case "translucencyFlatBrightness":
                s=env.envParams.m_globalLight.translucencyFlatBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.translucencyFlatBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.translucencyFlatBrightness.ScalarEditScale,
                    env.envParams.m_globalLight.translucencyFlatBrightness.ScalarEditOrigin,
                    env.envParams.m_globalLight.translucencyFlatBrightness.dataBaseType,
                    env.envParams.m_globalLight.translucencyFlatBrightness.CurveType);
            case "translucencyGainBrightness":
                s=env.envParams.m_globalLight.translucencyGainBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.translucencyGainBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.translucencyGainBrightness.ScalarEditScale,
                    env.envParams.m_globalLight.translucencyGainBrightness.ScalarEditOrigin,
                    env.envParams.m_globalLight.translucencyGainBrightness.dataBaseType,
                    env.envParams.m_globalLight.translucencyGainBrightness.CurveType);
            case "translucencyFresnelScaleLight":
                s=env.envParams.m_globalLight.translucencyFresnelScaleLight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.translucencyFresnelScaleLight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.translucencyFresnelScaleLight.ScalarEditScale,
                    env.envParams.m_globalLight.translucencyFresnelScaleLight.ScalarEditOrigin,
                    env.envParams.m_globalLight.translucencyFresnelScaleLight.dataBaseType,
                    env.envParams.m_globalLight.translucencyFresnelScaleLight.CurveType);
            case "translucencyFresnelScaleReflection":
                s=env.envParams.m_globalLight.translucencyFresnelScaleReflection.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.translucencyFresnelScaleReflection.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.translucencyFresnelScaleReflection.ScalarEditScale,
                    env.envParams.m_globalLight.translucencyFresnelScaleReflection.ScalarEditOrigin,
                    env.envParams.m_globalLight.translucencyFresnelScaleReflection.dataBaseType,
                    env.envParams.m_globalLight.translucencyFresnelScaleReflection.CurveType);
            case "envProbeBaseLightingAmbient.activated":   return toBool(env.envParams.m_globalLight.envProbeBaseLightingAmbient.activated);
            case "envProbeBaseLightingAmbient.colorAmbient":
                s=env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.CurveType);
            case "envProbeBaseLightingAmbient.colorSceneAdd":
                s=env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.CurveType);
            case "envProbeBaseLightingAmbient.colorSkyTop":
                s=env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.CurveType);
            case "envProbeBaseLightingAmbient.colorSkyHorizon":
                s=env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.CurveType);
            case "envProbeBaseLightingAmbient.skyShape":
                s=env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.CurveType);
            case "envProbeBaseLightingReflection.activated":   return toBool(env.envParams.m_globalLight.envProbeBaseLightingReflection.activated);
            case "envProbeBaseLightingReflection.colorAmbient":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.CurveType);
            case "envProbeBaseLightingReflection.colorSceneMul":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.CurveType);
            case "envProbeBaseLightingReflection.colorSceneAdd":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.CurveType);
            case "envProbeBaseLightingReflection.colorSkyMul":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.CurveType);
            case "envProbeBaseLightingReflection.colorSkyAdd":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.CurveType);
            case "envProbeBaseLightingReflection.remapOffset":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.CurveType);
            case "envProbeBaseLightingReflection.remapStrength":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.CurveType);
            case "envProbeBaseLightingReflection.remapClamp":
                s=env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.dataBaseType,
                    env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.CurveType);
            case "charactersLightingBoostAmbientLight":
                s=env.envParams.m_globalLight.charactersLightingBoostAmbientLight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.charactersLightingBoostAmbientLight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientLight.ScalarEditScale,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientLight.ScalarEditOrigin,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientLight.dataBaseType,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientLight.CurveType);
            case "charactersLightingBoostAmbientShadow":
                s=env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.ScalarEditScale,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.ScalarEditOrigin,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.dataBaseType,
                    env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.CurveType);
            case "charactersLightingBoostReflectionLight":
                s=env.envParams.m_globalLight.charactersLightingBoostReflectionLight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.charactersLightingBoostReflectionLight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionLight.ScalarEditScale,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionLight.ScalarEditOrigin,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionLight.dataBaseType,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionLight.CurveType);
            case "charactersLightingBoostReflectionShadow":
                s=env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.ScalarEditScale,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.ScalarEditOrigin,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.dataBaseType,
                    env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.CurveType);
            case "charactersEyeBlicksColor":
                s=env.envParams.m_globalLight.charactersEyeBlicksColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.charactersEyeBlicksColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.charactersEyeBlicksColor.ScalarEditScale,
                    env.envParams.m_globalLight.charactersEyeBlicksColor.ScalarEditOrigin,
                    env.envParams.m_globalLight.charactersEyeBlicksColor.dataBaseType,
                    env.envParams.m_globalLight.charactersEyeBlicksColor.CurveType);
            case "charactersEyeBlicksShadowedScale":
                s=env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.ScalarEditScale,
                    env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.ScalarEditOrigin,
                    env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.dataBaseType,
                    env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.CurveType);
            case "envProbeAmbientScaleLight":
                s=env.envParams.m_globalLight.envProbeAmbientScaleLight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeAmbientScaleLight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeAmbientScaleLight.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeAmbientScaleLight.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeAmbientScaleLight.dataBaseType,
                    env.envParams.m_globalLight.envProbeAmbientScaleLight.CurveType);
            case "envProbeAmbientScaleShadow":
                s=env.envParams.m_globalLight.envProbeAmbientScaleShadow.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeAmbientScaleShadow.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeAmbientScaleShadow.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeAmbientScaleShadow.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeAmbientScaleShadow.dataBaseType,
                    env.envParams.m_globalLight.envProbeAmbientScaleShadow.CurveType);
            case "envProbeReflectionScaleLight":
                s=env.envParams.m_globalLight.envProbeReflectionScaleLight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeReflectionScaleLight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeReflectionScaleLight.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeReflectionScaleLight.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeReflectionScaleLight.dataBaseType,
                    env.envParams.m_globalLight.envProbeReflectionScaleLight.CurveType);
            case "envProbeReflectionScaleShadow":
                s=env.envParams.m_globalLight.envProbeReflectionScaleShadow.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeReflectionScaleShadow.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeReflectionScaleShadow.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeReflectionScaleShadow.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeReflectionScaleShadow.dataBaseType,
                    env.envParams.m_globalLight.envProbeReflectionScaleShadow.CurveType);
            case "envProbeDistantScaleFactor":
                s=env.envParams.m_globalLight.envProbeDistantScaleFactor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalLight.envProbeDistantScaleFactor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalLight.envProbeDistantScaleFactor.ScalarEditScale,
                    env.envParams.m_globalLight.envProbeDistantScaleFactor.ScalarEditOrigin,
                    env.envParams.m_globalLight.envProbeDistantScaleFactor.dataBaseType,
                    env.envParams.m_globalLight.envProbeDistantScaleFactor.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_interiorFallback(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_interiorFallback.activated);
            case "colorAmbientMul":
                s=env.envParams.m_interiorFallback.colorAmbientMul.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_interiorFallback.colorAmbientMul.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_interiorFallback.colorAmbientMul.ScalarEditScale,
                    env.envParams.m_interiorFallback.colorAmbientMul.ScalarEditOrigin,
                    env.envParams.m_interiorFallback.colorAmbientMul.dataBaseType,
                    env.envParams.m_interiorFallback.colorAmbientMul.CurveType);
            case "colorReflectionLow":
                s=env.envParams.m_interiorFallback.colorReflectionLow.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_interiorFallback.colorReflectionLow.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_interiorFallback.colorReflectionLow.ScalarEditScale,
                    env.envParams.m_interiorFallback.colorReflectionLow.ScalarEditOrigin,
                    env.envParams.m_interiorFallback.colorReflectionLow.dataBaseType,
                    env.envParams.m_interiorFallback.colorReflectionLow.CurveType);
            case "colorReflectionMiddle":
                s=env.envParams.m_interiorFallback.colorReflectionMiddle.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_interiorFallback.colorReflectionMiddle.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_interiorFallback.colorReflectionMiddle.ScalarEditScale,
                    env.envParams.m_interiorFallback.colorReflectionMiddle.ScalarEditOrigin,
                    env.envParams.m_interiorFallback.colorReflectionMiddle.dataBaseType,
                    env.envParams.m_interiorFallback.colorReflectionMiddle.CurveType);
            case "colorReflectionHigh":
                s=env.envParams.m_interiorFallback.colorReflectionHigh.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_interiorFallback.colorReflectionHigh.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_interiorFallback.colorReflectionHigh.ScalarEditScale,
                    env.envParams.m_interiorFallback.colorReflectionHigh.ScalarEditOrigin,
                    env.envParams.m_interiorFallback.colorReflectionHigh.dataBaseType,
                    env.envParams.m_interiorFallback.colorReflectionHigh.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_speedTree(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_speedTree.activated);
            case "diffuse":
                s=env.envParams.m_speedTree.diffuse.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.diffuse.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.diffuse.ScalarEditScale,
                    env.envParams.m_speedTree.diffuse.ScalarEditOrigin,
                    env.envParams.m_speedTree.diffuse.dataBaseType,
                    env.envParams.m_speedTree.diffuse.CurveType);
            case "specularScale":
                s=env.envParams.m_speedTree.specularScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.specularScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.specularScale.ScalarEditScale,
                    env.envParams.m_speedTree.specularScale.ScalarEditOrigin,
                    env.envParams.m_speedTree.specularScale.dataBaseType,
                    env.envParams.m_speedTree.specularScale.CurveType);
            case "translucencyScale":
                s=env.envParams.m_speedTree.translucencyScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.translucencyScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.translucencyScale.ScalarEditScale,
                    env.envParams.m_speedTree.translucencyScale.ScalarEditOrigin,
                    env.envParams.m_speedTree.translucencyScale.dataBaseType,
                    env.envParams.m_speedTree.translucencyScale.CurveType);
            case "ambientOcclusionScale":
                s=env.envParams.m_speedTree.ambientOcclusionScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.ambientOcclusionScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.ambientOcclusionScale.ScalarEditScale,
                    env.envParams.m_speedTree.ambientOcclusionScale.ScalarEditOrigin,
                    env.envParams.m_speedTree.ambientOcclusionScale.dataBaseType,
                    env.envParams.m_speedTree.ambientOcclusionScale.CurveType);
            case "billboardsColor":
                s=env.envParams.m_speedTree.billboardsColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.billboardsColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.billboardsColor.ScalarEditScale,
                    env.envParams.m_speedTree.billboardsColor.ScalarEditOrigin,
                    env.envParams.m_speedTree.billboardsColor.dataBaseType,
                    env.envParams.m_speedTree.billboardsColor.CurveType);
            case "billboardsTranslucency":
                s=env.envParams.m_speedTree.billboardsTranslucency.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.billboardsTranslucency.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.billboardsTranslucency.ScalarEditScale,
                    env.envParams.m_speedTree.billboardsTranslucency.ScalarEditOrigin,
                    env.envParams.m_speedTree.billboardsTranslucency.dataBaseType,
                    env.envParams.m_speedTree.billboardsTranslucency.CurveType);
            case "randomColorsTrees.luminanceWeights":
                s=env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.CurveType);
            case "randomColorsTrees.randomColor0":
                s=env.envParams.m_speedTree.randomColorsTrees.randomColor0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.randomColor0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor0.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor0.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor0.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor0.CurveType);
            case "randomColorsTrees.saturation0":
                s=env.envParams.m_speedTree.randomColorsTrees.saturation0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.saturation0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.saturation0.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.saturation0.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.saturation0.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.saturation0.CurveType);
            case "randomColorsTrees.randomColor1":
                s=env.envParams.m_speedTree.randomColorsTrees.randomColor1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.randomColor1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor1.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor1.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor1.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor1.CurveType);
            case "randomColorsTrees.saturation1":
                s=env.envParams.m_speedTree.randomColorsTrees.saturation1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.saturation1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.saturation1.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.saturation1.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.saturation1.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.saturation1.CurveType);
            case "randomColorsTrees.randomColor2":
                s=env.envParams.m_speedTree.randomColorsTrees.randomColor2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.randomColor2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor2.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor2.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor2.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.randomColor2.CurveType);
            case "randomColorsTrees.saturation2":
                s=env.envParams.m_speedTree.randomColorsTrees.saturation2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsTrees.saturation2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsTrees.saturation2.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsTrees.saturation2.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsTrees.saturation2.dataBaseType,
                    env.envParams.m_speedTree.randomColorsTrees.saturation2.CurveType);
            case "randomColorsBranches.luminanceWeights":
                s=env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.CurveType);
            case "randomColorsBranches.randomColor0":
                s=env.envParams.m_speedTree.randomColorsBranches.randomColor0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.randomColor0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor0.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor0.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor0.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor0.CurveType);
            case "randomColorsBranches.saturation0":
                s=env.envParams.m_speedTree.randomColorsBranches.saturation0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.saturation0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.saturation0.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.saturation0.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.saturation0.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.saturation0.CurveType);
            case "randomColorsBranches.randomColor1":
                s=env.envParams.m_speedTree.randomColorsBranches.randomColor1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.randomColor1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor1.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor1.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor1.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor1.CurveType);
            case "randomColorsBranches.saturation1":
                s=env.envParams.m_speedTree.randomColorsBranches.saturation1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.saturation1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.saturation1.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.saturation1.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.saturation1.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.saturation1.CurveType);
            case "randomColorsBranches.randomColor2":
                s=env.envParams.m_speedTree.randomColorsBranches.randomColor2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.randomColor2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor2.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor2.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor2.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.randomColor2.CurveType);
            case "randomColorsBranches.saturation2":
                s=env.envParams.m_speedTree.randomColorsBranches.saturation2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsBranches.saturation2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsBranches.saturation2.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsBranches.saturation2.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsBranches.saturation2.dataBaseType,
                    env.envParams.m_speedTree.randomColorsBranches.saturation2.CurveType);
            case "randomColorsGrass.luminanceWeights":
                s=env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.CurveType);
            case "randomColorsGrass.randomColor0":
                s=env.envParams.m_speedTree.randomColorsGrass.randomColor0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.randomColor0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor0.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor0.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor0.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor0.CurveType);
            case "randomColorsGrass.saturation0":
                s=env.envParams.m_speedTree.randomColorsGrass.saturation0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.saturation0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.saturation0.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.saturation0.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.saturation0.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.saturation0.CurveType);
            case "randomColorsGrass.randomColor1":
                s=env.envParams.m_speedTree.randomColorsGrass.randomColor1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.randomColor1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor1.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor1.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor1.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor1.CurveType);
            case "randomColorsGrass.saturation1":
                s=env.envParams.m_speedTree.randomColorsGrass.saturation1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.saturation1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.saturation1.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.saturation1.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.saturation1.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.saturation1.CurveType);
            case "randomColorsGrass.randomColor2":
                s=env.envParams.m_speedTree.randomColorsGrass.randomColor2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.randomColor2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor2.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor2.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor2.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.randomColor2.CurveType);
            case "randomColorsGrass.saturation2":
                s=env.envParams.m_speedTree.randomColorsGrass.saturation2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsGrass.saturation2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsGrass.saturation2.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsGrass.saturation2.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsGrass.saturation2.dataBaseType,
                    env.envParams.m_speedTree.randomColorsGrass.saturation2.CurveType);
            case "randomColorsFallback":
                s=env.envParams.m_speedTree.randomColorsFallback.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.randomColorsFallback.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.randomColorsFallback.ScalarEditScale,
                    env.envParams.m_speedTree.randomColorsFallback.ScalarEditOrigin,
                    env.envParams.m_speedTree.randomColorsFallback.dataBaseType,
                    env.envParams.m_speedTree.randomColorsFallback.CurveType);
            case "pigmentBrightness":
                s=env.envParams.m_speedTree.pigmentBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.pigmentBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.pigmentBrightness.ScalarEditScale,
                    env.envParams.m_speedTree.pigmentBrightness.ScalarEditOrigin,
                    env.envParams.m_speedTree.pigmentBrightness.dataBaseType,
                    env.envParams.m_speedTree.pigmentBrightness.CurveType);
            case "pigmentFloodStartDist":
                s=env.envParams.m_speedTree.pigmentFloodStartDist.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.pigmentFloodStartDist.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.pigmentFloodStartDist.ScalarEditScale,
                    env.envParams.m_speedTree.pigmentFloodStartDist.ScalarEditOrigin,
                    env.envParams.m_speedTree.pigmentFloodStartDist.dataBaseType,
                    env.envParams.m_speedTree.pigmentFloodStartDist.CurveType);
            case "pigmentFloodRange":
                s=env.envParams.m_speedTree.pigmentFloodRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.pigmentFloodRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.pigmentFloodRange.ScalarEditScale,
                    env.envParams.m_speedTree.pigmentFloodRange.ScalarEditOrigin,
                    env.envParams.m_speedTree.pigmentFloodRange.dataBaseType,
                    env.envParams.m_speedTree.pigmentFloodRange.CurveType);
            case "billboardsLightBleed":
                s=env.envParams.m_speedTree.billboardsLightBleed.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_speedTree.billboardsLightBleed.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_speedTree.billboardsLightBleed.ScalarEditScale,
                    env.envParams.m_speedTree.billboardsLightBleed.ScalarEditOrigin,
                    env.envParams.m_speedTree.billboardsLightBleed.dataBaseType,
                    env.envParams.m_speedTree.billboardsLightBleed.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_toneMapping(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_toneMapping.activated);
            case "skyLuminanceCustomValue":
                s=env.envParams.m_toneMapping.skyLuminanceCustomValue.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.skyLuminanceCustomValue.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.skyLuminanceCustomValue.ScalarEditScale,
                    env.envParams.m_toneMapping.skyLuminanceCustomValue.ScalarEditOrigin,
                    env.envParams.m_toneMapping.skyLuminanceCustomValue.dataBaseType,
                    env.envParams.m_toneMapping.skyLuminanceCustomValue.CurveType);
            case "skyLuminanceCustomAmount":
                s=env.envParams.m_toneMapping.skyLuminanceCustomAmount.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.skyLuminanceCustomAmount.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.skyLuminanceCustomAmount.ScalarEditScale,
                    env.envParams.m_toneMapping.skyLuminanceCustomAmount.ScalarEditOrigin,
                    env.envParams.m_toneMapping.skyLuminanceCustomAmount.dataBaseType,
                    env.envParams.m_toneMapping.skyLuminanceCustomAmount.CurveType);
            case "luminanceLimitShape":
                s=env.envParams.m_toneMapping.luminanceLimitShape.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.luminanceLimitShape.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.luminanceLimitShape.ScalarEditScale,
                    env.envParams.m_toneMapping.luminanceLimitShape.ScalarEditOrigin,
                    env.envParams.m_toneMapping.luminanceLimitShape.dataBaseType,
                    env.envParams.m_toneMapping.luminanceLimitShape.CurveType);
            case "luminanceLimitMin":
                s=env.envParams.m_toneMapping.luminanceLimitMin.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.luminanceLimitMin.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.luminanceLimitMin.ScalarEditScale,
                    env.envParams.m_toneMapping.luminanceLimitMin.ScalarEditOrigin,
                    env.envParams.m_toneMapping.luminanceLimitMin.dataBaseType,
                    env.envParams.m_toneMapping.luminanceLimitMin.CurveType);
            case "luminanceLimitMax":
                s=env.envParams.m_toneMapping.luminanceLimitMax.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.luminanceLimitMax.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.luminanceLimitMax.ScalarEditScale,
                    env.envParams.m_toneMapping.luminanceLimitMax.ScalarEditOrigin,
                    env.envParams.m_toneMapping.luminanceLimitMax.dataBaseType,
                    env.envParams.m_toneMapping.luminanceLimitMax.CurveType);
            case "rejectThreshold":
                s=env.envParams.m_toneMapping.rejectThreshold.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.rejectThreshold.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.rejectThreshold.ScalarEditScale,
                    env.envParams.m_toneMapping.rejectThreshold.ScalarEditOrigin,
                    env.envParams.m_toneMapping.rejectThreshold.dataBaseType,
                    env.envParams.m_toneMapping.rejectThreshold.CurveType);
            case "rejectSmoothExtent":
                s=env.envParams.m_toneMapping.rejectSmoothExtent.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.rejectSmoothExtent.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.rejectSmoothExtent.ScalarEditScale,
                    env.envParams.m_toneMapping.rejectSmoothExtent.ScalarEditOrigin,
                    env.envParams.m_toneMapping.rejectSmoothExtent.dataBaseType,
                    env.envParams.m_toneMapping.rejectSmoothExtent.CurveType);
            case "newToneMapCurveParameters.shoulderStrength":
                s=env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.CurveType);
            case "newToneMapCurveParameters.linearStrength":
                s=env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.CurveType);
            case "newToneMapCurveParameters.linearAngle":
                s=env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.CurveType);
            case "newToneMapCurveParameters.toeStrength":
                s=env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.CurveType);
            case "newToneMapCurveParameters.toeNumerator":
                s=env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.CurveType);
            case "newToneMapCurveParameters.toeDenominator":
                s=env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.CurveType);
            case "newToneMapWhitepoint":
                s=env.envParams.m_toneMapping.newToneMapWhitepoint.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapWhitepoint.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapWhitepoint.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapWhitepoint.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapWhitepoint.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapWhitepoint.CurveType);
            case "newToneMapPostScale":
                s=env.envParams.m_toneMapping.newToneMapPostScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.newToneMapPostScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.newToneMapPostScale.ScalarEditScale,
                    env.envParams.m_toneMapping.newToneMapPostScale.ScalarEditOrigin,
                    env.envParams.m_toneMapping.newToneMapPostScale.dataBaseType,
                    env.envParams.m_toneMapping.newToneMapPostScale.CurveType);
            case "exposureScale":
                s=env.envParams.m_toneMapping.exposureScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.exposureScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.exposureScale.ScalarEditScale,
                    env.envParams.m_toneMapping.exposureScale.ScalarEditOrigin,
                    env.envParams.m_toneMapping.exposureScale.dataBaseType,
                    env.envParams.m_toneMapping.exposureScale.CurveType);
            case "postScale":
                s=env.envParams.m_toneMapping.postScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_toneMapping.postScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_toneMapping.postScale.ScalarEditScale,
                    env.envParams.m_toneMapping.postScale.ScalarEditOrigin,
                    env.envParams.m_toneMapping.postScale.dataBaseType,
                    env.envParams.m_toneMapping.postScale.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_bloomNew(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_bloomNew.activated);
            case "brightPassWeights":
                s=env.envParams.m_bloomNew.brightPassWeights.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.brightPassWeights.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.brightPassWeights.ScalarEditScale,
                    env.envParams.m_bloomNew.brightPassWeights.ScalarEditOrigin,
                    env.envParams.m_bloomNew.brightPassWeights.dataBaseType,
                    env.envParams.m_bloomNew.brightPassWeights.CurveType);
            case "color":
                s=env.envParams.m_bloomNew.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.color.ScalarEditScale,
                    env.envParams.m_bloomNew.color.ScalarEditOrigin,
                    env.envParams.m_bloomNew.color.dataBaseType,
                    env.envParams.m_bloomNew.color.CurveType);
            case "dirtColor":
                s=env.envParams.m_bloomNew.dirtColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.dirtColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.dirtColor.ScalarEditScale,
                    env.envParams.m_bloomNew.dirtColor.ScalarEditOrigin,
                    env.envParams.m_bloomNew.dirtColor.dataBaseType,
                    env.envParams.m_bloomNew.dirtColor.CurveType);
            case "threshold":
                s=env.envParams.m_bloomNew.threshold.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.threshold.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.threshold.ScalarEditScale,
                    env.envParams.m_bloomNew.threshold.ScalarEditOrigin,
                    env.envParams.m_bloomNew.threshold.dataBaseType,
                    env.envParams.m_bloomNew.threshold.CurveType);
            case "thresholdRange":
                s=env.envParams.m_bloomNew.thresholdRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.thresholdRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.thresholdRange.ScalarEditScale,
                    env.envParams.m_bloomNew.thresholdRange.ScalarEditOrigin,
                    env.envParams.m_bloomNew.thresholdRange.dataBaseType,
                    env.envParams.m_bloomNew.thresholdRange.CurveType);
            case "brightnessMax":
                s=env.envParams.m_bloomNew.brightnessMax.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.brightnessMax.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.brightnessMax.ScalarEditScale,
                    env.envParams.m_bloomNew.brightnessMax.ScalarEditOrigin,
                    env.envParams.m_bloomNew.brightnessMax.dataBaseType,
                    env.envParams.m_bloomNew.brightnessMax.CurveType);
            case "shaftsColor":
                s=env.envParams.m_bloomNew.shaftsColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.shaftsColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.shaftsColor.ScalarEditScale,
                    env.envParams.m_bloomNew.shaftsColor.ScalarEditOrigin,
                    env.envParams.m_bloomNew.shaftsColor.dataBaseType,
                    env.envParams.m_bloomNew.shaftsColor.CurveType);
            case "shaftsRadius":
                s=env.envParams.m_bloomNew.shaftsRadius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.shaftsRadius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.shaftsRadius.ScalarEditScale,
                    env.envParams.m_bloomNew.shaftsRadius.ScalarEditOrigin,
                    env.envParams.m_bloomNew.shaftsRadius.dataBaseType,
                    env.envParams.m_bloomNew.shaftsRadius.CurveType);
            case "shaftsShapeExp":
                s=env.envParams.m_bloomNew.shaftsShapeExp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.shaftsShapeExp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.shaftsShapeExp.ScalarEditScale,
                    env.envParams.m_bloomNew.shaftsShapeExp.ScalarEditOrigin,
                    env.envParams.m_bloomNew.shaftsShapeExp.dataBaseType,
                    env.envParams.m_bloomNew.shaftsShapeExp.CurveType);
            case "shaftsShapeInvSquare":
                s=env.envParams.m_bloomNew.shaftsShapeInvSquare.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.shaftsShapeInvSquare.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.shaftsShapeInvSquare.ScalarEditScale,
                    env.envParams.m_bloomNew.shaftsShapeInvSquare.ScalarEditOrigin,
                    env.envParams.m_bloomNew.shaftsShapeInvSquare.dataBaseType,
                    env.envParams.m_bloomNew.shaftsShapeInvSquare.CurveType);
            case "shaftsThreshold":
                s=env.envParams.m_bloomNew.shaftsThreshold.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.shaftsThreshold.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.shaftsThreshold.ScalarEditScale,
                    env.envParams.m_bloomNew.shaftsThreshold.ScalarEditOrigin,
                    env.envParams.m_bloomNew.shaftsThreshold.dataBaseType,
                    env.envParams.m_bloomNew.shaftsThreshold.CurveType);
            case "shaftsThresholdRange":
                s=env.envParams.m_bloomNew.shaftsThresholdRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_bloomNew.shaftsThresholdRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_bloomNew.shaftsThresholdRange.ScalarEditScale,
                    env.envParams.m_bloomNew.shaftsThresholdRange.ScalarEditOrigin,
                    env.envParams.m_bloomNew.shaftsThresholdRange.dataBaseType,
                    env.envParams.m_bloomNew.shaftsThresholdRange.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_globalFog(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "fogActivated":   return toBool(env.envParams.m_globalFog.fogActivated);
            case "fogAppearDistance":
                s=env.envParams.m_globalFog.fogAppearDistance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogAppearDistance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogAppearDistance.ScalarEditScale,
                    env.envParams.m_globalFog.fogAppearDistance.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogAppearDistance.dataBaseType,
                    env.envParams.m_globalFog.fogAppearDistance.CurveType);
            case "fogAppearRange":
                s=env.envParams.m_globalFog.fogAppearRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogAppearRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogAppearRange.ScalarEditScale,
                    env.envParams.m_globalFog.fogAppearRange.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogAppearRange.dataBaseType,
                    env.envParams.m_globalFog.fogAppearRange.CurveType);
            case "fogColorFront":
                s=env.envParams.m_globalFog.fogColorFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogColorFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogColorFront.ScalarEditScale,
                    env.envParams.m_globalFog.fogColorFront.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogColorFront.dataBaseType,
                    env.envParams.m_globalFog.fogColorFront.CurveType);
            case "fogColorMiddle":
                s=env.envParams.m_globalFog.fogColorMiddle.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogColorMiddle.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogColorMiddle.ScalarEditScale,
                    env.envParams.m_globalFog.fogColorMiddle.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogColorMiddle.dataBaseType,
                    env.envParams.m_globalFog.fogColorMiddle.CurveType);
            case "fogColorBack":
                s=env.envParams.m_globalFog.fogColorBack.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogColorBack.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogColorBack.ScalarEditScale,
                    env.envParams.m_globalFog.fogColorBack.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogColorBack.dataBaseType,
                    env.envParams.m_globalFog.fogColorBack.CurveType);
            case "fogDensity":
                s=env.envParams.m_globalFog.fogDensity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogDensity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogDensity.ScalarEditScale,
                    env.envParams.m_globalFog.fogDensity.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogDensity.dataBaseType,
                    env.envParams.m_globalFog.fogDensity.CurveType);
            case "fogFinalExp":
                s=env.envParams.m_globalFog.fogFinalExp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogFinalExp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogFinalExp.ScalarEditScale,
                    env.envParams.m_globalFog.fogFinalExp.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogFinalExp.dataBaseType,
                    env.envParams.m_globalFog.fogFinalExp.CurveType);
            case "fogDistClamp":
                s=env.envParams.m_globalFog.fogDistClamp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogDistClamp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogDistClamp.ScalarEditScale,
                    env.envParams.m_globalFog.fogDistClamp.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogDistClamp.dataBaseType,
                    env.envParams.m_globalFog.fogDistClamp.CurveType);
            case "fogVertOffset":
                s=env.envParams.m_globalFog.fogVertOffset.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogVertOffset.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogVertOffset.ScalarEditScale,
                    env.envParams.m_globalFog.fogVertOffset.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogVertOffset.dataBaseType,
                    env.envParams.m_globalFog.fogVertOffset.CurveType);
            case "fogVertDensity":
                s=env.envParams.m_globalFog.fogVertDensity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogVertDensity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogVertDensity.ScalarEditScale,
                    env.envParams.m_globalFog.fogVertDensity.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogVertDensity.dataBaseType,
                    env.envParams.m_globalFog.fogVertDensity.CurveType);
            case "fogVertDensityLightFront":
                s=env.envParams.m_globalFog.fogVertDensityLightFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogVertDensityLightFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogVertDensityLightFront.ScalarEditScale,
                    env.envParams.m_globalFog.fogVertDensityLightFront.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogVertDensityLightFront.dataBaseType,
                    env.envParams.m_globalFog.fogVertDensityLightFront.CurveType);
            case "fogVertDensityLightBack":
                s=env.envParams.m_globalFog.fogVertDensityLightBack.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogVertDensityLightBack.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogVertDensityLightBack.ScalarEditScale,
                    env.envParams.m_globalFog.fogVertDensityLightBack.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogVertDensityLightBack.dataBaseType,
                    env.envParams.m_globalFog.fogVertDensityLightBack.CurveType);
            case "fogSkyDensityScale":
                s=env.envParams.m_globalFog.fogSkyDensityScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogSkyDensityScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogSkyDensityScale.ScalarEditScale,
                    env.envParams.m_globalFog.fogSkyDensityScale.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogSkyDensityScale.dataBaseType,
                    env.envParams.m_globalFog.fogSkyDensityScale.CurveType);
            case "fogCloudsDensityScale":
                s=env.envParams.m_globalFog.fogCloudsDensityScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCloudsDensityScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCloudsDensityScale.ScalarEditScale,
                    env.envParams.m_globalFog.fogCloudsDensityScale.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCloudsDensityScale.dataBaseType,
                    env.envParams.m_globalFog.fogCloudsDensityScale.CurveType);
            case "fogSkyVertDensityLightFrontScale":
                s=env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.ScalarEditScale,
                    env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.dataBaseType,
                    env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.CurveType);
            case "fogSkyVertDensityLightBackScale":
                s=env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.ScalarEditScale,
                    env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.dataBaseType,
                    env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.CurveType);
            case "fogVertDensityRimRange":
                s=env.envParams.m_globalFog.fogVertDensityRimRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogVertDensityRimRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogVertDensityRimRange.ScalarEditScale,
                    env.envParams.m_globalFog.fogVertDensityRimRange.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogVertDensityRimRange.dataBaseType,
                    env.envParams.m_globalFog.fogVertDensityRimRange.CurveType);
            case "fogCustomColor":
                s=env.envParams.m_globalFog.fogCustomColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCustomColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCustomColor.ScalarEditScale,
                    env.envParams.m_globalFog.fogCustomColor.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCustomColor.dataBaseType,
                    env.envParams.m_globalFog.fogCustomColor.CurveType);
            case "fogCustomColorStart":
                s=env.envParams.m_globalFog.fogCustomColorStart.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCustomColorStart.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCustomColorStart.ScalarEditScale,
                    env.envParams.m_globalFog.fogCustomColorStart.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCustomColorStart.dataBaseType,
                    env.envParams.m_globalFog.fogCustomColorStart.CurveType);
            case "fogCustomColorRange":
                s=env.envParams.m_globalFog.fogCustomColorRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCustomColorRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCustomColorRange.ScalarEditScale,
                    env.envParams.m_globalFog.fogCustomColorRange.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCustomColorRange.dataBaseType,
                    env.envParams.m_globalFog.fogCustomColorRange.CurveType);
            case "fogCustomAmountScale":
                s=env.envParams.m_globalFog.fogCustomAmountScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCustomAmountScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCustomAmountScale.ScalarEditScale,
                    env.envParams.m_globalFog.fogCustomAmountScale.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCustomAmountScale.dataBaseType,
                    env.envParams.m_globalFog.fogCustomAmountScale.CurveType);
            case "fogCustomAmountScaleStart":
                s=env.envParams.m_globalFog.fogCustomAmountScaleStart.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCustomAmountScaleStart.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCustomAmountScaleStart.ScalarEditScale,
                    env.envParams.m_globalFog.fogCustomAmountScaleStart.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCustomAmountScaleStart.dataBaseType,
                    env.envParams.m_globalFog.fogCustomAmountScaleStart.CurveType);
            case "fogCustomAmountScaleRange":
                s=env.envParams.m_globalFog.fogCustomAmountScaleRange.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.fogCustomAmountScaleRange.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.fogCustomAmountScaleRange.ScalarEditScale,
                    env.envParams.m_globalFog.fogCustomAmountScaleRange.ScalarEditOrigin,
                    env.envParams.m_globalFog.fogCustomAmountScaleRange.dataBaseType,
                    env.envParams.m_globalFog.fogCustomAmountScaleRange.CurveType);
            case "aerialColorFront":
                s=env.envParams.m_globalFog.aerialColorFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.aerialColorFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.aerialColorFront.ScalarEditScale,
                    env.envParams.m_globalFog.aerialColorFront.ScalarEditOrigin,
                    env.envParams.m_globalFog.aerialColorFront.dataBaseType,
                    env.envParams.m_globalFog.aerialColorFront.CurveType);
            case "aerialColorMiddle":
                s=env.envParams.m_globalFog.aerialColorMiddle.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.aerialColorMiddle.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.aerialColorMiddle.ScalarEditScale,
                    env.envParams.m_globalFog.aerialColorMiddle.ScalarEditOrigin,
                    env.envParams.m_globalFog.aerialColorMiddle.dataBaseType,
                    env.envParams.m_globalFog.aerialColorMiddle.CurveType);
            case "aerialColorBack":
                s=env.envParams.m_globalFog.aerialColorBack.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.aerialColorBack.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.aerialColorBack.ScalarEditScale,
                    env.envParams.m_globalFog.aerialColorBack.ScalarEditOrigin,
                    env.envParams.m_globalFog.aerialColorBack.dataBaseType,
                    env.envParams.m_globalFog.aerialColorBack.CurveType);
            case "aerialFinalExp":
                s=env.envParams.m_globalFog.aerialFinalExp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.aerialFinalExp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.aerialFinalExp.ScalarEditScale,
                    env.envParams.m_globalFog.aerialFinalExp.ScalarEditOrigin,
                    env.envParams.m_globalFog.aerialFinalExp.dataBaseType,
                    env.envParams.m_globalFog.aerialFinalExp.CurveType);
            case "ssaoImpactClamp":
                s=env.envParams.m_globalFog.ssaoImpactClamp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.ssaoImpactClamp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.ssaoImpactClamp.ScalarEditScale,
                    env.envParams.m_globalFog.ssaoImpactClamp.ScalarEditOrigin,
                    env.envParams.m_globalFog.ssaoImpactClamp.dataBaseType,
                    env.envParams.m_globalFog.ssaoImpactClamp.CurveType);
            case "ssaoImpactNearValue":
                s=env.envParams.m_globalFog.ssaoImpactNearValue.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.ssaoImpactNearValue.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.ssaoImpactNearValue.ScalarEditScale,
                    env.envParams.m_globalFog.ssaoImpactNearValue.ScalarEditOrigin,
                    env.envParams.m_globalFog.ssaoImpactNearValue.dataBaseType,
                    env.envParams.m_globalFog.ssaoImpactNearValue.CurveType);
            case "ssaoImpactFarValue":
                s=env.envParams.m_globalFog.ssaoImpactFarValue.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.ssaoImpactFarValue.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.ssaoImpactFarValue.ScalarEditScale,
                    env.envParams.m_globalFog.ssaoImpactFarValue.ScalarEditOrigin,
                    env.envParams.m_globalFog.ssaoImpactFarValue.dataBaseType,
                    env.envParams.m_globalFog.ssaoImpactFarValue.CurveType);
            case "ssaoImpactNearDistance":
                s=env.envParams.m_globalFog.ssaoImpactNearDistance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.ssaoImpactNearDistance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.ssaoImpactNearDistance.ScalarEditScale,
                    env.envParams.m_globalFog.ssaoImpactNearDistance.ScalarEditOrigin,
                    env.envParams.m_globalFog.ssaoImpactNearDistance.dataBaseType,
                    env.envParams.m_globalFog.ssaoImpactNearDistance.CurveType);
            case "ssaoImpactFarDistance":
                s=env.envParams.m_globalFog.ssaoImpactFarDistance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.ssaoImpactFarDistance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.ssaoImpactFarDistance.ScalarEditScale,
                    env.envParams.m_globalFog.ssaoImpactFarDistance.ScalarEditOrigin,
                    env.envParams.m_globalFog.ssaoImpactFarDistance.dataBaseType,
                    env.envParams.m_globalFog.ssaoImpactFarDistance.CurveType);
            case "distantLightsIntensityScale":
                s=env.envParams.m_globalFog.distantLightsIntensityScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_globalFog.distantLightsIntensityScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_globalFog.distantLightsIntensityScale.ScalarEditScale,
                    env.envParams.m_globalFog.distantLightsIntensityScale.ScalarEditOrigin,
                    env.envParams.m_globalFog.distantLightsIntensityScale.dataBaseType,
                    env.envParams.m_globalFog.distantLightsIntensityScale.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_sky(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_sky.activated);
            case "activatedActivateFactor":   return toBool(env.envParams.m_sky.activatedActivateFactor);
            case "activateFactor":   return toFloat(env.envParams.m_sky.activateFactor);
            case "skyColor":
                s=env.envParams.m_sky.skyColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.skyColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.skyColor.ScalarEditScale,
                    env.envParams.m_sky.skyColor.ScalarEditOrigin,
                    env.envParams.m_sky.skyColor.dataBaseType,
                    env.envParams.m_sky.skyColor.CurveType);
            case "skyColorHorizon":
                s=env.envParams.m_sky.skyColorHorizon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.skyColorHorizon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.skyColorHorizon.ScalarEditScale,
                    env.envParams.m_sky.skyColorHorizon.ScalarEditOrigin,
                    env.envParams.m_sky.skyColorHorizon.dataBaseType,
                    env.envParams.m_sky.skyColorHorizon.CurveType);
            case "horizonVerticalAttenuation":
                s=env.envParams.m_sky.horizonVerticalAttenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.horizonVerticalAttenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.horizonVerticalAttenuation.ScalarEditScale,
                    env.envParams.m_sky.horizonVerticalAttenuation.ScalarEditOrigin,
                    env.envParams.m_sky.horizonVerticalAttenuation.dataBaseType,
                    env.envParams.m_sky.horizonVerticalAttenuation.CurveType);
            case "sunColorSky":
                s=env.envParams.m_sky.sunColorSky.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunColorSky.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunColorSky.ScalarEditScale,
                    env.envParams.m_sky.sunColorSky.ScalarEditOrigin,
                    env.envParams.m_sky.sunColorSky.dataBaseType,
                    env.envParams.m_sky.sunColorSky.CurveType);
            case "sunColorSkyBrightness":
                s=env.envParams.m_sky.sunColorSkyBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunColorSkyBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunColorSkyBrightness.ScalarEditScale,
                    env.envParams.m_sky.sunColorSkyBrightness.ScalarEditOrigin,
                    env.envParams.m_sky.sunColorSkyBrightness.dataBaseType,
                    env.envParams.m_sky.sunColorSkyBrightness.CurveType);
            case "sunAreaSkySize":
                s=env.envParams.m_sky.sunAreaSkySize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunAreaSkySize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunAreaSkySize.ScalarEditScale,
                    env.envParams.m_sky.sunAreaSkySize.ScalarEditOrigin,
                    env.envParams.m_sky.sunAreaSkySize.dataBaseType,
                    env.envParams.m_sky.sunAreaSkySize.CurveType);
            case "sunColorHorizon":
                s=env.envParams.m_sky.sunColorHorizon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunColorHorizon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunColorHorizon.ScalarEditScale,
                    env.envParams.m_sky.sunColorHorizon.ScalarEditOrigin,
                    env.envParams.m_sky.sunColorHorizon.dataBaseType,
                    env.envParams.m_sky.sunColorHorizon.CurveType);
            case "sunColorHorizonHorizontalScale":
                s=env.envParams.m_sky.sunColorHorizonHorizontalScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunColorHorizonHorizontalScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunColorHorizonHorizontalScale.ScalarEditScale,
                    env.envParams.m_sky.sunColorHorizonHorizontalScale.ScalarEditOrigin,
                    env.envParams.m_sky.sunColorHorizonHorizontalScale.dataBaseType,
                    env.envParams.m_sky.sunColorHorizonHorizontalScale.CurveType);
            case "sunBackHorizonColor":
                s=env.envParams.m_sky.sunBackHorizonColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunBackHorizonColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunBackHorizonColor.ScalarEditScale,
                    env.envParams.m_sky.sunBackHorizonColor.ScalarEditOrigin,
                    env.envParams.m_sky.sunBackHorizonColor.dataBaseType,
                    env.envParams.m_sky.sunBackHorizonColor.CurveType);
            case "sunInfluence":
                s=env.envParams.m_sky.sunInfluence.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.sunInfluence.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.sunInfluence.ScalarEditScale,
                    env.envParams.m_sky.sunInfluence.ScalarEditOrigin,
                    env.envParams.m_sky.sunInfluence.dataBaseType,
                    env.envParams.m_sky.sunInfluence.CurveType);
            case "moonColorSky":
                s=env.envParams.m_sky.moonColorSky.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonColorSky.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonColorSky.ScalarEditScale,
                    env.envParams.m_sky.moonColorSky.ScalarEditOrigin,
                    env.envParams.m_sky.moonColorSky.dataBaseType,
                    env.envParams.m_sky.moonColorSky.CurveType);
            case "moonColorSkyBrightness":
                s=env.envParams.m_sky.moonColorSkyBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonColorSkyBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonColorSkyBrightness.ScalarEditScale,
                    env.envParams.m_sky.moonColorSkyBrightness.ScalarEditOrigin,
                    env.envParams.m_sky.moonColorSkyBrightness.dataBaseType,
                    env.envParams.m_sky.moonColorSkyBrightness.CurveType);
            case "moonAreaSkySize":
                s=env.envParams.m_sky.moonAreaSkySize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonAreaSkySize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonAreaSkySize.ScalarEditScale,
                    env.envParams.m_sky.moonAreaSkySize.ScalarEditOrigin,
                    env.envParams.m_sky.moonAreaSkySize.dataBaseType,
                    env.envParams.m_sky.moonAreaSkySize.CurveType);
            case "moonColorHorizon":
                s=env.envParams.m_sky.moonColorHorizon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonColorHorizon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonColorHorizon.ScalarEditScale,
                    env.envParams.m_sky.moonColorHorizon.ScalarEditOrigin,
                    env.envParams.m_sky.moonColorHorizon.dataBaseType,
                    env.envParams.m_sky.moonColorHorizon.CurveType);
            case "moonColorHorizonHorizontalScale":
                s=env.envParams.m_sky.moonColorHorizonHorizontalScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonColorHorizonHorizontalScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonColorHorizonHorizontalScale.ScalarEditScale,
                    env.envParams.m_sky.moonColorHorizonHorizontalScale.ScalarEditOrigin,
                    env.envParams.m_sky.moonColorHorizonHorizontalScale.dataBaseType,
                    env.envParams.m_sky.moonColorHorizonHorizontalScale.CurveType);
            case "moonBackHorizonColor":
                s=env.envParams.m_sky.moonBackHorizonColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonBackHorizonColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonBackHorizonColor.ScalarEditScale,
                    env.envParams.m_sky.moonBackHorizonColor.ScalarEditOrigin,
                    env.envParams.m_sky.moonBackHorizonColor.dataBaseType,
                    env.envParams.m_sky.moonBackHorizonColor.CurveType);
            case "moonInfluence":
                s=env.envParams.m_sky.moonInfluence.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.moonInfluence.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.moonInfluence.ScalarEditScale,
                    env.envParams.m_sky.moonInfluence.ScalarEditOrigin,
                    env.envParams.m_sky.moonInfluence.dataBaseType,
                    env.envParams.m_sky.moonInfluence.CurveType);
            case "globalSkyBrightness":
                s=env.envParams.m_sky.globalSkyBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sky.globalSkyBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sky.globalSkyBrightness.ScalarEditScale,
                    env.envParams.m_sky.globalSkyBrightness.ScalarEditOrigin,
                    env.envParams.m_sky.globalSkyBrightness.dataBaseType,
                    env.envParams.m_sky.globalSkyBrightness.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_depthOfField(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_depthOfField.activated);
            case "nearBlurDist":
                s=env.envParams.m_depthOfField.nearBlurDist.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_depthOfField.nearBlurDist.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_depthOfField.nearBlurDist.ScalarEditScale,
                    env.envParams.m_depthOfField.nearBlurDist.ScalarEditOrigin,
                    env.envParams.m_depthOfField.nearBlurDist.dataBaseType,
                    env.envParams.m_depthOfField.nearBlurDist.CurveType);

            case "nearFocusDist":
                s=env.envParams.m_depthOfField.nearFocusDist.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_depthOfField.nearFocusDist.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_depthOfField.nearFocusDist.ScalarEditScale,
                    env.envParams.m_depthOfField.nearFocusDist.ScalarEditOrigin,
                    env.envParams.m_depthOfField.nearFocusDist.dataBaseType,
                    env.envParams.m_depthOfField.nearFocusDist.CurveType);

            case "farFocusDist":
                s=env.envParams.m_depthOfField.farFocusDist.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_depthOfField.farFocusDist.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_depthOfField.farFocusDist.ScalarEditScale,
                    env.envParams.m_depthOfField.farFocusDist.ScalarEditOrigin,
                    env.envParams.m_depthOfField.farFocusDist.dataBaseType,
                    env.envParams.m_depthOfField.farFocusDist.CurveType);

            case "farBlurDist":
                s=env.envParams.m_depthOfField.farBlurDist.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_depthOfField.farBlurDist.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_depthOfField.farBlurDist.ScalarEditScale,
                    env.envParams.m_depthOfField.farBlurDist.ScalarEditOrigin,
                    env.envParams.m_depthOfField.farBlurDist.dataBaseType,
                    env.envParams.m_depthOfField.farBlurDist.CurveType);

            case "intensity":
                s=env.envParams.m_depthOfField.intensity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_depthOfField.intensity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_depthOfField.intensity.ScalarEditScale,
                    env.envParams.m_depthOfField.intensity.ScalarEditOrigin,
                    env.envParams.m_depthOfField.intensity.dataBaseType,
                    env.envParams.m_depthOfField.intensity.CurveType);

            case "activatedSkyThreshold":return toBool(env.envParams.m_depthOfField.activatedSkyThreshold);
            case "skyThreshold":        return toFloat(env.envParams.m_depthOfField.skyThreshold);
            case "activatedSkyRange":   return toBool(env.envParams.m_depthOfField.activatedSkyRange);
            case "skyRange":            return toFloat(env.envParams.m_depthOfField.skyRange);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_colorModTransparency(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_colorModTransparency.activated);
            case "commonFarDist":
                s=env.envParams.m_colorModTransparency.commonFarDist.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.commonFarDist.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.commonFarDist.ScalarEditScale,
                    env.envParams.m_colorModTransparency.commonFarDist.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.commonFarDist.dataBaseType,
                    env.envParams.m_colorModTransparency.commonFarDist.CurveType);
            case "filterNearColor":
                s=env.envParams.m_colorModTransparency.filterNearColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.filterNearColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.filterNearColor.ScalarEditScale,
                    env.envParams.m_colorModTransparency.filterNearColor.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.filterNearColor.dataBaseType,
                    env.envParams.m_colorModTransparency.filterNearColor.CurveType);
            case "filterFarColor":
                s=env.envParams.m_colorModTransparency.filterFarColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.filterFarColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.filterFarColor.ScalarEditScale,
                    env.envParams.m_colorModTransparency.filterFarColor.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.filterFarColor.dataBaseType,
                    env.envParams.m_colorModTransparency.filterFarColor.CurveType);
            case "contrastNearStrength":
                s=env.envParams.m_colorModTransparency.contrastNearStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.contrastNearStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.contrastNearStrength.ScalarEditScale,
                    env.envParams.m_colorModTransparency.contrastNearStrength.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.contrastNearStrength.dataBaseType,
                    env.envParams.m_colorModTransparency.contrastNearStrength.CurveType);
            case "contrastFarStrength":
                s=env.envParams.m_colorModTransparency.contrastFarStrength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.contrastFarStrength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.contrastFarStrength.ScalarEditScale,
                    env.envParams.m_colorModTransparency.contrastFarStrength.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.contrastFarStrength.dataBaseType,
                    env.envParams.m_colorModTransparency.contrastFarStrength.CurveType);
            case "autoHideCustom0.activated":   return toBool(env.envParams.m_colorModTransparency.autoHideCustom0.activated);
            case "autoHideCustom0.distance":
                s=env.envParams.m_colorModTransparency.autoHideCustom0.distance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom0.distance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom0.distance.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom0.distance.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom0.distance.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom0.distance.CurveType);
            case "autoHideCustom0.range":
                s=env.envParams.m_colorModTransparency.autoHideCustom0.range.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom0.range.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom0.range.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom0.range.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom0.range.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom0.range.CurveType);
            case "autoHideCustom1.activated":   return toBool(env.envParams.m_colorModTransparency.autoHideCustom1.activated);
            case "autoHideCustom1.distance":
                s=env.envParams.m_colorModTransparency.autoHideCustom1.distance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom1.distance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom1.distance.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom1.distance.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom1.distance.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom1.distance.CurveType);
            case "autoHideCustom1.range":
                s=env.envParams.m_colorModTransparency.autoHideCustom1.range.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom1.range.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom1.range.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom1.range.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom1.range.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom1.range.CurveType);
            case "autoHideCustom2.activated":   return toBool(env.envParams.m_colorModTransparency.autoHideCustom2.activated);
            case "autoHideCustom2.distance":
                s=env.envParams.m_colorModTransparency.autoHideCustom2.distance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom2.distance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom2.distance.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom2.distance.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom2.distance.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom2.distance.CurveType);
            case "autoHideCustom2.range":
                s=env.envParams.m_colorModTransparency.autoHideCustom2.range.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom2.range.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom2.range.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom2.range.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom2.range.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom2.range.CurveType);
            case "autoHideCustom3.activated":   return toBool(env.envParams.m_colorModTransparency.autoHideCustom3.activated);
            case "autoHideCustom3.distance":
                s=env.envParams.m_colorModTransparency.autoHideCustom3.distance.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom3.distance.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom3.distance.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom3.distance.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom3.distance.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom3.distance.CurveType);
            case "autoHideCustom3.range":
                s=env.envParams.m_colorModTransparency.autoHideCustom3.range.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorModTransparency.autoHideCustom3.range.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorModTransparency.autoHideCustom3.range.ScalarEditScale,
                    env.envParams.m_colorModTransparency.autoHideCustom3.range.ScalarEditOrigin,
                    env.envParams.m_colorModTransparency.autoHideCustom3.range.dataBaseType,
                    env.envParams.m_colorModTransparency.autoHideCustom3.range.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_shadows(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activatedAutoHide":   return toBool(env.envParams.m_shadows.activatedAutoHide);
            case "autoHideBoxSizeMin":
                s=env.envParams.m_shadows.autoHideBoxSizeMin.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_shadows.autoHideBoxSizeMin.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_shadows.autoHideBoxSizeMin.ScalarEditScale,
                    env.envParams.m_shadows.autoHideBoxSizeMin.ScalarEditOrigin,
                    env.envParams.m_shadows.autoHideBoxSizeMin.dataBaseType,
                    env.envParams.m_shadows.autoHideBoxSizeMin.CurveType);
            case "autoHideBoxSizeMax":
                s=env.envParams.m_shadows.autoHideBoxSizeMax.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_shadows.autoHideBoxSizeMax.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_shadows.autoHideBoxSizeMax.ScalarEditScale,
                    env.envParams.m_shadows.autoHideBoxSizeMax.ScalarEditOrigin,
                    env.envParams.m_shadows.autoHideBoxSizeMax.dataBaseType,
                    env.envParams.m_shadows.autoHideBoxSizeMax.CurveType);
            case "autoHideBoxCompMaxX":
                s=env.envParams.m_shadows.autoHideBoxCompMaxX.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_shadows.autoHideBoxCompMaxX.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_shadows.autoHideBoxCompMaxX.ScalarEditScale,
                    env.envParams.m_shadows.autoHideBoxCompMaxX.ScalarEditOrigin,
                    env.envParams.m_shadows.autoHideBoxCompMaxX.dataBaseType,
                    env.envParams.m_shadows.autoHideBoxCompMaxX.CurveType);
            case "autoHideBoxCompMaxY":
                s=env.envParams.m_shadows.autoHideBoxCompMaxY.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_shadows.autoHideBoxCompMaxY.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_shadows.autoHideBoxCompMaxY.ScalarEditScale,
                    env.envParams.m_shadows.autoHideBoxCompMaxY.ScalarEditOrigin,
                    env.envParams.m_shadows.autoHideBoxCompMaxY.dataBaseType,
                    env.envParams.m_shadows.autoHideBoxCompMaxY.CurveType);
            case "autoHideBoxCompMaxZ":
                s=env.envParams.m_shadows.autoHideBoxCompMaxZ.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_shadows.autoHideBoxCompMaxZ.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_shadows.autoHideBoxCompMaxZ.ScalarEditScale,
                    env.envParams.m_shadows.autoHideBoxCompMaxZ.ScalarEditOrigin,
                    env.envParams.m_shadows.autoHideBoxCompMaxZ.dataBaseType,
                    env.envParams.m_shadows.autoHideBoxCompMaxZ.CurveType);
            case "autoHideDistScale":
                s=env.envParams.m_shadows.autoHideDistScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_shadows.autoHideDistScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_shadows.autoHideDistScale.ScalarEditScale,
                    env.envParams.m_shadows.autoHideDistScale.ScalarEditOrigin,
                    env.envParams.m_shadows.autoHideDistScale.dataBaseType,
                    env.envParams.m_shadows.autoHideDistScale.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_water(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_water.activated);
            case "waterFlowIntensity":
                s=env.envParams.m_water.waterFlowIntensity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterFlowIntensity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterFlowIntensity.ScalarEditScale,
                    env.envParams.m_water.waterFlowIntensity.ScalarEditOrigin,
                    env.envParams.m_water.waterFlowIntensity.dataBaseType,
                    env.envParams.m_water.waterFlowIntensity.CurveType);
            case "underwaterBrightness":
                s=env.envParams.m_water.underwaterBrightness.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.underwaterBrightness.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.underwaterBrightness.ScalarEditScale,
                    env.envParams.m_water.underwaterBrightness.ScalarEditOrigin,
                    env.envParams.m_water.underwaterBrightness.dataBaseType,
                    env.envParams.m_water.underwaterBrightness.CurveType);
            case "underWaterFogIntensity":
                s=env.envParams.m_water.underWaterFogIntensity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.underWaterFogIntensity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.underWaterFogIntensity.ScalarEditScale,
                    env.envParams.m_water.underWaterFogIntensity.ScalarEditOrigin,
                    env.envParams.m_water.underWaterFogIntensity.dataBaseType,
                    env.envParams.m_water.underWaterFogIntensity.CurveType);
            case "waterColor":
                s=env.envParams.m_water.waterColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterColor.ScalarEditScale,
                    env.envParams.m_water.waterColor.ScalarEditOrigin,
                    env.envParams.m_water.waterColor.dataBaseType,
                    env.envParams.m_water.waterColor.CurveType);
            case "underWaterColor":
                s=env.envParams.m_water.underWaterColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.underWaterColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.underWaterColor.ScalarEditScale,
                    env.envParams.m_water.underWaterColor.ScalarEditOrigin,
                    env.envParams.m_water.underWaterColor.dataBaseType,
                    env.envParams.m_water.underWaterColor.CurveType);
            case "waterFresnel":
                s=env.envParams.m_water.waterFresnel.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterFresnel.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterFresnel.ScalarEditScale,
                    env.envParams.m_water.waterFresnel.ScalarEditOrigin,
                    env.envParams.m_water.waterFresnel.dataBaseType,
                    env.envParams.m_water.waterFresnel.CurveType);
            case "waterCaustics":
                s=env.envParams.m_water.waterCaustics.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterCaustics.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterCaustics.ScalarEditScale,
                    env.envParams.m_water.waterCaustics.ScalarEditOrigin,
                    env.envParams.m_water.waterCaustics.dataBaseType,
                    env.envParams.m_water.waterCaustics.CurveType);
            case "waterFoamIntensity":
                s=env.envParams.m_water.waterFoamIntensity.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterFoamIntensity.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterFoamIntensity.ScalarEditScale,
                    env.envParams.m_water.waterFoamIntensity.ScalarEditOrigin,
                    env.envParams.m_water.waterFoamIntensity.dataBaseType,
                    env.envParams.m_water.waterFoamIntensity.CurveType);
            case "waterAmbientScale":
                s=env.envParams.m_water.waterAmbientScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterAmbientScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterAmbientScale.ScalarEditScale,
                    env.envParams.m_water.waterAmbientScale.ScalarEditOrigin,
                    env.envParams.m_water.waterAmbientScale.dataBaseType,
                    env.envParams.m_water.waterAmbientScale.CurveType);
            case "waterDiffuseScale":
                s=env.envParams.m_water.waterDiffuseScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_water.waterDiffuseScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_water.waterDiffuseScale.ScalarEditScale,
                    env.envParams.m_water.waterDiffuseScale.ScalarEditOrigin,
                    env.envParams.m_water.waterDiffuseScale.dataBaseType,
                    env.envParams.m_water.waterDiffuseScale.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_colorGroups(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_colorGroups.activated);
            case "defaultGroup":
                s=env.envParams.m_colorGroups.defaultGroup.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.defaultGroup.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.defaultGroup.ScalarEditScale,
                    env.envParams.m_colorGroups.defaultGroup.ScalarEditOrigin,
                    env.envParams.m_colorGroups.defaultGroup.dataBaseType,
                    env.envParams.m_colorGroups.defaultGroup.CurveType);
            case "lightsDefault":
                s=env.envParams.m_colorGroups.lightsDefault.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.lightsDefault.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.lightsDefault.ScalarEditScale,
                    env.envParams.m_colorGroups.lightsDefault.ScalarEditOrigin,
                    env.envParams.m_colorGroups.lightsDefault.dataBaseType,
                    env.envParams.m_colorGroups.lightsDefault.CurveType);
            case "lightsDawn":
                s=env.envParams.m_colorGroups.lightsDawn.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.lightsDawn.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.lightsDawn.ScalarEditScale,
                    env.envParams.m_colorGroups.lightsDawn.ScalarEditOrigin,
                    env.envParams.m_colorGroups.lightsDawn.dataBaseType,
                    env.envParams.m_colorGroups.lightsDawn.CurveType);
            case "lightsNoon":
                s=env.envParams.m_colorGroups.lightsNoon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.lightsNoon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.lightsNoon.ScalarEditScale,
                    env.envParams.m_colorGroups.lightsNoon.ScalarEditOrigin,
                    env.envParams.m_colorGroups.lightsNoon.dataBaseType,
                    env.envParams.m_colorGroups.lightsNoon.CurveType);
            case "lightsEvening":
                s=env.envParams.m_colorGroups.lightsEvening.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.lightsEvening.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.lightsEvening.ScalarEditScale,
                    env.envParams.m_colorGroups.lightsEvening.ScalarEditOrigin,
                    env.envParams.m_colorGroups.lightsEvening.dataBaseType,
                    env.envParams.m_colorGroups.lightsEvening.CurveType);
            case "lightsNight":
                s=env.envParams.m_colorGroups.lightsNight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.lightsNight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.lightsNight.ScalarEditScale,
                    env.envParams.m_colorGroups.lightsNight.ScalarEditOrigin,
                    env.envParams.m_colorGroups.lightsNight.dataBaseType,
                    env.envParams.m_colorGroups.lightsNight.CurveType);
            case "fxDefault":
                s=env.envParams.m_colorGroups.fxDefault.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxDefault.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxDefault.ScalarEditScale,
                    env.envParams.m_colorGroups.fxDefault.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxDefault.dataBaseType,
                    env.envParams.m_colorGroups.fxDefault.CurveType);
            case "fxFire":
                s=env.envParams.m_colorGroups.fxFire.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxFire.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxFire.ScalarEditScale,
                    env.envParams.m_colorGroups.fxFire.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxFire.dataBaseType,
                    env.envParams.m_colorGroups.fxFire.CurveType);
            case "fxFireFlares":
                s=env.envParams.m_colorGroups.fxFireFlares.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxFireFlares.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxFireFlares.ScalarEditScale,
                    env.envParams.m_colorGroups.fxFireFlares.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxFireFlares.dataBaseType,
                    env.envParams.m_colorGroups.fxFireFlares.CurveType);
            case "fxFireLight":
                s=env.envParams.m_colorGroups.fxFireLight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxFireLight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxFireLight.ScalarEditScale,
                    env.envParams.m_colorGroups.fxFireLight.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxFireLight.dataBaseType,
                    env.envParams.m_colorGroups.fxFireLight.CurveType);
            case "fxSmoke":
                s=env.envParams.m_colorGroups.fxSmoke.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSmoke.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSmoke.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSmoke.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSmoke.dataBaseType,
                    env.envParams.m_colorGroups.fxSmoke.CurveType);
            case "fxSmokeExplosion":
                s=env.envParams.m_colorGroups.fxSmokeExplosion.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSmokeExplosion.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSmokeExplosion.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSmokeExplosion.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSmokeExplosion.dataBaseType,
                    env.envParams.m_colorGroups.fxSmokeExplosion.CurveType);
            case "fxSky":
                s=env.envParams.m_colorGroups.fxSky.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSky.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSky.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSky.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSky.dataBaseType,
                    env.envParams.m_colorGroups.fxSky.CurveType);
            case "fxSkyAlpha":
                s=env.envParams.m_colorGroups.fxSkyAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyAlpha.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyAlpha.CurveType);
            case "fxSkyNight":
                s=env.envParams.m_colorGroups.fxSkyNight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyNight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyNight.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyNight.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyNight.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyNight.CurveType);
            case "fxSkyNightAlpha":
                s=env.envParams.m_colorGroups.fxSkyNightAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyNightAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyNightAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyNightAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyNightAlpha.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyNightAlpha.CurveType);
            case "fxSkyDawn":
                s=env.envParams.m_colorGroups.fxSkyDawn.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyDawn.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyDawn.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyDawn.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyDawn.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyDawn.CurveType);
            case "fxSkyDawnAlpha":
                s=env.envParams.m_colorGroups.fxSkyDawnAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyDawnAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyDawnAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyDawnAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyDawnAlpha.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyDawnAlpha.CurveType);
            case "fxSkyNoon":
                s=env.envParams.m_colorGroups.fxSkyNoon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyNoon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyNoon.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyNoon.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyNoon.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyNoon.CurveType);
            case "fxSkyNoonAlpha":
                s=env.envParams.m_colorGroups.fxSkyNoonAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyNoonAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyNoonAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyNoonAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyNoonAlpha.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyNoonAlpha.CurveType);
            case "fxSkySunset":
                s=env.envParams.m_colorGroups.fxSkySunset.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkySunset.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkySunset.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkySunset.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkySunset.dataBaseType,
                    env.envParams.m_colorGroups.fxSkySunset.CurveType);
            case "fxSkySunsetAlpha":
                s=env.envParams.m_colorGroups.fxSkySunsetAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkySunsetAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkySunsetAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkySunsetAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkySunsetAlpha.dataBaseType,
                    env.envParams.m_colorGroups.fxSkySunsetAlpha.CurveType);
            case "fxSkyRain":
                s=env.envParams.m_colorGroups.fxSkyRain.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyRain.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyRain.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyRain.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyRain.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyRain.CurveType);
            case "fxSkyRainAlpha":
                s=env.envParams.m_colorGroups.fxSkyRainAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxSkyRainAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxSkyRainAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.fxSkyRainAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxSkyRainAlpha.dataBaseType,
                    env.envParams.m_colorGroups.fxSkyRainAlpha.CurveType);
            case "mainCloudsMiddle":
                s=env.envParams.m_colorGroups.mainCloudsMiddle.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsMiddle.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsMiddle.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsMiddle.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsMiddle.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsMiddle.CurveType);
            case "mainCloudsMiddleAlpha":
                s=env.envParams.m_colorGroups.mainCloudsMiddleAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsMiddleAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsMiddleAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsMiddleAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsMiddleAlpha.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsMiddleAlpha.CurveType);
            case "mainCloudsFront":
                s=env.envParams.m_colorGroups.mainCloudsFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsFront.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsFront.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsFront.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsFront.CurveType);
            case "mainCloudsFrontAlpha":
                s=env.envParams.m_colorGroups.mainCloudsFrontAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsFrontAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsFrontAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsFrontAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsFrontAlpha.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsFrontAlpha.CurveType);
            case "mainCloudsBack":
                s=env.envParams.m_colorGroups.mainCloudsBack.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsBack.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsBack.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsBack.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsBack.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsBack.CurveType);
            case "mainCloudsBackAlpha":
                s=env.envParams.m_colorGroups.mainCloudsBackAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsBackAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsBackAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsBackAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsBackAlpha.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsBackAlpha.CurveType);
            case "mainCloudsRim":
                s=env.envParams.m_colorGroups.mainCloudsRim.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsRim.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsRim.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsRim.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsRim.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsRim.CurveType);
            case "mainCloudsRimAlpha":
                s=env.envParams.m_colorGroups.mainCloudsRimAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.mainCloudsRimAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.mainCloudsRimAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.mainCloudsRimAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.mainCloudsRimAlpha.dataBaseType,
                    env.envParams.m_colorGroups.mainCloudsRimAlpha.CurveType);
            case "backgroundCloudsFront":
                s=env.envParams.m_colorGroups.backgroundCloudsFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundCloudsFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundCloudsFront.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundCloudsFront.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundCloudsFront.dataBaseType,
                    env.envParams.m_colorGroups.backgroundCloudsFront.CurveType);
            case "backgroundCloudsFrontAlpha":
                s=env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.dataBaseType,
                    env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.CurveType);
            case "backgroundCloudsBack":
                s=env.envParams.m_colorGroups.backgroundCloudsBack.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundCloudsBack.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundCloudsBack.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundCloudsBack.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundCloudsBack.dataBaseType,
                    env.envParams.m_colorGroups.backgroundCloudsBack.CurveType);
            case "backgroundCloudsBackAlpha":
                s=env.envParams.m_colorGroups.backgroundCloudsBackAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundCloudsBackAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundCloudsBackAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundCloudsBackAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundCloudsBackAlpha.dataBaseType,
                    env.envParams.m_colorGroups.backgroundCloudsBackAlpha.CurveType);
            case "backgroundHazeFront":
                s=env.envParams.m_colorGroups.backgroundHazeFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundHazeFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundHazeFront.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundHazeFront.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundHazeFront.dataBaseType,
                    env.envParams.m_colorGroups.backgroundHazeFront.CurveType);
            case "backgroundHazeFrontAlpha":
                s=env.envParams.m_colorGroups.backgroundHazeFrontAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundHazeFrontAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundHazeFrontAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundHazeFrontAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundHazeFrontAlpha.dataBaseType,
                    env.envParams.m_colorGroups.backgroundHazeFrontAlpha.CurveType);
            case "backgroundHazeBack":
                s=env.envParams.m_colorGroups.backgroundHazeBack.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundHazeBack.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundHazeBack.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundHazeBack.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundHazeBack.dataBaseType,
                    env.envParams.m_colorGroups.backgroundHazeBack.CurveType);
            case "backgroundHazeBackAlpha":
                s=env.envParams.m_colorGroups.backgroundHazeBackAlpha.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.backgroundHazeBackAlpha.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.backgroundHazeBackAlpha.ScalarEditScale,
                    env.envParams.m_colorGroups.backgroundHazeBackAlpha.ScalarEditOrigin,
                    env.envParams.m_colorGroups.backgroundHazeBackAlpha.dataBaseType,
                    env.envParams.m_colorGroups.backgroundHazeBackAlpha.CurveType);
            case "fxBlood":
                s=env.envParams.m_colorGroups.fxBlood.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxBlood.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxBlood.ScalarEditScale,
                    env.envParams.m_colorGroups.fxBlood.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxBlood.dataBaseType,
                    env.envParams.m_colorGroups.fxBlood.CurveType);
            case "fxWater":
                s=env.envParams.m_colorGroups.fxWater.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxWater.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxWater.ScalarEditScale,
                    env.envParams.m_colorGroups.fxWater.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxWater.dataBaseType,
                    env.envParams.m_colorGroups.fxWater.CurveType);
            case "fxFog":
                s=env.envParams.m_colorGroups.fxFog.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxFog.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxFog.ScalarEditScale,
                    env.envParams.m_colorGroups.fxFog.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxFog.dataBaseType,
                    env.envParams.m_colorGroups.fxFog.CurveType);
            case "fxTrails":
                s=env.envParams.m_colorGroups.fxTrails.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxTrails.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxTrails.ScalarEditScale,
                    env.envParams.m_colorGroups.fxTrails.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxTrails.dataBaseType,
                    env.envParams.m_colorGroups.fxTrails.CurveType);
            case "fxScreenParticles":
                s=env.envParams.m_colorGroups.fxScreenParticles.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxScreenParticles.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxScreenParticles.ScalarEditScale,
                    env.envParams.m_colorGroups.fxScreenParticles.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxScreenParticles.dataBaseType,
                    env.envParams.m_colorGroups.fxScreenParticles.CurveType);
            case "fxLightShaft":
                s=env.envParams.m_colorGroups.fxLightShaft.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaft.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaft.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaft.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaft.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaft.CurveType);
            case "fxLightShaftSun":
                s=env.envParams.m_colorGroups.fxLightShaftSun.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftSun.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftSun.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftSun.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftSun.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftSun.CurveType);
            case "fxLightShaftInteriorDawn":
                s=env.envParams.m_colorGroups.fxLightShaftInteriorDawn.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftInteriorDawn.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftInteriorDawn.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftInteriorDawn.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftInteriorDawn.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftInteriorDawn.CurveType);
            case "fxLightShaftSpotlightDawn":
                s=env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.CurveType);
            case "fxLightShaftReflectionLightDawn":
                s=env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.CurveType);
            case "fxLightShaftInteriorNoon":
                s=env.envParams.m_colorGroups.fxLightShaftInteriorNoon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftInteriorNoon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNoon.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNoon.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNoon.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNoon.CurveType);
            case "fxLightShaftSpotlightNoon":
                s=env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.CurveType);
            case "fxLightShaftReflectionLightNoon":
                s=env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.CurveType);
            case "fxLightShaftInteriorEvening":
                s=env.envParams.m_colorGroups.fxLightShaftInteriorEvening.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftInteriorEvening.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftInteriorEvening.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftInteriorEvening.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftInteriorEvening.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftInteriorEvening.CurveType);
            case "fxLightShaftSpotlightEvening":
                s=env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.CurveType);
            case "fxLightShaftReflectionLightEvening":
                s=env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.CurveType);
            case "fxLightShaftInteriorNight":
                s=env.envParams.m_colorGroups.fxLightShaftInteriorNight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftInteriorNight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNight.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNight.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNight.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftInteriorNight.CurveType);
            case "fxLightShaftSpotlightNight":
                s=env.envParams.m_colorGroups.fxLightShaftSpotlightNight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftSpotlightNight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNight.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNight.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNight.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftSpotlightNight.CurveType);
            case "fxLightShaftReflectionLightNight":
                s=env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.ScalarEditScale,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.ScalarEditOrigin,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.dataBaseType,
                    env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.CurveType);
            case "activatedCustom0":   return toBool(env.envParams.m_colorGroups.activatedCustom0);
            case "customGroup0":
                s=env.envParams.m_colorGroups.customGroup0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.customGroup0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.customGroup0.ScalarEditScale,
                    env.envParams.m_colorGroups.customGroup0.ScalarEditOrigin,
                    env.envParams.m_colorGroups.customGroup0.dataBaseType,
                    env.envParams.m_colorGroups.customGroup0.CurveType);
            case "activatedCustom1":   return toBool(env.envParams.m_colorGroups.activatedCustom1);
            case "customGroup1":
                s=env.envParams.m_colorGroups.customGroup1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.customGroup1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.customGroup1.ScalarEditScale,
                    env.envParams.m_colorGroups.customGroup1.ScalarEditOrigin,
                    env.envParams.m_colorGroups.customGroup1.dataBaseType,
                    env.envParams.m_colorGroups.customGroup1.CurveType);
            case "activatedCustom2":   return toBool(env.envParams.m_colorGroups.activatedCustom2);
            case "customGroup2":
                s=env.envParams.m_colorGroups.customGroup2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_colorGroups.customGroup2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_colorGroups.customGroup2.ScalarEditScale,
                    env.envParams.m_colorGroups.customGroup2.ScalarEditOrigin,
                    env.envParams.m_colorGroups.customGroup2.dataBaseType,
                    env.envParams.m_colorGroups.customGroup2.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_flareColorGroups(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_flareColorGroups.activated);
            case "custom0.activated":   return toBool(env.envParams.m_flareColorGroups.custom0.activated);
            case "custom0.color0":
                s=env.envParams.m_flareColorGroups.custom0.color0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.color0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.color0.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.color0.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.color0.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.color0.CurveType);
            case "custom0.opacity0":
                s=env.envParams.m_flareColorGroups.custom0.opacity0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.opacity0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.opacity0.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.opacity0.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.opacity0.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.opacity0.CurveType);
            case "custom0.color1":
                s=env.envParams.m_flareColorGroups.custom0.color1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.color1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.color1.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.color1.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.color1.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.color1.CurveType);
            case "custom0.opacity1":
                s=env.envParams.m_flareColorGroups.custom0.opacity1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.opacity1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.opacity1.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.opacity1.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.opacity1.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.opacity1.CurveType);
            case "custom0.color2":
                s=env.envParams.m_flareColorGroups.custom0.color2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.color2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.color2.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.color2.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.color2.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.color2.CurveType);
            case "custom0.opacity2":
                s=env.envParams.m_flareColorGroups.custom0.opacity2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.opacity2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.opacity2.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.opacity2.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.opacity2.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.opacity2.CurveType);
            case "custom0.color3":
                s=env.envParams.m_flareColorGroups.custom0.color3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.color3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.color3.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.color3.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.color3.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.color3.CurveType);
            case "custom0.opacity3":
                s=env.envParams.m_flareColorGroups.custom0.opacity3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom0.opacity3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom0.opacity3.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom0.opacity3.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom0.opacity3.dataBaseType,
                    env.envParams.m_flareColorGroups.custom0.opacity3.CurveType);
            case "custom1.activated":   return toBool(env.envParams.m_flareColorGroups.custom1.activated);
            case "custom1.color0":
                s=env.envParams.m_flareColorGroups.custom1.color0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.color0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.color0.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.color0.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.color0.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.color0.CurveType);
            case "custom1.opacity0":
                s=env.envParams.m_flareColorGroups.custom1.opacity0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.opacity0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.opacity0.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.opacity0.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.opacity0.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.opacity0.CurveType);
            case "custom1.color1":
                s=env.envParams.m_flareColorGroups.custom1.color1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.color1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.color1.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.color1.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.color1.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.color1.CurveType);
            case "custom1.opacity1":
                s=env.envParams.m_flareColorGroups.custom1.opacity1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.opacity1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.opacity1.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.opacity1.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.opacity1.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.opacity1.CurveType);
            case "custom1.color2":
                s=env.envParams.m_flareColorGroups.custom1.color2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.color2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.color2.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.color2.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.color2.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.color2.CurveType);
            case "custom1.opacity2":
                s=env.envParams.m_flareColorGroups.custom1.opacity2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.opacity2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.opacity2.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.opacity2.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.opacity2.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.opacity2.CurveType);
            case "custom1.color3":
                s=env.envParams.m_flareColorGroups.custom1.color3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.color3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.color3.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.color3.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.color3.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.color3.CurveType);
            case "custom1.opacity3":
                s=env.envParams.m_flareColorGroups.custom1.opacity3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom1.opacity3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom1.opacity3.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom1.opacity3.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom1.opacity3.dataBaseType,
                    env.envParams.m_flareColorGroups.custom1.opacity3.CurveType);
            case "custom2.activated":   return toBool(env.envParams.m_flareColorGroups.custom2.activated);
            case "custom2.color0":
                s=env.envParams.m_flareColorGroups.custom2.color0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.color0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.color0.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.color0.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.color0.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.color0.CurveType);
            case "custom2.opacity0":
                s=env.envParams.m_flareColorGroups.custom2.opacity0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.opacity0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.opacity0.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.opacity0.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.opacity0.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.opacity0.CurveType);
            case "custom2.color1":
                s=env.envParams.m_flareColorGroups.custom2.color1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.color1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.color1.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.color1.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.color1.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.color1.CurveType);
            case "custom2.opacity1":
                s=env.envParams.m_flareColorGroups.custom2.opacity1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.opacity1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.opacity1.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.opacity1.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.opacity1.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.opacity1.CurveType);
            case "custom2.color2":
                s=env.envParams.m_flareColorGroups.custom2.color2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.color2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.color2.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.color2.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.color2.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.color2.CurveType);
            case "custom2.opacity2":
                s=env.envParams.m_flareColorGroups.custom2.opacity2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.opacity2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.opacity2.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.opacity2.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.opacity2.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.opacity2.CurveType);
            case "custom2.color3":
                s=env.envParams.m_flareColorGroups.custom2.color3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.color3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.color3.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.color3.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.color3.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.color3.CurveType);
            case "custom2.opacity3":
                s=env.envParams.m_flareColorGroups.custom2.opacity3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_flareColorGroups.custom2.opacity3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_flareColorGroups.custom2.opacity3.ScalarEditScale,
                    env.envParams.m_flareColorGroups.custom2.opacity3.ScalarEditOrigin,
                    env.envParams.m_flareColorGroups.custom2.opacity3.dataBaseType,
                    env.envParams.m_flareColorGroups.custom2.opacity3.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_sunAndMoonParams(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_sunAndMoonParams.activated);
            case "sunSize":
                s=env.envParams.m_sunAndMoonParams.sunSize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunSize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunSize.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunSize.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunSize.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunSize.CurveType);
            case "sunColor":
                s=env.envParams.m_sunAndMoonParams.sunColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunColor.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunColor.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunColor.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunColor.CurveType);
            case "sunFlareSize":
                s=env.envParams.m_sunAndMoonParams.sunFlareSize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareSize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareSize.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareSize.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareSize.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareSize.CurveType);
            case "sunFlareColor.activated":   return toBool(env.envParams.m_sunAndMoonParams.sunFlareColor.activated);
            case "sunFlareColor.color0":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.color0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.color0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color0.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color0.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color0.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color0.CurveType);
            case "sunFlareColor.opacity0":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.CurveType);
            case "sunFlareColor.color1":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.color1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.color1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color1.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color1.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color1.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color1.CurveType);
            case "sunFlareColor.opacity1":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.CurveType);
            case "sunFlareColor.color2":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.color2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.color2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color2.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color2.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color2.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color2.CurveType);
            case "sunFlareColor.opacity2":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.CurveType);
            case "sunFlareColor.color3":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.color3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.color3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color3.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color3.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color3.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.color3.CurveType);
            case "sunFlareColor.opacity3":
                s=env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.dataBaseType,
                    env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.CurveType);
            case "moonSize":
                s=env.envParams.m_sunAndMoonParams.moonSize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonSize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonSize.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonSize.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonSize.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonSize.CurveType);
            case "moonColor":
                s=env.envParams.m_sunAndMoonParams.moonColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonColor.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonColor.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonColor.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonColor.CurveType);
            case "moonFlareSize":
                s=env.envParams.m_sunAndMoonParams.moonFlareSize.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareSize.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareSize.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareSize.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareSize.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareSize.CurveType);
            case "moonFlareColor.activated":   return toBool(env.envParams.m_sunAndMoonParams.moonFlareColor.activated);
            case "moonFlareColor.color0":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.color0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.color0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color0.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color0.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color0.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color0.CurveType);
            case "moonFlareColor.opacity0":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.CurveType);
            case "moonFlareColor.color1":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.color1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.color1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color1.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color1.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color1.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color1.CurveType);
            case "moonFlareColor.opacity1":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.CurveType);
            case "moonFlareColor.color2":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.color2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.color2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color2.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color2.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color2.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color2.CurveType);
            case "moonFlareColor.opacity2":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.CurveType);
            case "moonFlareColor.color3":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.color3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.color3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color3.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color3.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color3.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.color3.CurveType);
            case "moonFlareColor.opacity3":
                s=env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.ScalarEditScale,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.ScalarEditOrigin,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.dataBaseType,
                    env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_windParams(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_windParams.activated);
            case "windStrengthOverride":
                s=env.envParams.m_windParams.windStrengthOverride.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_windParams.windStrengthOverride.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_windParams.windStrengthOverride.ScalarEditScale,
                    env.envParams.m_windParams.windStrengthOverride.ScalarEditOrigin,
                    env.envParams.m_windParams.windStrengthOverride.dataBaseType,
                    env.envParams.m_windParams.windStrengthOverride.CurveType);

            case "cloudsVelocityOverride":
                s=env.envParams.m_windParams.cloudsVelocityOverride.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_windParams.cloudsVelocityOverride.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_windParams.cloudsVelocityOverride.ScalarEditScale,
                    env.envParams.m_windParams.cloudsVelocityOverride.ScalarEditOrigin,
                    env.envParams.m_windParams.cloudsVelocityOverride.dataBaseType,
                    env.envParams.m_windParams.cloudsVelocityOverride.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_gameplayEffects(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_gameplayEffects.activated);
            case "catEffectBrightnessMultiply":
                s=env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.ScalarEditScale,
                    env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.ScalarEditOrigin,
                    env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.dataBaseType,
                    env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.CurveType);
            case "behaviorAnimationMultiplier":
                s=env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.ScalarEditScale,
                    env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.ScalarEditOrigin,
                    env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.dataBaseType,
                    env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.CurveType);
            case "specularityMultiplier":
                s=env.envParams.m_gameplayEffects.specularityMultiplier.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_gameplayEffects.specularityMultiplier.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_gameplayEffects.specularityMultiplier.ScalarEditScale,
                    env.envParams.m_gameplayEffects.specularityMultiplier.ScalarEditOrigin,
                    env.envParams.m_gameplayEffects.specularityMultiplier.dataBaseType,
                    env.envParams.m_gameplayEffects.specularityMultiplier.CurveType);
            case "glossinessMultiplier":
                s=env.envParams.m_gameplayEffects.glossinessMultiplier.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_gameplayEffects.glossinessMultiplier.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_gameplayEffects.glossinessMultiplier.ScalarEditScale,
                    env.envParams.m_gameplayEffects.glossinessMultiplier.ScalarEditOrigin,
                    env.envParams.m_gameplayEffects.glossinessMultiplier.dataBaseType,
                    env.envParams.m_gameplayEffects.glossinessMultiplier.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_motionBlur(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_motionBlur.activated);
            case "strength":
                s=env.envParams.m_motionBlur.strength.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_motionBlur.strength.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_motionBlur.strength.ScalarEditScale,
                    env.envParams.m_motionBlur.strength.ScalarEditOrigin,
                    env.envParams.m_motionBlur.strength.dataBaseType,
                    env.envParams.m_motionBlur.strength.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_cameraLightsSetup(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_cameraLightsSetup.activated);
            case "gameplayLight0.activated":   return toBool(env.envParams.m_cameraLightsSetup.gameplayLight0.activated);
            case "gameplayLight0.color":
                s=env.envParams.m_cameraLightsSetup.gameplayLight0.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight0.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.color.CurveType);
            case "gameplayLight0.attenuation":
                s=env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.CurveType);
            case "gameplayLight0.radius":
                s=env.envParams.m_cameraLightsSetup.gameplayLight0.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight0.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.radius.CurveType);
            case "gameplayLight0.offsetFront":
                s=env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.CurveType);
            case "gameplayLight0.offsetRight":
                s=env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.CurveType);
            case "gameplayLight0.offsetUp":
                s=env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.CurveType);
            case "gameplayLight1.activated":   return toBool(env.envParams.m_cameraLightsSetup.gameplayLight1.activated);
            case "gameplayLight1.color":
                s=env.envParams.m_cameraLightsSetup.gameplayLight1.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight1.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.color.CurveType);
            case "gameplayLight1.attenuation":
                s=env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.CurveType);
            case "gameplayLight1.radius":
                s=env.envParams.m_cameraLightsSetup.gameplayLight1.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight1.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.radius.CurveType);
            case "gameplayLight1.offsetFront":
                s=env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.CurveType);
            case "gameplayLight1.offsetRight":
                s=env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.CurveType);
            case "gameplayLight1.offsetUp":
                s=env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.CurveType);
            case "sceneLight0.activated":   return toBool(env.envParams.m_cameraLightsSetup.sceneLight0.activated);
            case "sceneLight0.color":
                s=env.envParams.m_cameraLightsSetup.sceneLight0.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight0.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight0.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight0.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight0.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight0.color.CurveType);
            case "sceneLight0.attenuation":
                s=env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.CurveType);
            case "sceneLight0.radius":
                s=env.envParams.m_cameraLightsSetup.sceneLight0.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight0.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight0.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight0.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight0.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight0.radius.CurveType);
            case "sceneLight0.offsetFront":
                s=env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.CurveType);
            case "sceneLight0.offsetRight":
                s=env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.CurveType);
            case "sceneLight0.offsetUp":
                s=env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.CurveType);
            case "sceneLight1.activated":   return toBool(env.envParams.m_cameraLightsSetup.sceneLight1.activated);
            case "sceneLight1.color":
                s=env.envParams.m_cameraLightsSetup.sceneLight1.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight1.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight1.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight1.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight1.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight1.color.CurveType);
            case "sceneLight1.attenuation":
                s=env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.CurveType);
            case "sceneLight1.radius":
                s=env.envParams.m_cameraLightsSetup.sceneLight1.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight1.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight1.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight1.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight1.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight1.radius.CurveType);
            case "sceneLight1.offsetFront":
                s=env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.CurveType);
            case "sceneLight1.offsetRight":
                s=env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.CurveType);
            case "sceneLight1.offsetUp":
                s=env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.CurveType);
            case "dialogLight0.activated":   return toBool(env.envParams.m_cameraLightsSetup.dialogLight0.activated);
            case "dialogLight0.color":
                s=env.envParams.m_cameraLightsSetup.dialogLight0.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight0.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight0.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight0.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight0.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight0.color.CurveType);
            case "dialogLight0.attenuation":
                s=env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.CurveType);
            case "dialogLight0.radius":
                s=env.envParams.m_cameraLightsSetup.dialogLight0.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight0.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight0.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight0.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight0.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight0.radius.CurveType);
            case "dialogLight0.offsetFront":
                s=env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.CurveType);
            case "dialogLight0.offsetRight":
                s=env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.CurveType);
            case "dialogLight0.offsetUp":
                s=env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.CurveType);
            case "dialogLight1.activated":   return toBool(env.envParams.m_cameraLightsSetup.dialogLight1.activated);
            case "dialogLight1.color":
                s=env.envParams.m_cameraLightsSetup.dialogLight1.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight1.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight1.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight1.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight1.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight1.color.CurveType);
            case "dialogLight1.attenuation":
                s=env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.CurveType);
            case "dialogLight1.radius":
                s=env.envParams.m_cameraLightsSetup.dialogLight1.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight1.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight1.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight1.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight1.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight1.radius.CurveType);
            case "dialogLight1.offsetFront":
                s=env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.CurveType);
            case "dialogLight1.offsetRight":
                s=env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.CurveType);
            case "dialogLight1.offsetUp":
                s=env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.CurveType);
            case "interiorLight0.activated":   return toBool(env.envParams.m_cameraLightsSetup.interiorLight0.activated);
            case "interiorLight0.color":
                s=env.envParams.m_cameraLightsSetup.interiorLight0.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight0.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight0.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight0.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight0.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight0.color.CurveType);
            case "interiorLight0.attenuation":
                s=env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.CurveType);
            case "interiorLight0.radius":
                s=env.envParams.m_cameraLightsSetup.interiorLight0.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight0.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight0.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight0.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight0.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight0.radius.CurveType);
            case "interiorLight0.offsetFront":
                s=env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.CurveType);
            case "interiorLight0.offsetRight":
                s=env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.CurveType);
            case "interiorLight0.offsetUp":
                s=env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.CurveType);
            case "interiorLight1.activated":   return toBool(env.envParams.m_cameraLightsSetup.interiorLight1.activated);
            case "interiorLight1.color":
                s=env.envParams.m_cameraLightsSetup.interiorLight1.color.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight1.color.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight1.color.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight1.color.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight1.color.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight1.color.CurveType);
            case "interiorLight1.attenuation":
                s=env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.CurveType);
            case "interiorLight1.radius":
                s=env.envParams.m_cameraLightsSetup.interiorLight1.radius.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight1.radius.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight1.radius.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight1.radius.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight1.radius.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight1.radius.CurveType);
            case "interiorLight1.offsetFront":
                s=env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.CurveType);
            case "interiorLight1.offsetRight":
                s=env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.CurveType);
            case "interiorLight1.offsetUp":
                s=env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.dataBaseType,
                    env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.CurveType);
            case "playerInInteriorLightsScale":
                s=env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.dataBaseType,
                    env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.CurveType);
            case "sceneLightColorInterior0":
                s=env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.CurveType);
            case "sceneLightColorInterior1":
                s=env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.dataBaseType,
                    env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.CurveType);
            case "cameraLightsNonCharacterScale":
                s=env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.ScalarEditScale,
                    env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.ScalarEditOrigin,
                    env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.dataBaseType,
                    env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    private function get_m_dialogLightParams(id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        switch (id) {
            case "activated":   return toBool(env.envParams.m_dialogLightParams.activated);
            case "lightColor":
                s=env.envParams.m_dialogLightParams.lightColor.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_dialogLightParams.lightColor.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_dialogLightParams.lightColor.ScalarEditScale,
                    env.envParams.m_dialogLightParams.lightColor.ScalarEditOrigin,
                    env.envParams.m_dialogLightParams.lightColor.dataBaseType,
                    env.envParams.m_dialogLightParams.lightColor.CurveType);
            case "lightColor2":
                s=env.envParams.m_dialogLightParams.lightColor2.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_dialogLightParams.lightColor2.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_dialogLightParams.lightColor2.ScalarEditScale,
                    env.envParams.m_dialogLightParams.lightColor2.ScalarEditOrigin,
                    env.envParams.m_dialogLightParams.lightColor2.dataBaseType,
                    env.envParams.m_dialogLightParams.lightColor2.CurveType);
            case "lightColor3":
                s=env.envParams.m_dialogLightParams.lightColor3.dataCurveValues.Size();
                for(i=0;i<s;i+=1){c.PushBack(env.envParams.m_dialogLightParams.lightColor3.dataCurveValues[i]);}
                return toCurve(c,
                    env.envParams.m_dialogLightParams.lightColor3.ScalarEditScale,
                    env.envParams.m_dialogLightParams.lightColor3.ScalarEditOrigin,
                    env.envParams.m_dialogLightParams.lightColor3.dataBaseType,
                    env.envParams.m_dialogLightParams.lightColor3.CurveType);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    public function getValue(cat1: String, id: String) : SEUI_Value {
        var null: SEUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;

        // top level partitioning
        switch (cat1) {
            case "finalColorBalance":   return get_m_finalColorBalance(id);
            case "sharpen":             return get_m_sharpen(id);
            case "paintEffect":         return get_m_paintEffect(id);
            case "ssaoNV":              return get_m_ssaoNV(id);
            case "ssaoMS":              return get_m_ssaoMS(id);
            case "globalLight":         return get_m_globalLight(id);
            case "interiorFallback":    return get_m_interiorFallback(id);
            case "speedTree":           return get_m_speedTree(id);
            case "toneMapping":         return get_m_toneMapping(id);
            case "bloomNew":            return get_m_bloomNew(id);
            case "globalFog":           return get_m_globalFog(id);
            case "sky":                 return get_m_sky(id);
            case "depthOfField":        return get_m_depthOfField(id);
            case "colorModTransparency":return get_m_colorModTransparency(id);
            case "shadows":             return get_m_shadows(id);
            case "water":               return get_m_water(id);
            case "colorGroups":         return get_m_colorGroups(id);
            case "flareColorGroups":    return get_m_flareColorGroups(id);
            case "sunAndMoonParams":    return get_m_sunAndMoonParams(id);
            case "windParams":          return get_m_windParams(id);
            case "gameplayEffects":     return get_m_gameplayEffects(id);
            case "motionBlur":          return get_m_motionBlur(id);
            case "cameraLightsSetup":   return get_m_cameraLightsSetup(id);
            case "dialogLightParams":   return get_m_dialogLightParams(id);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    private function set_m_finalColorBalance(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":                   env.envParams.m_finalColorBalance.activated = v.b; break;
            case "activatedBalanceMap":         env.envParams.m_finalColorBalance.activatedBalanceMap = v.b; break;
            case "activatedParametricBalance":  env.envParams.m_finalColorBalance.activatedParametricBalance = v.b; break;
            case "vignetteWeights":             env.envParams.m_finalColorBalance.vignetteWeights.dataCurveValues = v.curve.values; break;
            case "vignetteColor":               env.envParams.m_finalColorBalance.vignetteColor.dataCurveValues = v.curve.values; break;
            case "vignetteOpacity":             env.envParams.m_finalColorBalance.vignetteOpacity.dataCurveValues = v.curve.values; break;
            case "chromaticAberrationSize":     env.envParams.m_finalColorBalance.chromaticAberrationSize.dataCurveValues = v.curve.values; break;
            case "balanceMapLerp":              env.envParams.m_finalColorBalance.balanceMapLerp.dataCurveValues = v.curve.values; break;
            case "balanceMapAmount":            env.envParams.m_finalColorBalance.balanceMapAmount.dataCurveValues = v.curve.values; break;
            case "balancePostBrightness":       env.envParams.m_finalColorBalance.balancePostBrightness.dataCurveValues = v.curve.values; break;
            case "levelsShadows":               env.envParams.m_finalColorBalance.levelsShadows.dataCurveValues = v.curve.values; break;
            case "levelsMidtones":              env.envParams.m_finalColorBalance.levelsMidtones.dataCurveValues = v.curve.values; break;
            case "levelsHighlights":            env.envParams.m_finalColorBalance.levelsHighlights.dataCurveValues = v.curve.values; break;
            case "midtoneRangeMin":             env.envParams.m_finalColorBalance.midtoneRangeMin.dataCurveValues = v.curve.values; break;
            case "midtoneRangeMax":             env.envParams.m_finalColorBalance.midtoneRangeMax.dataCurveValues = v.curve.values; break;
            case "midtoneMarginMin":            env.envParams.m_finalColorBalance.midtoneMarginMin.dataCurveValues = v.curve.values; break;
            case "midtoneMarginMax":            env.envParams.m_finalColorBalance.midtoneMarginMax.dataCurveValues = v.curve.values; break;
            case "parametricBalanceLow.saturation": env.envParams.m_finalColorBalance.parametricBalanceLow.saturation.dataCurveValues = v.curve.values; break;
            case "parametricBalanceLow.color":      env.envParams.m_finalColorBalance.parametricBalanceLow.color.dataCurveValues = v.curve.values; break;
            case "parametricBalanceMid.saturation": env.envParams.m_finalColorBalance.parametricBalanceMid.saturation.dataCurveValues = v.curve.values; break;
            case "parametricBalanceMid.color":      env.envParams.m_finalColorBalance.parametricBalanceMid.color.dataCurveValues = v.curve.values; break;
            case "parametricBalanceHigh.saturation":env.envParams.m_finalColorBalance.parametricBalanceHigh.saturation.dataCurveValues = v.curve.values; break;
            case "parametricBalanceHigh.color":     env.envParams.m_finalColorBalance.parametricBalanceHigh.color.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_sharpen(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":       env.envParams.m_sharpen.activated = v.b; break;
            case "sharpenNear":     env.envParams.m_sharpen.sharpenNear.dataCurveValues = v.curve.values; break;
            case "sharpenFar":      env.envParams.m_sharpen.sharpenFar.dataCurveValues = v.curve.values; break;
            case "distanceNear":    env.envParams.m_sharpen.distanceNear.dataCurveValues = v.curve.values; break;
            case "distanceFar":     env.envParams.m_sharpen.distanceFar.dataCurveValues = v.curve.values; break;
            case "lumFilterOffset": env.envParams.m_sharpen.lumFilterOffset.dataCurveValues = v.curve.values; break;
            case "lumFilterRange":  env.envParams.m_sharpen.lumFilterRange.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_paintEffect(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":   env.envParams.m_paintEffect.activated = v.b; break;
            case "amount":      env.envParams.m_paintEffect.amount.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_ssaoNV(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_ssaoNV.activated = v.b; break;
            case "radius":              env.envParams.m_ssaoNV.radius.dataCurveValues = v.curve.values; break;
            case "bias":                env.envParams.m_ssaoNV.bias.dataCurveValues = v.curve.values; break;
            case "detailStrength":      env.envParams.m_ssaoNV.detailStrength.dataCurveValues = v.curve.values; break;
            case "coarseStrength":      env.envParams.m_ssaoNV.coarseStrength.dataCurveValues = v.curve.values; break;
            case "powerExponent":       env.envParams.m_ssaoNV.powerExponent.dataCurveValues = v.curve.values; break;
            case "blurSharpness":       env.envParams.m_ssaoNV.blurSharpness.dataCurveValues = v.curve.values; break;
            case "valueClamp":          env.envParams.m_ssaoNV.valueClamp.dataCurveValues = v.curve.values; break;
            case "ssaoColor":           env.envParams.m_ssaoNV.ssaoColor.dataCurveValues = v.curve.values; break;
            case "nonAmbientInfluence": env.envParams.m_ssaoNV.nonAmbientInfluence.dataCurveValues = v.curve.values; break;
            case "translucencyInfluence":   env.envParams.m_ssaoNV.translucencyInfluence.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_ssaoMS(id: String, v: SEUI_Value)  {
        switch (id) {
            case "activated":               env.envParams.m_ssaoMS.activated = v.b; break;
            case "noiseFilterTolerance":    env.envParams.m_ssaoMS.noiseFilterTolerance.dataCurveValues = v.curve.values; break;
            case "blurTolerance":           env.envParams.m_ssaoMS.blurTolerance.dataCurveValues = v.curve.values; break;
            case "upsampleTolerance":       env.envParams.m_ssaoMS.upsampleTolerance.dataCurveValues = v.curve.values; break;
            case "rejectionFalloff":        env.envParams.m_ssaoMS.rejectionFalloff.dataCurveValues = v.curve.values; break;
            case "combineResolutionsBeforeBlur":env.envParams.m_ssaoMS.combineResolutionsBeforeBlur = v.b; break;
            case "combineResolutionsWithMul":   env.envParams.m_ssaoMS.combineResolutionsWithMul = v.b; break;
            case "hierarchyDepth":          env.envParams.m_ssaoMS.hierarchyDepth.dataCurveValues = v.curve.values; break;
            case "normalAOMultiply":        env.envParams.m_ssaoMS.normalAOMultiply.dataCurveValues = v.curve.values; break;
            case "normalToDepthBrightnessEqualiser":env.envParams.m_ssaoMS.normalToDepthBrightnessEqualiser.dataCurveValues = v.curve.values; break;
            case "normalBackProjectionTolerance":   env.envParams.m_ssaoMS.normalBackProjectionTolerance.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_globalLight(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":                       env.envParams.m_globalLight.activated = v.b; break;
            case "activatedGlobalLightActivated":   env.envParams.m_globalLight.activatedGlobalLightActivated = v.b; break;
            case "globalLightActivated":            env.envParams.m_globalLight.globalLightActivated = v.f; break;
            case "activatedActivatedFactorLightDir":env.envParams.m_globalLight.activatedActivatedFactorLightDir = v.b; break;
            case "activatedFactorLightDir":         env.envParams.m_globalLight.activatedFactorLightDir = v.f; break;
            case "sunColor":                        env.envParams.m_globalLight.sunColor.dataCurveValues = v.curve.values; break;
            case "sunColorLightSide":               env.envParams.m_globalLight.sunColorLightSide.dataCurveValues = v.curve.values; break;
            case "sunColorLightOppositeSide":       env.envParams.m_globalLight.sunColorLightOppositeSide.dataCurveValues = v.curve.values; break;
            case "sunColorCenterArea":              env.envParams.m_globalLight.sunColorCenterArea.dataCurveValues = v.curve.values; break;
            case "sunColorSidesMargin":             env.envParams.m_globalLight.sunColorSidesMargin.dataCurveValues = v.curve.values; break;
            case "sunColorBottomHeight":            env.envParams.m_globalLight.sunColorBottomHeight.dataCurveValues = v.curve.values; break;
            case "sunColorTopHeight":               env.envParams.m_globalLight.sunColorTopHeight.dataCurveValues = v.curve.values; break;
            case "forcedLightDirAnglesYaw":         env.envParams.m_globalLight.forcedLightDirAnglesYaw.dataCurveValues = v.curve.values; break;
            case "forcedLightDirAnglesPitch":       env.envParams.m_globalLight.forcedLightDirAnglesPitch.dataCurveValues = v.curve.values; break;
            case "forcedLightDirAnglesRoll":        env.envParams.m_globalLight.forcedLightDirAnglesRoll.dataCurveValues = v.curve.values; break;
            case "forcedSunDirAnglesYaw":           env.envParams.m_globalLight.forcedSunDirAnglesYaw.dataCurveValues = v.curve.values; break;
            case "forcedSunDirAnglesPitch":         env.envParams.m_globalLight.forcedSunDirAnglesPitch.dataCurveValues = v.curve.values; break;
            case "forcedSunDirAnglesRoll":          env.envParams.m_globalLight.forcedSunDirAnglesRoll.dataCurveValues = v.curve.values; break;
            case "forcedMoonDirAnglesYaw":          env.envParams.m_globalLight.forcedMoonDirAnglesYaw.dataCurveValues = v.curve.values; break;
            case "forcedMoonDirAnglesPitch":        env.envParams.m_globalLight.forcedMoonDirAnglesPitch.dataCurveValues = v.curve.values; break;
            case "forcedMoonDirAnglesRoll":         env.envParams.m_globalLight.forcedMoonDirAnglesRoll.dataCurveValues = v.curve.values; break;
            case "translucencyViewDependency":      env.envParams.m_globalLight.translucencyViewDependency.dataCurveValues = v.curve.values; break;
            case "translucencyBaseFlatness":        env.envParams.m_globalLight.translucencyBaseFlatness.dataCurveValues = v.curve.values; break;
            case "translucencyFlatBrightness":      env.envParams.m_globalLight.translucencyFlatBrightness.dataCurveValues = v.curve.values; break;
            case "translucencyGainBrightness":      env.envParams.m_globalLight.translucencyGainBrightness.dataCurveValues = v.curve.values; break;
            case "translucencyFresnelScaleLight":           env.envParams.m_globalLight.translucencyFresnelScaleLight.dataCurveValues = v.curve.values; break;
            case "translucencyFresnelScaleReflection":      env.envParams.m_globalLight.translucencyFresnelScaleReflection.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingAmbient.activated":   env.envParams.m_globalLight.envProbeBaseLightingAmbient.activated = v.b; break;
            case "envProbeBaseLightingAmbient.colorAmbient":    env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorAmbient.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingAmbient.colorSceneAdd":   env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSceneAdd.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingAmbient.colorSkyTop":     env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyTop.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingAmbient.colorSkyHorizon": env.envParams.m_globalLight.envProbeBaseLightingAmbient.colorSkyHorizon.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingAmbient.skyShape":        env.envParams.m_globalLight.envProbeBaseLightingAmbient.skyShape.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.activated":    env.envParams.m_globalLight.envProbeBaseLightingReflection.activated = v.b; break;
            case "envProbeBaseLightingReflection.colorAmbient": env.envParams.m_globalLight.envProbeBaseLightingReflection.colorAmbient.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.colorSceneMul":env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneMul.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.colorSceneAdd":env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSceneAdd.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.colorSkyMul":  env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyMul.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.colorSkyAdd":  env.envParams.m_globalLight.envProbeBaseLightingReflection.colorSkyAdd.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.remapOffset":  env.envParams.m_globalLight.envProbeBaseLightingReflection.remapOffset.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.remapStrength":env.envParams.m_globalLight.envProbeBaseLightingReflection.remapStrength.dataCurveValues = v.curve.values; break;
            case "envProbeBaseLightingReflection.remapClamp":   env.envParams.m_globalLight.envProbeBaseLightingReflection.remapClamp.dataCurveValues = v.curve.values; break;
            case "charactersLightingBoostAmbientLight":         env.envParams.m_globalLight.charactersLightingBoostAmbientLight.dataCurveValues = v.curve.values; break;
            case "charactersLightingBoostAmbientShadow":        env.envParams.m_globalLight.charactersLightingBoostAmbientShadow.dataCurveValues = v.curve.values; break;
            case "charactersLightingBoostReflectionLight":      env.envParams.m_globalLight.charactersLightingBoostReflectionLight.dataCurveValues = v.curve.values; break;
            case "charactersLightingBoostReflectionShadow":     env.envParams.m_globalLight.charactersLightingBoostReflectionShadow.dataCurveValues = v.curve.values; break;
            case "charactersEyeBlicksColor":        env.envParams.m_globalLight.charactersEyeBlicksColor.dataCurveValues = v.curve.values; break;
            case "charactersEyeBlicksShadowedScale":env.envParams.m_globalLight.charactersEyeBlicksShadowedScale.dataCurveValues = v.curve.values; break;
            case "envProbeAmbientScaleLight":       env.envParams.m_globalLight.envProbeAmbientScaleLight.dataCurveValues = v.curve.values; break;
            case "envProbeAmbientScaleShadow":      env.envParams.m_globalLight.envProbeAmbientScaleShadow.dataCurveValues = v.curve.values; break;
            case "envProbeReflectionScaleLight":    env.envParams.m_globalLight.envProbeReflectionScaleLight.dataCurveValues = v.curve.values; break;
            case "envProbeReflectionScaleShadow":   env.envParams.m_globalLight.envProbeReflectionScaleShadow.dataCurveValues = v.curve.values; break;
            case "envProbeDistantScaleFactor":      env.envParams.m_globalLight.envProbeDistantScaleFactor.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_interiorFallback(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_interiorFallback.activated = v.b; break;
            case "colorAmbientMul":     env.envParams.m_interiorFallback.colorAmbientMul.dataCurveValues = v.curve.values; break;
            case "colorReflectionLow":  env.envParams.m_interiorFallback.colorReflectionLow.dataCurveValues = v.curve.values; break;
            case "colorReflectionMiddle":   env.envParams.m_interiorFallback.colorReflectionMiddle.dataCurveValues = v.curve.values; break;
            case "colorReflectionHigh": env.envParams.m_interiorFallback.colorReflectionHigh.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_speedTree(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_speedTree.activated = v.b; break;
            case "diffuse":             env.envParams.m_speedTree.diffuse.dataCurveValues = v.curve.values; break;
            case "specularScale":       env.envParams.m_speedTree.specularScale.dataCurveValues = v.curve.values; break;
            case "translucencyScale":   env.envParams.m_speedTree.translucencyScale.dataCurveValues = v.curve.values; break;
            case "ambientOcclusionScale":   env.envParams.m_speedTree.ambientOcclusionScale.dataCurveValues = v.curve.values; break;
            case "billboardsColor":         env.envParams.m_speedTree.billboardsColor.dataCurveValues = v.curve.values; break;
            case "billboardsTranslucency":  env.envParams.m_speedTree.billboardsTranslucency.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.luminanceWeights":  env.envParams.m_speedTree.randomColorsTrees.luminanceWeights.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.randomColor0":      env.envParams.m_speedTree.randomColorsTrees.randomColor0.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.saturation0":       env.envParams.m_speedTree.randomColorsTrees.saturation0.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.randomColor1":      env.envParams.m_speedTree.randomColorsTrees.randomColor1.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.saturation1":       env.envParams.m_speedTree.randomColorsTrees.saturation1.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.randomColor2":      env.envParams.m_speedTree.randomColorsTrees.randomColor2.dataCurveValues = v.curve.values; break;
            case "randomColorsTrees.saturation2":       env.envParams.m_speedTree.randomColorsTrees.saturation2.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.luminanceWeights":   env.envParams.m_speedTree.randomColorsBranches.luminanceWeights.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.randomColor0":   env.envParams.m_speedTree.randomColorsBranches.randomColor0.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.saturation0":    env.envParams.m_speedTree.randomColorsBranches.saturation0.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.randomColor1":   env.envParams.m_speedTree.randomColorsBranches.randomColor1.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.saturation1":    env.envParams.m_speedTree.randomColorsBranches.saturation1.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.randomColor2":   env.envParams.m_speedTree.randomColorsBranches.randomColor2.dataCurveValues = v.curve.values; break;
            case "randomColorsBranches.saturation2":    env.envParams.m_speedTree.randomColorsBranches.saturation2.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.luminanceWeights":  env.envParams.m_speedTree.randomColorsGrass.luminanceWeights.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.randomColor0":      env.envParams.m_speedTree.randomColorsGrass.randomColor0.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.saturation0":       env.envParams.m_speedTree.randomColorsGrass.saturation0.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.randomColor1":      env.envParams.m_speedTree.randomColorsGrass.randomColor1.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.saturation1":       env.envParams.m_speedTree.randomColorsGrass.saturation1.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.randomColor2":      env.envParams.m_speedTree.randomColorsGrass.randomColor2.dataCurveValues = v.curve.values; break;
            case "randomColorsGrass.saturation2":       env.envParams.m_speedTree.randomColorsGrass.saturation2.dataCurveValues = v.curve.values; break;
            case "randomColorsFallback":    env.envParams.m_speedTree.randomColorsFallback.dataCurveValues = v.curve.values; break;
            case "pigmentBrightness":       env.envParams.m_speedTree.pigmentBrightness.dataCurveValues = v.curve.values; break;
            case "pigmentFloodStartDist":   env.envParams.m_speedTree.pigmentFloodStartDist.dataCurveValues = v.curve.values; break;
            case "pigmentFloodRange":       env.envParams.m_speedTree.pigmentFloodRange.dataCurveValues = v.curve.values; break;
            case "billboardsLightBleed":    env.envParams.m_speedTree.billboardsLightBleed.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_toneMapping(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":               env.envParams.m_toneMapping.activated = v.b; break;
            case "skyLuminanceCustomValue": env.envParams.m_toneMapping.skyLuminanceCustomValue.dataCurveValues = v.curve.values; break;
            case "skyLuminanceCustomAmount":env.envParams.m_toneMapping.skyLuminanceCustomAmount.dataCurveValues = v.curve.values; break;
            case "luminanceLimitShape":     env.envParams.m_toneMapping.luminanceLimitShape.dataCurveValues = v.curve.values; break;
            case "luminanceLimitMin":       env.envParams.m_toneMapping.luminanceLimitMin.dataCurveValues = v.curve.values; break;
            case "luminanceLimitMax":       env.envParams.m_toneMapping.luminanceLimitMax.dataCurveValues = v.curve.values; break;
            case "rejectThreshold":         env.envParams.m_toneMapping.rejectThreshold.dataCurveValues = v.curve.values; break;
            case "rejectSmoothExtent":      env.envParams.m_toneMapping.rejectSmoothExtent.dataCurveValues = v.curve.values; break;
            case "newToneMapCurveParameters.shoulderStrength":  env.envParams.m_toneMapping.newToneMapCurveParameters.shoulderStrength.dataCurveValues = v.curve.values; break;
            case "newToneMapCurveParameters.linearStrength":    env.envParams.m_toneMapping.newToneMapCurveParameters.linearStrength.dataCurveValues = v.curve.values; break;
            case "newToneMapCurveParameters.linearAngle":       env.envParams.m_toneMapping.newToneMapCurveParameters.linearAngle.dataCurveValues = v.curve.values; break;
            case "newToneMapCurveParameters.toeStrength":       env.envParams.m_toneMapping.newToneMapCurveParameters.toeStrength.dataCurveValues = v.curve.values; break;
            case "newToneMapCurveParameters.toeNumerator":      env.envParams.m_toneMapping.newToneMapCurveParameters.toeNumerator.dataCurveValues = v.curve.values; break;
            case "newToneMapCurveParameters.toeDenominator":    env.envParams.m_toneMapping.newToneMapCurveParameters.toeDenominator.dataCurveValues = v.curve.values; break;
            case "newToneMapWhitepoint":    env.envParams.m_toneMapping.newToneMapWhitepoint.dataCurveValues = v.curve.values; break;
            case "newToneMapPostScale":     env.envParams.m_toneMapping.newToneMapPostScale.dataCurveValues = v.curve.values; break;
            case "exposureScale":           env.envParams.m_toneMapping.exposureScale.dataCurveValues = v.curve.values; break;
            case "postScale":               env.envParams.m_toneMapping.postScale.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_bloomNew(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_bloomNew.activated = v.b; break;
            case "brightPassWeights":   env.envParams.m_bloomNew.brightPassWeights.dataCurveValues = v.curve.values; break;
            case "color":               env.envParams.m_bloomNew.color.dataCurveValues = v.curve.values; break;
            case "dirtColor":           env.envParams.m_bloomNew.dirtColor.dataCurveValues = v.curve.values; break;
            case "threshold":           env.envParams.m_bloomNew.threshold.dataCurveValues = v.curve.values; break;
            case "thresholdRange":      env.envParams.m_bloomNew.thresholdRange.dataCurveValues = v.curve.values; break;
            case "brightnessMax":       env.envParams.m_bloomNew.brightnessMax.dataCurveValues = v.curve.values; break;
            case "shaftsColor":         env.envParams.m_bloomNew.shaftsColor.dataCurveValues = v.curve.values; break;
            case "shaftsRadius":        env.envParams.m_bloomNew.shaftsRadius.dataCurveValues = v.curve.values; break;
            case "shaftsShapeExp":      env.envParams.m_bloomNew.shaftsShapeExp.dataCurveValues = v.curve.values; break;
            case "shaftsShapeInvSquare":env.envParams.m_bloomNew.shaftsShapeInvSquare.dataCurveValues = v.curve.values; break;
            case "shaftsThreshold":     env.envParams.m_bloomNew.shaftsThreshold.dataCurveValues = v.curve.values; break;
            case "shaftsThresholdRange":env.envParams.m_bloomNew.shaftsThresholdRange.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_globalFog(id: String, v: SEUI_Value) {
        switch (id) {
            case "fogActivated":        env.envParams.m_globalFog.fogActivated = v.b; break;
            case "fogAppearDistance":   env.envParams.m_globalFog.fogAppearDistance.dataCurveValues = v.curve.values; break;
            case "fogAppearRange":      env.envParams.m_globalFog.fogAppearRange.dataCurveValues = v.curve.values; break;
            case "fogColorFront":       env.envParams.m_globalFog.fogColorFront.dataCurveValues = v.curve.values; break;
            case "fogColorMiddle":      env.envParams.m_globalFog.fogColorMiddle.dataCurveValues = v.curve.values; break;
            case "fogColorBack":        env.envParams.m_globalFog.fogColorBack.dataCurveValues = v.curve.values; break;
            case "fogDensity":          env.envParams.m_globalFog.fogDensity.dataCurveValues = v.curve.values; break;
            case "fogFinalExp":         env.envParams.m_globalFog.fogFinalExp.dataCurveValues = v.curve.values; break;
            case "fogDistClamp":        env.envParams.m_globalFog.fogDistClamp.dataCurveValues = v.curve.values; break;
            case "fogVertOffset":       env.envParams.m_globalFog.fogVertOffset.dataCurveValues = v.curve.values; break;
            case "fogVertDensity":      env.envParams.m_globalFog.fogVertDensity.dataCurveValues = v.curve.values; break;
            case "fogVertDensityLightFront":env.envParams.m_globalFog.fogVertDensityLightFront.dataCurveValues = v.curve.values; break;
            case "fogVertDensityLightBack": env.envParams.m_globalFog.fogVertDensityLightBack.dataCurveValues = v.curve.values; break;
            case "fogSkyDensityScale":      env.envParams.m_globalFog.fogSkyDensityScale.dataCurveValues = v.curve.values; break;
            case "fogCloudsDensityScale":   env.envParams.m_globalFog.fogCloudsDensityScale.dataCurveValues = v.curve.values; break;
            case "fogSkyVertDensityLightFrontScale":    env.envParams.m_globalFog.fogSkyVertDensityLightFrontScale.dataCurveValues = v.curve.values; break;
            case "fogSkyVertDensityLightBackScale":     env.envParams.m_globalFog.fogSkyVertDensityLightBackScale.dataCurveValues = v.curve.values; break;
            case "fogVertDensityRimRange":  env.envParams.m_globalFog.fogVertDensityRimRange.dataCurveValues = v.curve.values; break;
            case "fogCustomColor":          env.envParams.m_globalFog.fogCustomColor.dataCurveValues = v.curve.values; break;
            case "fogCustomColorStart":     env.envParams.m_globalFog.fogCustomColorStart.dataCurveValues = v.curve.values; break;
            case "fogCustomColorRange":     env.envParams.m_globalFog.fogCustomColorRange.dataCurveValues = v.curve.values; break;
            case "fogCustomAmountScale":    env.envParams.m_globalFog.fogCustomAmountScale.dataCurveValues = v.curve.values; break;
            case "fogCustomAmountScaleStart":   env.envParams.m_globalFog.fogCustomAmountScaleStart.dataCurveValues = v.curve.values; break;
            case "fogCustomAmountScaleRange":   env.envParams.m_globalFog.fogCustomAmountScaleRange.dataCurveValues = v.curve.values; break;
            case "aerialColorFront":        env.envParams.m_globalFog.aerialColorFront.dataCurveValues = v.curve.values; break;
            case "aerialColorMiddle":       env.envParams.m_globalFog.aerialColorMiddle.dataCurveValues = v.curve.values; break;
            case "aerialColorBack":         env.envParams.m_globalFog.aerialColorBack.dataCurveValues = v.curve.values; break;
            case "aerialFinalExp":          env.envParams.m_globalFog.aerialFinalExp.dataCurveValues = v.curve.values; break;
            case "ssaoImpactClamp":         env.envParams.m_globalFog.ssaoImpactClamp.dataCurveValues = v.curve.values; break;
            case "ssaoImpactNearValue":     env.envParams.m_globalFog.ssaoImpactNearValue.dataCurveValues = v.curve.values; break;
            case "ssaoImpactFarValue":      env.envParams.m_globalFog.ssaoImpactFarValue.dataCurveValues = v.curve.values; break;
            case "ssaoImpactNearDistance":  env.envParams.m_globalFog.ssaoImpactNearDistance.dataCurveValues = v.curve.values; break;
            case "ssaoImpactFarDistance":   env.envParams.m_globalFog.ssaoImpactFarDistance.dataCurveValues = v.curve.values; break;
            case "distantLightsIntensityScale": env.envParams.m_globalFog.distantLightsIntensityScale.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_sky(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":               env.envParams.m_sky.activated = v.b; break;
            case "activatedActivateFactor": env.envParams.m_sky.activatedActivateFactor = v.b; break;
            case "activateFactor":          env.envParams.m_sky.activateFactor = v.f; break;
            case "skyColor":                env.envParams.m_sky.skyColor.dataCurveValues = v.curve.values; break;
            case "skyColorHorizon":         env.envParams.m_sky.skyColorHorizon.dataCurveValues = v.curve.values; break;
            case "horizonVerticalAttenuation":  env.envParams.m_sky.horizonVerticalAttenuation.dataCurveValues = v.curve.values; break;
            case "sunColorSky":             env.envParams.m_sky.sunColorSky.dataCurveValues = v.curve.values; break;
            case "sunColorSkyBrightness":   env.envParams.m_sky.sunColorSkyBrightness.dataCurveValues = v.curve.values; break;
            case "sunAreaSkySize":          env.envParams.m_sky.sunAreaSkySize.dataCurveValues = v.curve.values; break;
            case "sunColorHorizon":         env.envParams.m_sky.sunColorHorizon.dataCurveValues = v.curve.values; break;
            case "sunColorHorizonHorizontalScale":  env.envParams.m_sky.sunColorHorizonHorizontalScale.dataCurveValues = v.curve.values; break;
            case "sunBackHorizonColor":     env.envParams.m_sky.sunBackHorizonColor.dataCurveValues = v.curve.values; break;
            case "sunInfluence":            env.envParams.m_sky.sunInfluence.dataCurveValues = v.curve.values; break;
            case "moonColorSky":            env.envParams.m_sky.moonColorSky.dataCurveValues = v.curve.values; break;
            case "moonColorSkyBrightness":  env.envParams.m_sky.moonColorSkyBrightness.dataCurveValues = v.curve.values; break;
            case "moonAreaSkySize":         env.envParams.m_sky.moonAreaSkySize.dataCurveValues = v.curve.values; break;
            case "moonColorHorizon":        env.envParams.m_sky.moonColorHorizon.dataCurveValues = v.curve.values; break;
            case "moonColorHorizonHorizontalScale": env.envParams.m_sky.moonColorHorizonHorizontalScale.dataCurveValues = v.curve.values; break;
            case "moonBackHorizonColor":    env.envParams.m_sky.moonBackHorizonColor.dataCurveValues = v.curve.values; break;
            case "moonInfluence":           env.envParams.m_sky.moonInfluence.dataCurveValues = v.curve.values; break;
            case "globalSkyBrightness":     env.envParams.m_sky.globalSkyBrightness.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_depthOfField(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_depthOfField.activated = v.b; break;
            case "nearBlurDist":        env.envParams.m_depthOfField.nearBlurDist.dataCurveValues = v.curve.values; break;
            case "nearFocusDist":       env.envParams.m_depthOfField.nearFocusDist.dataCurveValues = v.curve.values; break;
            case "farFocusDist":        env.envParams.m_depthOfField.farFocusDist.dataCurveValues = v.curve.values; break;
            case "farBlurDist":         env.envParams.m_depthOfField.farBlurDist.dataCurveValues = v.curve.values; break;
            case "intensity":           env.envParams.m_depthOfField.intensity.dataCurveValues = v.curve.values; break;
            case "activatedSkyThreshold":   env.envParams.m_depthOfField.activatedSkyThreshold = v.b; break;;
            case "skyThreshold":        env.envParams.m_depthOfField.skyThreshold = v.f; break;
            case "activatedSkyRange":   env.envParams.m_depthOfField.activatedSkyRange = v.b; break;;
            case "skyRange":            env.envParams.m_depthOfField.skyRange = v.f; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_colorModTransparency(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_colorModTransparency.activated = v.b; break;
            case "commonFarDist":       env.envParams.m_colorModTransparency.commonFarDist.dataCurveValues = v.curve.values; break;
            case "filterNearColor":     env.envParams.m_colorModTransparency.filterNearColor.dataCurveValues = v.curve.values; break;
            case "filterFarColor":      env.envParams.m_colorModTransparency.filterFarColor.dataCurveValues = v.curve.values; break;
            case "contrastNearStrength":    env.envParams.m_colorModTransparency.contrastNearStrength.dataCurveValues = v.curve.values; break;
            case "contrastFarStrength":     env.envParams.m_colorModTransparency.contrastFarStrength.dataCurveValues = v.curve.values; break;
            case "autoHideCustom0.distance":env.envParams.m_colorModTransparency.autoHideCustom0.distance.dataCurveValues = v.curve.values; break;
            case "autoHideCustom0.range":   env.envParams.m_colorModTransparency.autoHideCustom0.range.dataCurveValues = v.curve.values; break;
            case "autoHideCustom1.activated":   env.envParams.m_colorModTransparency.autoHideCustom1.activated = v.b; break;
            case "autoHideCustom1.distance":    env.envParams.m_colorModTransparency.autoHideCustom1.distance.dataCurveValues = v.curve.values; break;
            case "autoHideCustom1.range":       env.envParams.m_colorModTransparency.autoHideCustom1.range.dataCurveValues = v.curve.values; break;
            case "autoHideCustom2.activated":   env.envParams.m_colorModTransparency.autoHideCustom2.activated = v.b; break;
            case "autoHideCustom2.distance":    env.envParams.m_colorModTransparency.autoHideCustom2.distance.dataCurveValues = v.curve.values; break;
            case "autoHideCustom2.range":       env.envParams.m_colorModTransparency.autoHideCustom2.range.dataCurveValues = v.curve.values; break;
            case "autoHideCustom3.activated":   env.envParams.m_colorModTransparency.autoHideCustom3.activated = v.b; break;
            case "autoHideCustom3.distance":    env.envParams.m_colorModTransparency.autoHideCustom3.distance.dataCurveValues = v.curve.values; break;
            case "autoHideCustom3.range":       env.envParams.m_colorModTransparency.autoHideCustom3.range.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_shadows(id: String, v: SEUI_Value) {
        switch (id) {
            case "activatedAutoHide":   env.envParams.m_shadows.activatedAutoHide = v.b; break;
            case "autoHideBoxSizeMin":  env.envParams.m_shadows.autoHideBoxSizeMin.dataCurveValues = v.curve.values; break;
            case "autoHideBoxSizeMax":  env.envParams.m_shadows.autoHideBoxSizeMax.dataCurveValues = v.curve.values; break;
            case "autoHideBoxCompMaxX": env.envParams.m_shadows.autoHideBoxCompMaxX.dataCurveValues = v.curve.values; break;
            case "autoHideBoxCompMaxY": env.envParams.m_shadows.autoHideBoxCompMaxY.dataCurveValues = v.curve.values; break;
            case "autoHideBoxCompMaxZ": env.envParams.m_shadows.autoHideBoxCompMaxZ.dataCurveValues = v.curve.values; break;
            case "autoHideDistScale":   env.envParams.m_shadows.autoHideDistScale.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_water(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_water.activated = v.b; break;
            case "waterFlowIntensity":  env.envParams.m_water.waterFlowIntensity.dataCurveValues = v.curve.values; break;
            case "underwaterBrightness":    env.envParams.m_water.underwaterBrightness.dataCurveValues = v.curve.values; break;
            case "underWaterFogIntensity":  env.envParams.m_water.underWaterFogIntensity.dataCurveValues = v.curve.values; break;
            case "waterColor":          env.envParams.m_water.waterColor.dataCurveValues = v.curve.values; break;
            case "underWaterColor":     env.envParams.m_water.underWaterColor.dataCurveValues = v.curve.values; break;
            case "waterFresnel":        env.envParams.m_water.waterFresnel.dataCurveValues = v.curve.values; break;
            case "waterCaustics":       env.envParams.m_water.waterCaustics.dataCurveValues = v.curve.values; break;
            case "waterFoamIntensity":  env.envParams.m_water.waterFoamIntensity.dataCurveValues = v.curve.values; break;
            case "waterAmbientScale":   env.envParams.m_water.waterAmbientScale.dataCurveValues = v.curve.values; break;
            case "waterDiffuseScale":   env.envParams.m_water.waterDiffuseScale.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_colorGroups(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":       env.envParams.m_colorGroups.activated = v.b; break;
            case "defaultGroup":    env.envParams.m_colorGroups.defaultGroup.dataCurveValues = v.curve.values; break;
            case "lightsDefault":   env.envParams.m_colorGroups.lightsDefault.dataCurveValues = v.curve.values; break;
            case "lightsDawn":      env.envParams.m_colorGroups.lightsDawn.dataCurveValues = v.curve.values; break;
            case "lightsNoon":      env.envParams.m_colorGroups.lightsNoon.dataCurveValues = v.curve.values; break;
            case "lightsEvening":   env.envParams.m_colorGroups.lightsEvening.dataCurveValues = v.curve.values; break;
            case "lightsNight":     env.envParams.m_colorGroups.lightsNight.dataCurveValues = v.curve.values; break;
            case "fxDefault":       env.envParams.m_colorGroups.fxDefault.dataCurveValues = v.curve.values; break;
            case "fxFire":          env.envParams.m_colorGroups.fxFire.dataCurveValues = v.curve.values; break;
            case "fxFireFlares":    env.envParams.m_colorGroups.fxFireFlares.dataCurveValues = v.curve.values; break;
            case "fxFireLight":     env.envParams.m_colorGroups.fxFireLight.dataCurveValues = v.curve.values; break;
            case "fxSmoke":         env.envParams.m_colorGroups.fxSmoke.dataCurveValues = v.curve.values; break;
            case "fxSmokeExplosion":env.envParams.m_colorGroups.fxSmokeExplosion.dataCurveValues = v.curve.values; break;
            case "fxSky":           env.envParams.m_colorGroups.fxSky.dataCurveValues = v.curve.values; break;
            case "fxSkyAlpha":      env.envParams.m_colorGroups.fxSkyAlpha.dataCurveValues = v.curve.values; break;
            case "fxSkyNight":      env.envParams.m_colorGroups.fxSkyNight.dataCurveValues = v.curve.values; break;
            case "fxSkyNightAlpha": env.envParams.m_colorGroups.fxSkyNightAlpha.dataCurveValues = v.curve.values; break;
            case "fxSkyDawn":       env.envParams.m_colorGroups.fxSkyDawn.dataCurveValues = v.curve.values; break;
            case "fxSkyDawnAlpha":  env.envParams.m_colorGroups.fxSkyDawnAlpha.dataCurveValues = v.curve.values; break;
            case "fxSkyNoon":       env.envParams.m_colorGroups.fxSkyNoon.dataCurveValues = v.curve.values; break;
            case "fxSkyNoonAlpha":  env.envParams.m_colorGroups.fxSkyNoonAlpha.dataCurveValues = v.curve.values; break;
            case "fxSkySunset":     env.envParams.m_colorGroups.fxSkySunset.dataCurveValues = v.curve.values; break;
            case "fxSkySunsetAlpha":    env.envParams.m_colorGroups.fxSkySunsetAlpha.dataCurveValues = v.curve.values; break;
            case "fxSkyRain":       env.envParams.m_colorGroups.fxSkyRain.dataCurveValues = v.curve.values; break;
            case "fxSkyRainAlpha":  env.envParams.m_colorGroups.fxSkyRainAlpha.dataCurveValues = v.curve.values; break;
            case "mainCloudsMiddle":        env.envParams.m_colorGroups.mainCloudsMiddle.dataCurveValues = v.curve.values; break;
            case "mainCloudsMiddleAlpha":   env.envParams.m_colorGroups.mainCloudsMiddleAlpha.dataCurveValues = v.curve.values; break;
            case "mainCloudsFront":         env.envParams.m_colorGroups.mainCloudsFront.dataCurveValues = v.curve.values; break;
            case "mainCloudsFrontAlpha":    env.envParams.m_colorGroups.mainCloudsFrontAlpha.dataCurveValues = v.curve.values; break;
            case "mainCloudsBack":          env.envParams.m_colorGroups.mainCloudsBack.dataCurveValues = v.curve.values; break;
            case "mainCloudsBackAlpha":     env.envParams.m_colorGroups.mainCloudsBackAlpha.dataCurveValues = v.curve.values; break;
            case "mainCloudsRim":           env.envParams.m_colorGroups.mainCloudsRim.dataCurveValues = v.curve.values; break;
            case "mainCloudsRimAlpha":      env.envParams.m_colorGroups.mainCloudsRimAlpha.dataCurveValues = v.curve.values; break;
            case "backgroundCloudsFront":   env.envParams.m_colorGroups.backgroundCloudsFront.dataCurveValues = v.curve.values; break;
            case "backgroundCloudsFrontAlpha":  env.envParams.m_colorGroups.backgroundCloudsFrontAlpha.dataCurveValues = v.curve.values; break;
            case "backgroundCloudsBack":        env.envParams.m_colorGroups.backgroundCloudsBack.dataCurveValues = v.curve.values; break;
            case "backgroundCloudsBackAlpha":   env.envParams.m_colorGroups.backgroundCloudsBackAlpha.dataCurveValues = v.curve.values; break;
            case "backgroundHazeFront":         env.envParams.m_colorGroups.backgroundHazeFront.dataCurveValues = v.curve.values; break;
            case "backgroundHazeFrontAlpha":    env.envParams.m_colorGroups.backgroundHazeFrontAlpha.dataCurveValues = v.curve.values; break;
            case "backgroundHazeBack":          env.envParams.m_colorGroups.backgroundHazeBack.dataCurveValues = v.curve.values; break;
            case "backgroundHazeBackAlpha":     env.envParams.m_colorGroups.backgroundHazeBackAlpha.dataCurveValues = v.curve.values; break;
            case "fxBlood":         env.envParams.m_colorGroups.fxBlood.dataCurveValues = v.curve.values; break;
            case "fxWater":         env.envParams.m_colorGroups.fxWater.dataCurveValues = v.curve.values; break;
            case "fxFog":           env.envParams.m_colorGroups.fxFog.dataCurveValues = v.curve.values; break;
            case "fxTrails":        env.envParams.m_colorGroups.fxTrails.dataCurveValues = v.curve.values; break;
            case "fxScreenParticles":   env.envParams.m_colorGroups.fxScreenParticles.dataCurveValues = v.curve.values; break;
            case "fxLightShaft":        env.envParams.m_colorGroups.fxLightShaft.dataCurveValues = v.curve.values; break;
            case "fxLightShaftSun":     env.envParams.m_colorGroups.fxLightShaftSun.dataCurveValues = v.curve.values; break;
            case "fxLightShaftInteriorDawn":    env.envParams.m_colorGroups.fxLightShaftInteriorDawn.dataCurveValues = v.curve.values; break;
            case "fxLightShaftSpotlightDawn":   env.envParams.m_colorGroups.fxLightShaftSpotlightDawn.dataCurveValues = v.curve.values; break;
            case "fxLightShaftReflectionLightDawn": env.envParams.m_colorGroups.fxLightShaftReflectionLightDawn.dataCurveValues = v.curve.values; break;
            case "fxLightShaftInteriorNoon":        env.envParams.m_colorGroups.fxLightShaftInteriorNoon.dataCurveValues = v.curve.values; break;
            case "fxLightShaftSpotlightNoon":       env.envParams.m_colorGroups.fxLightShaftSpotlightNoon.dataCurveValues = v.curve.values; break;
            case "fxLightShaftReflectionLightNoon": env.envParams.m_colorGroups.fxLightShaftReflectionLightNoon.dataCurveValues = v.curve.values; break;
            case "fxLightShaftInteriorEvening":     env.envParams.m_colorGroups.fxLightShaftInteriorEvening.dataCurveValues = v.curve.values; break;
            case "fxLightShaftSpotlightEvening":    env.envParams.m_colorGroups.fxLightShaftSpotlightEvening.dataCurveValues = v.curve.values; break;
            case "fxLightShaftReflectionLightEvening":  env.envParams.m_colorGroups.fxLightShaftReflectionLightEvening.dataCurveValues = v.curve.values; break;
            case "fxLightShaftInteriorNight":       env.envParams.m_colorGroups.fxLightShaftInteriorNight.dataCurveValues = v.curve.values; break;
            case "fxLightShaftSpotlightNight":      env.envParams.m_colorGroups.fxLightShaftSpotlightNight.dataCurveValues = v.curve.values; break;
            case "fxLightShaftReflectionLightNight":    env.envParams.m_colorGroups.fxLightShaftReflectionLightNight.dataCurveValues = v.curve.values; break;
            case "activatedCustom0":    env.envParams.m_colorGroups.activatedCustom0 = v.b; break;
            case "customGroup0":        env.envParams.m_colorGroups.customGroup0.dataCurveValues = v.curve.values; break;
            case "activatedCustom1":    env.envParams.m_colorGroups.activatedCustom1 = v.b; break;
            case "customGroup1":        env.envParams.m_colorGroups.customGroup1.dataCurveValues = v.curve.values; break;
            case "activatedCustom2":    env.envParams.m_colorGroups.activatedCustom2 = v.b; break;
            case "customGroup2":        env.envParams.m_colorGroups.customGroup2.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_flareColorGroups(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_flareColorGroups.activated = v.b; break;
            case "custom0.activated":   env.envParams.m_flareColorGroups.custom0.activated = v.b; break;
            case "custom0.color0":      env.envParams.m_flareColorGroups.custom0.color0.dataCurveValues = v.curve.values; break;
            case "custom0.opacity0":    env.envParams.m_flareColorGroups.custom0.opacity0.dataCurveValues = v.curve.values; break;
            case "custom0.color1":      env.envParams.m_flareColorGroups.custom0.color1.dataCurveValues = v.curve.values; break;
            case "custom0.opacity1":    env.envParams.m_flareColorGroups.custom0.opacity1.dataCurveValues = v.curve.values; break;
            case "custom0.color2":      env.envParams.m_flareColorGroups.custom0.color2.dataCurveValues = v.curve.values; break;
            case "custom0.opacity2":    env.envParams.m_flareColorGroups.custom0.opacity2.dataCurveValues = v.curve.values; break;
            case "custom0.color3":      env.envParams.m_flareColorGroups.custom0.color3.dataCurveValues = v.curve.values; break;
            case "custom0.opacity3":    env.envParams.m_flareColorGroups.custom0.opacity3.dataCurveValues = v.curve.values; break;
            case "custom1.activated":   env.envParams.m_flareColorGroups.custom1.activated = v.b; break;
            case "custom1.color0":      env.envParams.m_flareColorGroups.custom1.color0.dataCurveValues = v.curve.values; break;
            case "custom1.opacity0":    env.envParams.m_flareColorGroups.custom1.opacity0.dataCurveValues = v.curve.values; break;
            case "custom1.color1":      env.envParams.m_flareColorGroups.custom1.color1.dataCurveValues = v.curve.values; break;
            case "custom1.opacity1":    env.envParams.m_flareColorGroups.custom1.opacity1.dataCurveValues = v.curve.values; break;
            case "custom1.color2":      env.envParams.m_flareColorGroups.custom1.color2.dataCurveValues = v.curve.values; break;
            case "custom1.opacity2":    env.envParams.m_flareColorGroups.custom1.opacity2.dataCurveValues = v.curve.values; break;
            case "custom1.color3":      env.envParams.m_flareColorGroups.custom1.color3.dataCurveValues = v.curve.values; break;
            case "custom1.opacity3":    env.envParams.m_flareColorGroups.custom1.opacity3.dataCurveValues = v.curve.values; break;
            case "custom2.activated":   env.envParams.m_flareColorGroups.custom2.activated = v.b; break;
            case "custom2.color0":      env.envParams.m_flareColorGroups.custom2.color0.dataCurveValues = v.curve.values; break;
            case "custom2.opacity0":    env.envParams.m_flareColorGroups.custom2.opacity0.dataCurveValues = v.curve.values; break;
            case "custom2.color1":      env.envParams.m_flareColorGroups.custom2.color1.dataCurveValues = v.curve.values; break;
            case "custom2.opacity1":    env.envParams.m_flareColorGroups.custom2.opacity1.dataCurveValues = v.curve.values; break;
            case "custom2.color2":      env.envParams.m_flareColorGroups.custom2.color2.dataCurveValues = v.curve.values; break;
            case "custom2.opacity2":    env.envParams.m_flareColorGroups.custom2.opacity2.dataCurveValues = v.curve.values; break;
            case "custom2.color3":      env.envParams.m_flareColorGroups.custom2.color3.dataCurveValues = v.curve.values; break;
            case "custom2.opacity3":    env.envParams.m_flareColorGroups.custom2.opacity3.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_sunAndMoonParams(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":       env.envParams.m_sunAndMoonParams.activated = v.b; break;
            case "sunSize":         env.envParams.m_sunAndMoonParams.sunSize.dataCurveValues = v.curve.values; break;
            case "sunColor":        env.envParams.m_sunAndMoonParams.sunColor.dataCurveValues = v.curve.values; break;
            case "sunFlareSize":    env.envParams.m_sunAndMoonParams.sunFlareSize.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.activated": env.envParams.m_sunAndMoonParams.sunFlareColor.activated = v.b; break;
            case "sunFlareColor.color0":    env.envParams.m_sunAndMoonParams.sunFlareColor.color0.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.opacity0":  env.envParams.m_sunAndMoonParams.sunFlareColor.opacity0.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.color1":    env.envParams.m_sunAndMoonParams.sunFlareColor.color1.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.opacity1":  env.envParams.m_sunAndMoonParams.sunFlareColor.opacity1.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.color2":    env.envParams.m_sunAndMoonParams.sunFlareColor.color2.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.opacity2":  env.envParams.m_sunAndMoonParams.sunFlareColor.opacity2.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.color3":    env.envParams.m_sunAndMoonParams.sunFlareColor.color3.dataCurveValues = v.curve.values; break;
            case "sunFlareColor.opacity3":  env.envParams.m_sunAndMoonParams.sunFlareColor.opacity3.dataCurveValues = v.curve.values; break;
            case "moonSize":        env.envParams.m_sunAndMoonParams.moonSize.dataCurveValues = v.curve.values; break;
            case "moonColor":       env.envParams.m_sunAndMoonParams.moonColor.dataCurveValues = v.curve.values; break;
            case "moonFlareSize":   env.envParams.m_sunAndMoonParams.moonFlareSize.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.activated":    env.envParams.m_sunAndMoonParams.moonFlareColor.activated = v.b; break;
            case "moonFlareColor.color0":   env.envParams.m_sunAndMoonParams.moonFlareColor.color0.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.opacity0": env.envParams.m_sunAndMoonParams.moonFlareColor.opacity0.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.color1":   env.envParams.m_sunAndMoonParams.moonFlareColor.color1.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.opacity1": env.envParams.m_sunAndMoonParams.moonFlareColor.opacity1.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.color2":   env.envParams.m_sunAndMoonParams.moonFlareColor.color2.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.opacity2": env.envParams.m_sunAndMoonParams.moonFlareColor.opacity2.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.color3":   env.envParams.m_sunAndMoonParams.moonFlareColor.color3.dataCurveValues = v.curve.values; break;
            case "moonFlareColor.opacity3": env.envParams.m_sunAndMoonParams.moonFlareColor.opacity3.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_windParams(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":           env.envParams.m_windParams.activated = v.b; break;
            case "windStrengthOverride":    env.envParams.m_windParams.windStrengthOverride.dataCurveValues = v.curve.values; break;
            case "cloudsVelocityOverride":  env.envParams.m_windParams.cloudsVelocityOverride.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_gameplayEffects(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":                   env.envParams.m_gameplayEffects.activated = v.b; break;
            case "catEffectBrightnessMultiply": env.envParams.m_gameplayEffects.catEffectBrightnessMultiply.dataCurveValues = v.curve.values; break;
            case "behaviorAnimationMultiplier": env.envParams.m_gameplayEffects.behaviorAnimationMultiplier.dataCurveValues = v.curve.values; break;
            case "specularityMultiplier":       env.envParams.m_gameplayEffects.specularityMultiplier.dataCurveValues = v.curve.values; break;
            case "glossinessMultiplier":        env.envParams.m_gameplayEffects.glossinessMultiplier.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_motionBlur(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":   env.envParams.m_motionBlur.activated = v.b; break;
            case "strength":    env.envParams.m_motionBlur.strength.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_cameraLightsSetup(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":                   env.envParams.m_cameraLightsSetup.activated = v.b; break;
            case "gameplayLight0.activated":    env.envParams.m_cameraLightsSetup.gameplayLight0.activated = v.b; break;
            case "gameplayLight0.color":        env.envParams.m_cameraLightsSetup.gameplayLight0.color.dataCurveValues = v.curve.values; break;
            case "gameplayLight0.attenuation":  env.envParams.m_cameraLightsSetup.gameplayLight0.attenuation.dataCurveValues = v.curve.values; break;
            case "gameplayLight0.radius":       env.envParams.m_cameraLightsSetup.gameplayLight0.radius.dataCurveValues = v.curve.values; break;
            case "gameplayLight0.offsetFront":  env.envParams.m_cameraLightsSetup.gameplayLight0.offsetFront.dataCurveValues = v.curve.values; break;
            case "gameplayLight0.offsetRight":  env.envParams.m_cameraLightsSetup.gameplayLight0.offsetRight.dataCurveValues = v.curve.values; break;
            case "gameplayLight0.offsetUp":     env.envParams.m_cameraLightsSetup.gameplayLight0.offsetUp.dataCurveValues = v.curve.values; break;
            case "gameplayLight1.activated":    env.envParams.m_cameraLightsSetup.gameplayLight1.activated = v.b; break;
            case "gameplayLight1.color":        env.envParams.m_cameraLightsSetup.gameplayLight1.color.dataCurveValues = v.curve.values; break;
            case "gameplayLight1.attenuation":  env.envParams.m_cameraLightsSetup.gameplayLight1.attenuation.dataCurveValues = v.curve.values; break;
            case "gameplayLight1.radius":       env.envParams.m_cameraLightsSetup.gameplayLight1.radius.dataCurveValues = v.curve.values; break;
            case "gameplayLight1.offsetFront":  env.envParams.m_cameraLightsSetup.gameplayLight1.offsetFront.dataCurveValues = v.curve.values; break;
            case "gameplayLight1.offsetRight":  env.envParams.m_cameraLightsSetup.gameplayLight1.offsetRight.dataCurveValues = v.curve.values; break;
            case "gameplayLight1.offsetUp":     env.envParams.m_cameraLightsSetup.gameplayLight1.offsetUp.dataCurveValues = v.curve.values; break;
            case "sceneLight0.activated":       env.envParams.m_cameraLightsSetup.sceneLight0.activated = v.b; break;
            case "sceneLight0.color":           env.envParams.m_cameraLightsSetup.sceneLight0.color.dataCurveValues = v.curve.values; break;
            case "sceneLight0.attenuation":     env.envParams.m_cameraLightsSetup.sceneLight0.attenuation.dataCurveValues = v.curve.values; break;
            case "sceneLight0.radius":          env.envParams.m_cameraLightsSetup.sceneLight0.radius.dataCurveValues = v.curve.values; break;
            case "sceneLight0.offsetFront":     env.envParams.m_cameraLightsSetup.sceneLight0.offsetFront.dataCurveValues = v.curve.values; break;
            case "sceneLight0.offsetRight":     env.envParams.m_cameraLightsSetup.sceneLight0.offsetRight.dataCurveValues = v.curve.values; break;
            case "sceneLight0.offsetUp":        env.envParams.m_cameraLightsSetup.sceneLight0.offsetUp.dataCurveValues = v.curve.values; break;
            case "sceneLight1.activated":       env.envParams.m_cameraLightsSetup.sceneLight1.activated = v.b; break;
            case "sceneLight1.color":           env.envParams.m_cameraLightsSetup.sceneLight1.color.dataCurveValues = v.curve.values; break;
            case "sceneLight1.attenuation":     env.envParams.m_cameraLightsSetup.sceneLight1.attenuation.dataCurveValues = v.curve.values; break;
            case "sceneLight1.radius":          env.envParams.m_cameraLightsSetup.sceneLight1.radius.dataCurveValues = v.curve.values; break;
            case "sceneLight1.offsetFront":     env.envParams.m_cameraLightsSetup.sceneLight1.offsetFront.dataCurveValues = v.curve.values; break;
            case "sceneLight1.offsetRight":     env.envParams.m_cameraLightsSetup.sceneLight1.offsetRight.dataCurveValues = v.curve.values; break;
            case "sceneLight1.offsetUp":        env.envParams.m_cameraLightsSetup.sceneLight1.offsetUp.dataCurveValues = v.curve.values; break;
            case "dialogLight0.activated":      env.envParams.m_cameraLightsSetup.dialogLight0.activated = v.b; break;
            case "dialogLight0.color":          env.envParams.m_cameraLightsSetup.dialogLight0.color.dataCurveValues = v.curve.values; break;
            case "dialogLight0.attenuation":    env.envParams.m_cameraLightsSetup.dialogLight0.attenuation.dataCurveValues = v.curve.values; break;
            case "dialogLight0.radius":         env.envParams.m_cameraLightsSetup.dialogLight0.radius.dataCurveValues = v.curve.values; break;
            case "dialogLight0.offsetFront":    env.envParams.m_cameraLightsSetup.dialogLight0.offsetFront.dataCurveValues = v.curve.values; break;
            case "dialogLight0.offsetRight":    env.envParams.m_cameraLightsSetup.dialogLight0.offsetRight.dataCurveValues = v.curve.values; break;
            case "dialogLight0.offsetUp":       env.envParams.m_cameraLightsSetup.dialogLight0.offsetUp.dataCurveValues = v.curve.values; break;
            case "dialogLight1.activated":      env.envParams.m_cameraLightsSetup.dialogLight1.activated = v.b; break;
            case "dialogLight1.color":          env.envParams.m_cameraLightsSetup.dialogLight1.color.dataCurveValues = v.curve.values; break;
            case "dialogLight1.attenuation":    env.envParams.m_cameraLightsSetup.dialogLight1.attenuation.dataCurveValues = v.curve.values; break;
            case "dialogLight1.radius":         env.envParams.m_cameraLightsSetup.dialogLight1.radius.dataCurveValues = v.curve.values; break;
            case "dialogLight1.offsetFront":    env.envParams.m_cameraLightsSetup.dialogLight1.offsetFront.dataCurveValues = v.curve.values; break;
            case "dialogLight1.offsetRight":    env.envParams.m_cameraLightsSetup.dialogLight1.offsetRight.dataCurveValues = v.curve.values; break;
            case "dialogLight1.offsetUp":       env.envParams.m_cameraLightsSetup.dialogLight1.offsetUp.dataCurveValues = v.curve.values; break;
            case "interiorLight0.activated":    env.envParams.m_cameraLightsSetup.interiorLight0.activated = v.b; break;
            case "interiorLight0.color":        env.envParams.m_cameraLightsSetup.interiorLight0.color.dataCurveValues = v.curve.values; break;
            case "interiorLight0.attenuation":  env.envParams.m_cameraLightsSetup.interiorLight0.attenuation.dataCurveValues = v.curve.values; break;
            case "interiorLight0.radius":       env.envParams.m_cameraLightsSetup.interiorLight0.radius.dataCurveValues = v.curve.values; break;
            case "interiorLight0.offsetFront":  env.envParams.m_cameraLightsSetup.interiorLight0.offsetFront.dataCurveValues = v.curve.values; break;
            case "interiorLight0.offsetRight":  env.envParams.m_cameraLightsSetup.interiorLight0.offsetRight.dataCurveValues = v.curve.values; break;
            case "interiorLight0.offsetUp":     env.envParams.m_cameraLightsSetup.interiorLight0.offsetUp.dataCurveValues = v.curve.values; break;
            case "interiorLight1.activated":    env.envParams.m_cameraLightsSetup.interiorLight1.activated = v.b; break;
            case "interiorLight1.color":        env.envParams.m_cameraLightsSetup.interiorLight1.color.dataCurveValues = v.curve.values; break;
            case "interiorLight1.attenuation":  env.envParams.m_cameraLightsSetup.interiorLight1.attenuation.dataCurveValues = v.curve.values; break;
            case "interiorLight1.radius":       env.envParams.m_cameraLightsSetup.interiorLight1.radius.dataCurveValues = v.curve.values; break;
            case "interiorLight1.offsetFront":  env.envParams.m_cameraLightsSetup.interiorLight1.offsetFront.dataCurveValues = v.curve.values; break;
            case "interiorLight1.offsetRight":  env.envParams.m_cameraLightsSetup.interiorLight1.offsetRight.dataCurveValues = v.curve.values; break;
            case "interiorLight1.offsetUp":     env.envParams.m_cameraLightsSetup.interiorLight1.offsetUp.dataCurveValues = v.curve.values; break;
            case "playerInInteriorLightsScale": env.envParams.m_cameraLightsSetup.playerInInteriorLightsScale.dataCurveValues = v.curve.values; break;
            case "sceneLightColorInterior0":    env.envParams.m_cameraLightsSetup.sceneLightColorInterior0.dataCurveValues = v.curve.values; break;
            case "sceneLightColorInterior1":    env.envParams.m_cameraLightsSetup.sceneLightColorInterior1.dataCurveValues = v.curve.values; break;
            case "cameraLightsNonCharacterScale":   env.envParams.m_cameraLightsSetup.cameraLightsNonCharacterScale.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function set_m_dialogLightParams(id: String, v: SEUI_Value) {
        switch (id) {
            case "activated":   env.envParams.m_dialogLightParams.activated = v.b; break;
            case "lightColor":  env.envParams.m_dialogLightParams.lightColor.dataCurveValues = v.curve.values; break;
            case "lightColor2": env.envParams.m_dialogLightParams.lightColor2.dataCurveValues = v.curve.values; break;
            case "lightColor3": env.envParams.m_dialogLightParams.lightColor3.dataCurveValues = v.curve.values; break;
        }
    }
    // ------------------------------------------------------------------------
    private function setValue(cat1: string, id: string, v: SEUI_Value) {
        // top level partitioning
        switch (cat1) {
            case "finalColorBalance":   set_m_finalColorBalance(id, v); break;
            case "sharpen":             set_m_sharpen(id, v); break;
            case "paintEffect":         set_m_paintEffect(id, v); break;
            case "ssaoNV":              set_m_ssaoNV(id, v); break;
            case "ssaoMS":              set_m_ssaoMS(id, v); break;
            case "globalLight":         set_m_globalLight(id, v); break;
            case "interiorFallback":    set_m_interiorFallback(id, v); break;
            case "speedTree":           set_m_speedTree(id, v); break;
            case "toneMapping":         set_m_toneMapping(id, v); break;
            case "bloomNew":            set_m_bloomNew(id, v); break;
            case "globalFog":           set_m_globalFog(id, v); break;
            case "sky":                 set_m_sky(id, v); break;
            case "depthOfField":        set_m_depthOfField(id, v); break;
            case "colorModTransparency":set_m_colorModTransparency(id, v); break;
            case "shadows":             set_m_shadows(id, v); break;
            case "water":               set_m_water(id, v); break;
            case "colorGroups":         set_m_colorGroups(id, v); break;
            case "flareColorGroups":    set_m_flareColorGroups(id, v); break;
            case "sunAndMoonParams":    set_m_sunAndMoonParams(id, v); break;
            case "windParams":          set_m_windParams(id, v); break;
            case "gameplayEffects":     set_m_gameplayEffects(id, v); break;
            case "motionBlur":          set_m_motionBlur(id, v); break;
            case "cameraLightsSetup":   set_m_cameraLightsSetup(id, v); break;
            case "dialogLightParams":   set_m_dialogLightParams(id, v); break;
        }
    }
    // ------------------------------------------------------------------------
    private function adjust(
        v: SEUI_Value, delta: float, type: EEUI_AdjustType) : SEUI_Value
    {
        var cp: SCurveDataEntry;
        switch (v.type) {
            case EEUI_CURVE:
                cp = v.curve.values[v.curve.p];
                switch (type) {
                    case EEUI_SCALAR: cp.lue += delta; break;
                    case EEUI_VEC1: cp.ntrolPoint.X += delta; break;
                    case EEUI_VEC2: cp.ntrolPoint.Y += delta; break;
                    case EEUI_VEC3: cp.ntrolPoint.Z += delta; break;
                    case EEUI_VEC4: cp.ntrolPoint.W += delta; break;
                }
                v.curve.values[v.curve.p] = cp;
                break;
            case EEUI_FLOAT:    v.f += delta; break;
            case EEUI_BOOL:     v.b = delta > 0; break;
            case EEUI_INT:      v.i += RoundF(delta); break;
        }
        return v;
    }
    // ------------------------------------------------------------------------
    public function adjustValue(
        cat1: String, id: String, delta: float, type: EEUI_AdjustType) : SEUI_Value
    {
        var v: SEUI_Value;
        v = adjust(getValue(cat1, id), delta, type);
        setValue(cat1, id, v);
        return v;
    }
    // ------------------------------------------------------------------------
    public function getCurrentValue(cat1: String, id: String) : SEUI_Value {
        var s: SEUI_Value;
        var d: SEUI_Value;

        s = getValue(cat1, id);

        // copy the relevant data
        d.type = s.type;
        switch (s.type) {
            case EEUI_CURVE:    d.curve.values.PushBack(s.curve.values[s.curve.p]); break;
            case EEUI_FLOAT:    d.f = s.f; break;
            case EEUI_BOOL:     d.b = s.b; break;
            case EEUI_INT:      d.i = s.i; break;
        }
        return d;
    }
    // ------------------------------------------------------------------------
    public function setCurrentValue(
        cat1: String, id: String, newValue: SEUI_Value)
    {
        var d: SEUI_Value;
        var cp, newCp: SCurveDataEntry;

        d = getValue(cat1, id);

        // copy the relevant data
        switch (d.type) {
            case EEUI_CURVE:
                // overwrite *only* the values as other settings are NOT set!
                cp = d.curve.values[d.curve.p];
                newCp = newValue.curve.values[0];
                cp.lue = newCp.lue;
                cp.ntrolPoint.X = newCp.ntrolPoint.X;
                cp.ntrolPoint.Y = newCp.ntrolPoint.Y;
                cp.ntrolPoint.Z = newCp.ntrolPoint.Z;
                cp.ntrolPoint.W = newCp.ntrolPoint.W;

                d.curve.values[d.curve.p] = cp;
                break;

            case EEUI_FLOAT:    d.f = newValue.f; break;
            case EEUI_BOOL:     d.b = newValue.b; break;
            case EEUI_INT:      d.i = newValue.i; break;
        }
        setValue(cat1, id, d);
    }
    // ------------------------------------------------------------------------
    public function getFormattedValue(cat1: String, id: String) : String {
        return formatValue(getValue(cat1, id));
    }
    // ------------------------------------------------------------------------
    public function removeCurrentCurvePoint(cat1: String, id: String) : bool {
        var val: SEUI_Value;
        var p: int;

        val = getValue(cat1, id);

        if (val.type == EEUI_CURVE && val.curve.values.Size() > 0) {

            p = val.curve.p;
            val.curve.values.Erase(p);
            val.curve.p = Min(p, val.curve.values.Size() - 1);
            setValue(cat1, id, val);
            return true;
        }

        return false;
    }
    // ------------------------------------------------------------------------
    private function newEntry(time: float) : SCurveDataEntry {
        return SCurveDataEntry(
            time,
            Vector(-0.1, 0.0, 0.0, 0.0),
            -0.1
        );
        //rveTypeL: Uint16;
        //rveTypeR: Uint16;
    }
    // ------------------------------------------------------------------------
    private function findNearestPointBefore(
        time: float, points: array<SCurveDataEntry>) : int
    {
        var bestDelta: float = 1;
        var delta: float;
        var index, i: int;

        index = 0;
        for (i = 0; i < points.Size(); i += 1) {
            delta = time - points[i].me;

            if (delta == 0.0) {
                // there is already such a point
                return -1;
            }
            if (delta > 0 && delta < bestDelta) {
                index = i;
                bestDelta = delta;
            }
        }

        return index;
    }
    // ------------------------------------------------------------------------
    public function addNewCurvePoint(
        gameTime: GameTime, cat1: String, id: String) : bool
    {
        var newEntry: SCurveDataEntry;
        var time: float = GameTimeToSeconds(gameTime) / 86400.0;
        var val: SEUI_Value;
        var len, p: int;

        val = getValue(cat1, id);

        if (val.type == EEUI_CURVE) {
            time = ModF(time, 1.0);
            newEntry = newEntry(time);

            // since the gametime may be some arbitrary time we have to find the
            // correct position in the array (assuming it's time sorted)
            p = findNearestPointBefore(time, val.curve.values);

            if (p >= 0) {
                // add directly after
                len = val.curve.values.Size();
                if (len == 0 || p == len - 1) {
                    val.curve.values.PushBack(newEntry);
                } else {
                    val.curve.values.Insert(p + 1, newEntry);
                }

                val.curve.p = Min(p + 1, val.curve.values.Size() - 1);
                setValue(cat1, id, val);

                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------------------
    public function getTimeOfValue(
        cat1: String, id: String, pointOffset: int,
        out time: GameTime, out pointSlot: int, out slotCount: int) : bool
    {
        var val: SEUI_Value;
        var p: int;

        val = getValue(cat1, id);

        if (val.type == EEUI_CURVE) {
            p = val.curve.p + pointOffset;
            slotCount = val.curve.values.Size();

            if (p >= 0 && p < slotCount) {
                time = timeToGameTime(val.curve.values[p].me);
                pointSlot = p + 1;
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
