// ----------------------------------------------------------------------------
//
// KNOWN BUGS:
//  - UI listview weirdness
//
// TODO:
//  - define (optionally) type of setting in the csv (e.g. Color, distance, ..) and
//      clamp values accordingly
//  - add (optionally) range of setting in the csv and clamp accordingly
//  - add (optionally) stepsize for a setting in the csv (or use range/20) and
//      adjust increasing/decreasing of value accordingly
//  - add "fast/slow" modifier hotkeys for value adjustments which double / halve
//      current stepsize for setting while hotkey is pressed
//
// ----------------------------------------------------------------------------
function modCreate_EnvUiMod() : CModEnvUi {
    return new CModEnvUi in thePlayer;
}
// ----------------------------------------------------------------------------
// when multiple envs are active adjustment of settings does not seem to work
exec function envui_disable_envs() {
    var null: CEnvironmentDefinition;
    var i : int;

    for (i = 0; i < 10000; i+=1) {
        DeactivateEnvironment(i, 0.0);
    }
}
// ----------------------------------------------------------------------------
exec function envui() {
    var mod: CModEnvUi;
    mod = modCreate_EnvUiMod();
    mod.init();
}
// ----------------------------------------------------------------------------
struct SEUI_Setting {
    var cat1: String;
    var cat2: String;
    var cat3: String;
    var sid: String;
    var caption: String;
}
// ----------------------------------------------------------------------------
enum EEUI_EmptySettingFilter {
    EEUI_ESF_NONE = 0,
    EEUI_ESF_HIDE = 1,
    EEUI_ESF_MARK = 2,
}
// ----------------------------------------------------------------------------
class CModEnvUiList extends CModUiFilteredList {
    private var settings: array<SEUI_Setting>;
    private var fullList: array<SModUiCategorizedListItem>;

    private var filterType: EEUI_EmptySettingFilter;
    // ------------------------------------------------------------------------
    public function initList() {
        var data: C2dArray;
        var i: int;

        data = LoadCSV("dlc/dlcenvui/data/envui_settings.csv");

        items.Clear();
        // csv: CAT1;CAT2;CAT3;id;caption
        for (i = 0; i < data.GetNumRows(); i += 1) {
            settings.PushBack(SEUI_Setting(
                data.GetValueAt(0, i),
                data.GetValueAt(1, i),
                data.GetValueAt(2, i),
                data.GetValueAt(3, i),
                data.GetValueAt(4, i),
            ));
            fullList.PushBack(SModUiCategorizedListItem(
                i,
                settings[i].caption,
                settings[i].cat1,
                settings[i].cat2,
                settings[i].cat3
            ));
        }

        items = fullList;
        filterType = EEUI_ESF_HIDE;
    }
    // ------------------------------------------------------------------------
    public function getEmptySettingsFilter() : EEUI_EmptySettingFilter {
        return this.filterType;
    }
    // ------------------------------------------------------------------------
    public function filterEmptySettings(envParams: CModEnvUiParams, filter: EEUI_EmptySettingFilter) {
        var value: SEUI_Value;
        var i, s: int;
        var skipCategory: String;
        var removeEmpty: bool;

        filterType = filter;

        switch (filterType) {
            case EEUI_ESF_MARK: removeEmpty = false; break;
            case EEUI_ESF_HIDE: removeEmpty = true; break;

            case EEUI_ESF_NONE:
            default:
                items = fullList;
                return;
        }

        skipCategory = "-";

        s = fullList.Size();
        items.Clear();

        for (i = 0; i < s; i += 1) {
            value = envParams.getValue(settings[i].cat1, settings[i].sid);

            if (settings[i].sid == "activated" && !value.b) {
                skipCategory = settings[i].cat1;
            }

            if (settings[i].cat1 != skipCategory) {
                skipCategory = "-";

                if (value.type != EEUI_CURVE || value.curve.values.Size() > 0) {
                    items.PushBack(fullList[i]);
                    continue;
                }
            }

            if (!removeEmpty) {
                items.PushBack(
                    SModUiCategorizedListItem(
                        i,
                        "<font color=\"#666666\">" + settings[i].caption + "</font>",
                        settings[i].cat1,
                        settings[i].cat2,
                        settings[i].cat3
                    )
                );
            }
        }
    }
    // ------------------------------------------------------------------------
    public function refreshFilter(envParams: CModEnvUiParams) {
        filterEmptySettings(envParams, filterType);
    }
    // ------------------------------------------------------------------------
    public function getMeta(id: int) : SEUI_Setting {
        return settings[id];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// callback for generic ui list which will call example mod back
class CModEnvUiListCallback extends IModUiEditableListCallback {
    public var callback: CModEnvUi;

    public function OnConfigUi() {
        var topMenuConf: SModUiTopMenuConfig;

        // overwrite default config to save screenspace and use extra field
        listMenuRef.setMenuConfig(SModUiMenuConfig(67.0, 80.0, 22));
        // patch top menu config
        topMenuConf = listMenuRef.getTopMenuConfig();
        topMenuConf.y -= 20.0;
        topMenuConf.height = 22.7;

        topMenuConf.titleField.x = 1;
        topMenuConf.titleField.y -= 20;

        topMenuConf.statsField.x = 3;
        topMenuConf.statsField.y -= 20;

        topMenuConf.editField.x = 3;
        topMenuConf.editField.y -= 20;

        topMenuConf.extraField.x = 4;
        topMenuConf.extraField.y = 152;

        topMenuConf.titleField.fontSize = 17;
        topMenuConf.statsField.fontSize = 12;
        topMenuConf.editField.fontSize = 11;
        topMenuConf.extraField.fontSize = 9;

        listMenuRef.setTopMenuConfig(topMenuConf);
    }

    public function OnOpened() { callback.OnUpdateView(); }

    public function OnInputEnd(inputString: String) { callback.OnInputEnd(inputString); }

    public function OnInputCancel() { callback.OnInputCancel(); }

    public function OnClosed() {
        var null: CModUiEditableListView;
        delete this.listMenuRef;
        this.listMenuRef = null;
        callback.OnClosedView();
    }

    public function OnSelected(optionName: String) { callback.OnSelected(optionName); }
}
// ----------------------------------------------------------------------------
state EUI_EditSetting in CModEnvUi {
    // ------------------------------------------------------------------------
    // alias
    private var envParams: CModEnvUiParams;
    private var listProvider: CModEnvUiList;

    private var selectedSetting: SEUI_Setting;
    private var stepSize: float; default stepSize = 1.0;
    // ------------------------------------------------------------------------
    private var isStepSizeChange: bool;
    private var isTimeCycle: bool;
    private var isValueEditing: bool;

    private var editedValueType: EEUI_ValueType;
    // ------------------------------------------------------------------------
    // gamesettings to be restored after leaving editing mode
    private var prevGameTime: GameTime;
    private var prevWeather: CName;
    private var restoreWeather: bool;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        this.envParams = parent.envParams;
        this.listProvider = parent.listProvider;

        theInput.StoreContext('MOD_EnvUi');
        registerListeners();

        if (prevStateName == 'EUI_Idle') {
            saveGameplaySettings();
            parent.envStack.storeCurrentEnvs();
            envParams.setEnv(parent.envStack.getTopLayerEnv());

            listProvider.refreshFilter(envParams);
            updateEnvCaption();

            if (isWeatherEnvActive()) {
                parent.log.debug("weather env active");
                RequestWeatherChangeTo('WT_Clear', 0.0, false);
                restoreWeather = true;
            } else {
                restoreWeather = false;
            }
        }
        parent.showUi(true);
    }
    // ------------------------------------------------------------------------
    private function isWeatherEnvActive() : bool {
        var world: CGameWorld;
        var weatherSettings: C2dArray;
        var weatherRow: int;
        var envPath: String;

        world = (CGameWorld)theGame.GetWorld();
        weatherSettings = world.environmentParameters.weatherTemplate;

        prevWeather = GetWeatherConditionName();

        weatherRow = weatherSettings.GetRowIndex("name", prevWeather);
        envPath = weatherSettings.GetValue("envPath", weatherRow);

        return envPath != "";
    }
    // ------------------------------------------------------------------------
    entry function changeWeather(weather: name) {
        var i: int;

        while (GetWeatherConditionName() != 'WT_Clear' && i < 60) {
            SleepOneFrame();
            i += 1;
        }
        RequestWeatherChangeTo(weather, 0.0, false);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        if (nextStateName == 'EUI_Idle') {
            restoreGameplaySettings();
            envParams.setEnv(parent.envStack.getBaseLayerEnv());

            if (restoreWeather) {
                changeWeather(prevWeather);
                parent.log.debug("restoring weather env " + prevWeather);
            }
        }
        unregisterListeners();
        theInput.RestoreContext('MOD_EnvUi', true);
    }
    // ------------------------------------------------------------------------
    private function saveGameplaySettings() {
        prevGameTime = theGame.GetGameTime();
        parent.modEnvUtil.freezeTime();
        parent.modEnvUtil.deactivateHud();

        // stop envui breakage if enemies attack
        thePlayer.SetTemporaryAttitudeGroup('q104_avallach_friendly_to_all', AGP_Default);
    }
    // ------------------------------------------------------------------------
    private function restoreGameplaySettings() {
        thePlayer.ResetTemporaryAttitudeGroup(AGP_Default);

        theGame.SetGameTime(prevGameTime, true);

        parent.modEnvUtil.unfreezeTime();
        parent.modEnvUtil.reactivateHud();
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('EUI_SetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ResetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ListCategoryUp'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_SelectPrevNext'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_LogValues'));

        hotkeyList.PushBack(HotkeyHelp_from('EUI_EditCurrentValue'));

        hotkeyList.PushBack(HotkeyHelp_from('EUI_ChangeValue'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ChangeValueP1'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ChangeValueP2'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ChangeValueP3'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ChangeValueP4'));

        hotkeyList.PushBack(HotkeyHelp_from('EUI_ToggleStepSizeChange'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ControlStepSize'));

        hotkeyList.PushBack(HotkeyHelp_from('EUI_CycleCurvePointTime'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_SelectPrevNext', "EUI_SelectCurvePoint"));

        hotkeyList.PushBack(HotkeyHelp_from('EUI_AdjustTime'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_SnapTimeToCurvePoint'));

        hotkeyList.PushBack(HotkeyHelp_from('EUI_AddPoint'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_DelPoint'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ToggleEmptyVisibility'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ChangeEnvLayer'));
    }
    // ------------------------------------------------------------------------
    // called by user action to start filter input
    event OnFilter(action: SInputAction) {
        if (!parent.view.listMenuRef.isEditActive() && IsPressed(action)) {

            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("EUI_lFilter"), listProvider.getWildcardFilter());
        }
    }
    // ------------------------------------------------------------------------
    // called by user action to reset currently set filter
    event OnResetFilter() {
        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        updateView();
    }
    // ------------------------------------------------------------------------
    // called by user action to go one opened category up
    event OnCategoryUp(action: SInputAction) {
        if (IsPressed(action)) {
            listProvider.clearLowestSelectedCategory();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnCycleSelection(action: SInputAction) {
        var prev: bool;
        var id: String;

        if (action.value != 0) {
            prev = action.value < 0;

            if (isTimeCycle) {
                if (prev) {
                    changeToCurvePointTime(-1);
                } else {
                    changeToCurvePointTime(1);
                }
                updateCurrentValueCaption();
            } else {
                if (prev) {
                    id = listProvider.getPreviousId();
                } else {
                    id = listProvider.getNextId();
                }
                OnSelected(id);
            }
        }
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnInputCancel() {
        parent.notice(GetLocStringByKeyExt("EUI_iEditCanceled"));

        isValueEditing = false;
        parent.view.listMenuRef.resetEditField();
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnInputEnd(inputString: String) {
        if (inputString == "") {
            OnResetFilter();
        } else {
            if (isValueEditing) {
                parseAndSetValue(inputString);
                parent.view.listMenuRef.resetEditField();
            } else {
                // Note: filter field is not removed to indicate the current filter
                listProvider.setWildcardFilter(inputString);
            }
            updateView();
        }
        isValueEditing = false;
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnSelected(listItemId: String) {
        // listprovider opens a category if a category was selected otherwise
        // returns true (meaning a "real" item was selected)
        if (listProvider.setSelection(listItemId, true)) {
            selectedSetting = listProvider.getMeta(StringToInt(listItemId));
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview (when list menu opens)
    event OnUpdateView() {
        var wildcard: String;
        // Note: if search filter is active show the wildcard to indicate the
        // current filter
        wildcard = listProvider.getWildcardFilter();
        if (wildcard != "") {
            parent.view.listMenuRef.setInputFieldData(
                GetLocStringByKeyExt("EUI_lFilter"), wildcard);
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnModifierChange(action: SInputAction) {
        if (IsPressed(action)) {
            switch (action.aName) {
                case 'EUI_CycleCurvePointTime':     isTimeCycle = true; break;
                case 'EUI_ToggleStepSizeChange':    isStepSizeChange = true; break;
            }
        } else if (IsReleased(action)) {
            switch (action.aName) {
                case 'EUI_CycleCurvePointTime':     isTimeCycle = false; break;
                case 'EUI_ToggleStepSizeChange':    isStepSizeChange = false; break;
            }
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    event OnManagePoint(action: SInputAction) {
        var gameTime: GameTime;

        if (IsPressed(action)) {
            switch (action.aName) {
                case 'EUI_AddPoint':
                    gameTime = theGame.GetGameTime();

                    if (envParams.addNewCurvePoint(
                            gameTime, selectedSetting.cat1, selectedSetting.sid))
                    {
                        parent.notice(GetLocStringByKeyExt("EUI_iPointAdded"));
                    }
                    break;

                case 'EUI_DelPoint':
                    if (envParams.removeCurrentCurvePoint(
                            selectedSetting.cat1, selectedSetting.sid))
                    {
                        parent.notice(GetLocStringByKeyExt("EUI_iPointRemoved"));
                    }
                    break;
            }
            listProvider.refreshFilter(envParams);
            updateCurrentValueCaption();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // value adjustments
    event OnChangeValue(action: SInputAction) {
        var t: EEUI_AdjustType;
        var v: SEUI_Value;

        if (action.value != 0) {

            switch (action.aName) {
                case 'EUI_ChangeValue':     t = EEUI_SCALAR; break;
                case 'EUI_ChangeValueP1':   t = EEUI_VEC1; break;
                case 'EUI_ChangeValueP2':   t = EEUI_VEC2; break;
                case 'EUI_ChangeValueP3':   t = EEUI_VEC3; break;
                case 'EUI_ChangeValueP4':   t = EEUI_VEC4; break;
            }

            v = envParams.adjustValue(
                selectedSetting.cat1, selectedSetting.sid, action.value * stepSize, t);

            updateCurrentValueCaption();

            // since bools may toggle filtering of empty categories ("activated")
            // update filter
            if (v.type == EEUI_BOOL) {
                listProvider.refreshFilter(envParams);
                updateView();
            }
        }
    }
    // ------------------------------------------------------------------------
    private function updateCurrentValueCaption() {
        parent.view.listMenuRef.setExtraField(
                envParams.getFormattedValue(selectedSetting.cat1, selectedSetting.sid),
                "#aaaaaa");
    }
    // ------------------------------------------------------------------------
    private function strToFloat(str: String, out f: float) : bool {
        f = StringToFloat(str, -666.6);
        return f != -666.6;
    }
    // ------------------------------------------------------------------------
    private function strToInt(str: String, out i: int) : bool {
        i = StringToInt(str, -666);
        return i != -666;
    }
    // ------------------------------------------------------------------------
    private function strToCurvePoint(str: String, out p: SCurveDataEntry) : bool
    {
        var success: bool;
        var v, x, y, z, w: float;
        var remaining, s1, s2, s3, s4, s5: String;

        success = StrSplitFirst(str, " ", s1, remaining);
        success = success && StrSplitFirst(remaining, " ", s2, remaining);
        success = success && StrSplitFirst(remaining, " ", s3, remaining);
        success = success && StrSplitFirst(remaining, " ", s4, s5);

        success = success && strToFloat(s1, v);
        success = success && strToFloat(s2, x);
        success = success && strToFloat(s3, y);
        success = success && strToFloat(s4, z);
        success = success && strToFloat(s5, w);

        p.lue = v;
        p.ntrolPoint = Vector(x, y, z, w);

        return success;
    }
    // ------------------------------------------------------------------------
    private function parseValueStr(
        str: String, type: EEUI_ValueType, out result: SEUI_Value) : bool
    {
        var success: bool;

        switch (type) {
            case EEUI_CURVE:
                success = strToCurvePoint(str, result.curve.values[0]);
                break;

            case EEUI_FLOAT:
                success = strToFloat(str, result.f);
                break;

            case EEUI_INT:
                success = strToInt(str, result.i);
                break;
        }
        return success;
    }
    // ------------------------------------------------------------------------
    private function parseAndSetValue(inputString: String) {
        var newValue: SEUI_Value;

        newValue.curve.values.PushBack(SCurveDataEntry());

        if (parseValueStr(inputString, editedValueType, newValue)) {
            envParams.setCurrentValue(selectedSetting.cat1, selectedSetting.sid, newValue);
        } else {
            parent.error(GetLocStringByKeyExt("EUI_eValueParseErr"));
        }
    }
    // ------------------------------------------------------------------------
    event OnEditValue(action: SInputAction) {
        var strValue: String;
        var value: SEUI_Value;
        var point: SCurveDataEntry;
        var null: SEUI_Setting;

        if (IsPressed(action)
            && selectedSetting != null
            && !parent.view.listMenuRef.isEditActive())
        {
            value = envParams.getCurrentValue(selectedSetting.cat1, selectedSetting.sid);

            switch (value.type) {
                case EEUI_CURVE:
                    point = value.curve.values[0];
                    strValue = NoTrailZeros(point.lue) + " "
                        + NoTrailZeros(point.ntrolPoint.X) + " "
                        + NoTrailZeros(point.ntrolPoint.Y) + " "
                        + NoTrailZeros(point.ntrolPoint.Z) + " "
                        + NoTrailZeros(point.ntrolPoint.W);
                    break;

                case EEUI_FLOAT:    strValue = NoTrailZeros(value.f); break;
                case EEUI_INT:      strValue = IntToString(value.i); break;
                case EEUI_BOOL:     return false; // doesn't make sense to "edit" bools
            }

            isValueEditing = true;
            editedValueType = value.type;

            parent.view.listMenuRef.setExtraField("");
            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("EUI_lEditValue"), strValue);
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        if (isStepSizeChange && action.value != 0) {

            if (action.value > 0) {
                if (stepSize < 20) { stepSize *= 1.5; }
            } else {
                if (stepSize > 0.1)  { stepSize /= 1.5; }
            }
            stepSize = ClampF(stepSize, 0.01, 20);

            parent.notice(GetLocStringByKeyExt("EUI_iCurrentStepsSize")
                + " " + FloatToString(stepSize));
        }
    }
    // ------------------------------------------------------------------------
    event OnShowValue(action: SInputAction) {
        if (IsPressed(action)) {
            parent.notice(GetLocStringByKeyExt("EUI_iCurrentValue")
                + " "
                + envParams.getFormattedValue(selectedSetting.cat1, selectedSetting.sid));
        }
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // time stuff
    event OnSnapToCurveTime(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'EUI_SnapTimeToCurvePoint') {
                changeToCurvePointTime(0);
            }
        }
    }
    // ------------------------------------------------------------------------
    private function changeToCurvePointTime(pointOffset: int) {
        var gameTime: GameTime;
        var pointSlot, slotCount: int;

        if (envParams.getTimeOfValue(selectedSetting.cat1, selectedSetting.sid, pointOffset,
            gameTime, pointSlot, slotCount))
        {
            theGame.SetGameTime(gameTime, true);
            parent.notice(GetLocStringByKeyExt("EUI_iTimeSnappedToPoint")
                + " (" + pointSlot + "/" + slotCount + ")");
        }
        // not a curve point or not a valid point offset
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    event OnLogEnvData(action: SInputAction) {
        var defWriter: CModEnvUiDefinitionWriter;
        var setting: SEUI_Setting;
        var envs: array<String>;
        var i: int;

        if (IsPressed(action)) {

            LogChannel('ENVUIDATA', GetLocStringByKeyExt("EUI_iEnvsLogStart")
                + " (" + GameTimeToString(theGame.GetGameTime()) + ")");

            GetActiveAreaEnvironmentDefinitions(envs);
            for (i = 0; i < envs.Size(); i += 1) {
                LogChannel('ENVUIDATA', envs[i]);
            }
            LogChannel('ENVUIDATA', "logged env: " + parent.envStack.getCurrentEnvCaption());

            defWriter = new CModEnvUiDefinitionWriter in this;
            defWriter.init();
            defWriter.logDefinition(envParams);

            parent.notice(GetLocStringByKeyExt("EUI_iEnvsLogged"));
        }
    }
    // ------------------------------------------------------------------------
    protected function updateView() {
        updateCurrentValueCaption();

        // set updated list data and render in listview
        parent.view.listMenuRef.setListData(
            listProvider.getFilteredList(),
            listProvider.getMatchingItemCount(),
            // number of items without filtering
            listProvider.getTotalCount());

        parent.view.listMenuRef.updateView();
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PopState();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleEmptySettingVisibility(action: SInputAction) {
        if (IsReleased(action)) {
            switch (listProvider.getEmptySettingsFilter()) {
                case EEUI_ESF_HIDE:
                    listProvider.filterEmptySettings(envParams, EEUI_ESF_MARK); break;
                case EEUI_ESF_MARK:
                    listProvider.filterEmptySettings(envParams, EEUI_ESF_NONE); break;
                default:
                    listProvider.filterEmptySettings(envParams, EEUI_ESF_HIDE);
            }
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    private function updateEnvCaption() {
        var newCaption: String;

        newCaption = GetLocStringByKeyExt("EUI_lListTitle")
            + " <font size=\"8\" color=\"#aaaaaa\">("
            + parent.envStack.getCurrentEnvCaption()
            + ")</font>";

        parent.view.title = newCaption;
        // update field if the menu is already open
        parent.view.listMenuRef.setTitle(newCaption);
    }
    // ------------------------------------------------------------------------
    event OnChangeEnvLayer(action: SInputAction) {
        if (action.value != 0) {
            if (action.value < 0) {
                envParams.setEnv(parent.envStack.getPreviousLayerEnv());
            } else {
                envParams.setEnv(parent.envStack.getNextLayerEnv());
            }
            listProvider.refreshFilter(envParams);
            updateEnvCaption();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnInteractiveTime(action: SInputAction) {
        if (IsReleased(action)) {
            parent.PushState('EUI_InteractiveTime');
        }
    }
    // ------------------------------------------------------------------------
    private function registerListeners() {
        theInput.RegisterListener(parent, 'OnHelpMePrettyPlease', 'EUI_ShowHelp');

        theInput.RegisterListener(this, 'OnBack', 'EUI_Back');
        theInput.RegisterListener(this, 'OnFilter', 'EUI_SetFilter');
        theInput.RegisterListener(this, 'OnResetFilter', 'EUI_ResetFilter');
        theInput.RegisterListener(this, 'OnCategoryUp', 'EUI_ListCategoryUp');
        theInput.RegisterListener(this, 'OnCycleSelection', 'EUI_SelectPrevNext');
        theInput.RegisterListener(this, 'OnChangeValue', 'EUI_ChangeValue');
        theInput.RegisterListener(this, 'OnChangeValue', 'EUI_ChangeValueP1');
        theInput.RegisterListener(this, 'OnChangeValue', 'EUI_ChangeValueP2');
        theInput.RegisterListener(this, 'OnChangeValue', 'EUI_ChangeValueP3');
        theInput.RegisterListener(this, 'OnChangeValue', 'EUI_ChangeValueP4');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'EUI_ControlStepSize');
        theInput.RegisterListener(this, 'OnLogEnvData', 'EUI_LogValues');

        theInput.RegisterListener(this, 'OnEditValue', 'EUI_EditCurrentValue');

        theInput.RegisterListener(this, 'OnModifierChange', 'EUI_ToggleStepSizeChange');
        theInput.RegisterListener(this, 'OnModifierChange', 'EUI_CycleCurvePointTime');

        theInput.RegisterListener(this, 'OnSnapToCurveTime', 'EUI_SnapTimeToCurvePoint');
        theInput.RegisterListener(this, 'OnInteractiveTime', 'EUI_AdjustTime');

        theInput.RegisterListener(this, 'OnManagePoint', 'EUI_AddPoint');
        theInput.RegisterListener(this, 'OnManagePoint', 'EUI_DelPoint');

        theInput.RegisterListener(this, 'OnChangeEnvLayer', 'EUI_ChangeEnvLayer');

        theInput.RegisterListener(this, 'OnToggleEmptySettingVisibility', 'EUI_ToggleEmptyVisibility');
    }
    // ------------------------------------------------------------------------
    private function unregisterListeners() {
        theInput.UnregisterListener(parent, 'EUI_ShowHelp');

        theInput.UnregisterListener(this, 'EUI_Back');
        theInput.UnregisterListener(this, 'EUI_SetFilter');
        theInput.UnregisterListener(this, 'EUI_ResetFilter');
        theInput.UnregisterListener(this, 'EUI_ListCategoryUp');
        theInput.UnregisterListener(this, 'EUI_SelectPrevNext');
        theInput.UnregisterListener(this, 'EUI_ChangeValue');
        theInput.UnregisterListener(this, 'EUI_ChangeValueP1');
        theInput.UnregisterListener(this, 'EUI_ChangeValueP2');
        theInput.UnregisterListener(this, 'EUI_ChangeValueP3');
        theInput.UnregisterListener(this, 'EUI_ChangeValueP4');
        theInput.UnregisterListener(this, 'EUI_ControlStepSize');
        theInput.UnregisterListener(this, 'EUI_LogValues');

        theInput.UnregisterListener(this, 'EUI_EditCurrentValue');

        theInput.UnregisterListener(this, 'EUI_ToggleStepSizeChange');
        theInput.UnregisterListener(this, 'EUI_CycleCurvePointTime');

        theInput.UnregisterListener(this, 'EUI_SnapTimeToCurvePoint');
        theInput.UnregisterListener(this, 'EUI_AdjustTime');

        theInput.UnregisterListener(this, 'EUI_AddPoint');
        theInput.UnregisterListener(this, 'EUI_DelPoint');

        theInput.UnregisterListener(this, 'EUI_ChangeEnvLayer');

        theInput.UnregisterListener(this, 'EUI_ToggleEmptyVisibility');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// idle state waits for menu hotkey
state EUI_Idle in CModEnvUi {
    // ------------------------------------------------------------------------
    // called by user action to open menu
    event OnOpenMenu(action: SInputAction) {
        var envDefs: array<String>;
        var str: String;
        var i: int;

        if (IsPressed(action)) {
            GetActiveAreaEnvironmentDefinitions(envDefs);
            for (i = 0; i < envDefs.Size(); i += 1) {
                str += envDefs[i] + "/";
            }
            theGame.GetGuiManager().ShowNotification(
                IntToString(envDefs.Size()) + " "
                + GetLocStringByKeyExt("EUI_iActiveEnvs") + " "
                + str);
            parent.PushState('EUI_EditSetting');
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModEnvUiEnvStack {
    // ------------------------------------------------------------------------
    private var skipCutsceneEnv: bool; default skipCutsceneEnv = true;
    // ------------------------------------------------------------------------
    private var envRegistry: CModEnvUiEnvRegistry;

    private var envNames: array<String>;
    private var envStack: array<CEnvironmentDefinition>;
    private var currentLayer: int;
    // ------------------------------------------------------------------------
    public function init(registry: CModEnvUiEnvRegistry) {
        envRegistry = registry;
    }
    // ------------------------------------------------------------------------
    public function storeCurrentEnvs() {
        var envPath: String;
        var i: int;

        envNames.Clear();
        envStack.Clear();
        currentLayer = 0;
        GetActiveAreaEnvironmentDefinitions(envNames);

        for (i = 0; i < envNames.Size(); i += 1) {
            envPath = envRegistry.getPath(envNames[i]);

            if (envPath != "") {
                envStack.PushBack((CEnvironmentDefinition)LoadResource(envPath, true));
            } else {
                LogChannel('ENVUI', "env name not found in registry: " + envNames[i]);
                theGame.GetGuiManager().ShowNotification(
                    GetLocStringByKeyExt("EUI_eEnvPathNotInRegistry") + " " + envNames[i]);
            }
        }
    }
    // ------------------------------------------------------------------------
    private function getLayerEnv(layer: int) : CEnvironmentDefinition {
        currentLayer = layer;
        return envStack[currentLayer];
    }
    // ------------------------------------------------------------------------
    public function getNextLayerEnv() : CEnvironmentDefinition {
        currentLayer = (currentLayer + 1) % envNames.Size();

        // skip cutscene layer
        if (skipCutsceneEnv && currentLayer == 1) {
            return getNextLayerEnv();
        }
        return getLayerEnv(currentLayer);
    }
    // ------------------------------------------------------------------------
    public function getPreviousLayerEnv() : CEnvironmentDefinition {
        currentLayer = (envNames.Size() + currentLayer - 1) % envNames.Size();

        // skip cutscene layer
        if (skipCutsceneEnv && currentLayer == 1) {
            return getPreviousLayerEnv();
        }
        return getLayerEnv(currentLayer);
    }
    // ------------------------------------------------------------------------
    public function getCurrentLayerEnv() : CEnvironmentDefinition {
        return getLayerEnv(currentLayer);
    }
    // ------------------------------------------------------------------------
    public function getBaseLayerEnv() : CEnvironmentDefinition {
        return getLayerEnv(0);
    }
    // ------------------------------------------------------------------------
    public function getTopLayerEnv() : CEnvironmentDefinition {
        // always skip cutscene layer (flag is ignored!)
        if (envNames.Size() == 2) {
            return getLayerEnv(0);
        }
        return getLayerEnv(envNames.Size() - 1);
    }
    // ------------------------------------------------------------------------
    public function getCurrentEnvCaption() : String {
        var layers, current: int;

        layers = envNames.Size();
        current = currentLayer;

        if (skipCutsceneEnv) {
            layers -= 1;
            if (currentLayer > 1) {
                current -= 1;
            }
        }

        return IntToString(current + 1) + "/" + IntToString(layers) + " "
            + StrReplaceAll(envNames[currentLayer], "_", " ");
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CModEnvUiEnvRegistry {
    // ------------------------------------------------------------------------
    private var envPaths: array<String>;
    private var envIds: array<String>;
    // ------------------------------------------------------------------------
    public function init() {
        var data: C2dArray;
        var i: int;
        var custom: array<String>;
        var separator, id: String;

        data = LoadCSV("dlc/dlcenvui/data/envui_env_list.csv");

        envPaths.Clear();
        envIds.Clear();
        // csv: id;path;
        for (i = 0; i < data.GetNumRows(); i += 1) {
            envIds.PushBack(data.GetValueAt(0, i));
            envPaths.PushBack(data.GetValueAt(1, i));
        }

        // add custom envs
        custom = ENVUI_getExtraEnvs();
        separator = StrChar(92);

        for (i = 0; i < custom.Size(); i += 1) {
            id = StrAfterLast(custom[i], separator);
            if (StrEndsWith(id, ".env")) {
                id = StrBeforeLast(id, ".env");
            }
            LogChannel('ENVUI', "found custom env: " + id + " path: " + custom[i]);
            envPaths.PushBack(custom[i]);
            envIds.PushBack(id);
        }
    }
    // ------------------------------------------------------------------------
    public function getPath(id: String) : String {
        var i: int;

        i = envIds.FindFirst(id);
        return envPaths[i];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CModEnvUi extends CMod {
    default modName = 'EnvUi';
    default modAuthor = "rmemr";
    default modUrl = "http://www.nexusmods.com/witcher3/mods/2111/";
    default modVersion = '0.4.1';

    default logLevel = MLOG_DEBUG;
    // ------------------------------------------------------------------------
    protected var view: CModEnvUiListCallback;
    protected var listProvider: CModEnvUiList;

    protected var envParams: CModEnvUiParams;

    protected var envRegistry: CModEnvUiEnvRegistry;
    protected var envStack: CModEnvUiEnvStack;
    // ------------------------------------------------------------------------
    protected var modEnvUtil: CRadishModUtils;
    // ------------------------------------------------------------------------
    // flag indicating to pop state *after* currently open view is closed
    private var popStateAfterViewClose: bool;
    // ------------------------------------------------------------------------
    public function init() {
        super.init();

        envRegistry = new CModEnvUiEnvRegistry in this;
        envRegistry.init();

        // prepare env layerstack
        envStack = new CModEnvUiEnvStack in this;
        envStack.init(envRegistry);

        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("EUI_Started"));

        // prepare view callback wiring and set labels
        view = new CModEnvUiListCallback in this;
        view.callback = this;
        view.title = GetLocStringByKeyExt("EUI_lListTitle");
        view.statsLabel = GetLocStringByKeyExt("EUI_lListElements");

        // utility
        modEnvUtil = this.createModEnvUtil();

        // load list of settings
        listProvider = new CModEnvUiList in this;
        listProvider.initList();

        envParams = new CModEnvUiParams in this;

        registerListeners();
        PushState('EUI_Idle');
    }
    // ------------------------------------------------------------------------
    private function createModEnvUtil() : CRadishModUtils {
        var entity : CEntity;
        var template : CEntityTemplate;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_modutils.w2ent", true);
        entity = theGame.CreateEntity(template,
                thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishModUtils)entity;
    }
    // ------------------------------------------------------------------------
    public function showUi(showUi: bool) {
        if (showUi) {
            if (!view.listMenuRef) {
                theGame.RequestMenu('ListView', view);
            }
        } else {
            if (view.listMenuRef) {
                view.listMenuRef.close();
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('EUI_ShowHelp'));
        hotkeyList.PushBack(HotkeyHelp_from('EUI_Back'));
    }
    // ------------------------------------------------------------------------
    event OnHelpMePrettyPlease(action: SInputAction) {
        var helpPopup: CModUiHotkeyHelp;
        var titleKey: String;
        var introText: String;
        var hotkeyList: array<SModUiHotkeyHelp>;

        if (IsPressed(action)) {
            helpPopup = new CModUiHotkeyHelp in this;
            titleKey = "EUI_tHelpHotkey";
            introText += "<p align=\"left\">" + GetLocStringByKeyExt("EUI_GeneralHelp") + "</p>";

            OnHotkeyHelp(hotkeyList);

            helpPopup.open(titleKey, introText, hotkeyList);
        }
    }
    // ------------------------------------------------------------------------
    event OnUpdateView() {}
    event OnInputEnd(inputString: String) {}
    event OnInputCancel() {}
    event OnSelected(listItemId: String) {}
    // ------------------------------------------------------------------------
    event OnClosedView() {
        var null: SInputAction;
        if (popStateAfterViewClose) {
            popStateAfterViewClose = false;
            PopState();
        }
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        theGame.GetGuiManager().ShowNotification(msg);
    }
    // ------------------------------------------------------------------------
    protected function error(errMsg: String) {
        theGame.GetGuiManager().ShowNotification(errMsg);
    }
    // ------------------------------------------------------------------------
    public function isUiShown() : bool {
        return view.listMenuRef;
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        var null: CModUiEditableListView;

        if (IsReleased(action)) {
            if (isUiShown()) {
                // defer restoring previous state to enable clean close of listview
                // otherwise (previous) state tries to open view on OnEnterState but
                // it gets immediately closed again
                popStateAfterViewClose = true;
                // since back is bound to ESCAPE which automatically closes an
                // open menu view the view must no be closed *again*
                delete view.listMenuRef;
                view.listMenuRef = null;
            } else {
                PopState();
            }
        }
    }
    // ------------------------------------------------------------------------
    protected function timeToString(time: GameTime) : String {
        var timeSec, sec, mins, hours: int;

        sec = GameTimeSeconds(time);
        mins = GameTimeMinutes(time);
        hours = GameTimeHours(time);

        return IntToString(hours) + ":" + IntToString(mins) + ":" + sec;
    }
    // ------------------------------------------------------------------------
    private function registerListeners() {
        theInput.RegisterListener(this, 'OnOpenMenu', 'EUI_OpenMenu');
    }
    // ------------------------------------------------------------------------
    private function unregisterListeners() {
        theInput.UnregisterListener(this, 'EUI_OpenMenu');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
